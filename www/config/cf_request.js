"use strict";

const express	= require('express');
const rateLimit	= require('express-rate-limit');

module.exports = function (app) {
	const limiter = rateLimit({
		windowMs: 10 * 60 * 1000, // 15 minutes
		max: 100 // limit each IP to 100 requests per windowMs
	});

    app.use(express.json({limit: '50mb'}));
    app.use(express.urlencoded({limit: '50mb', extended: true}));
	app.use(limiter);
};