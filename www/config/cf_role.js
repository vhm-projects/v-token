"use strict";

const jwt       = require('jsonwebtoken');
const cfJwt     = require('./cf_jws');
const request   = require('request');

const { 
	ADMIN_ACCESS, 
	EDITOR_ACCESS, 
	TYPE_RESPONSE
} = require('../utils/constant');

const { checkAndResponseForEachEnv, getParams } = require('./helpers');

module.exports = {
    role: {
        all: {
            bin: 1,
            auth: (_, __, next) => next()
        },
        user: {
            bin: 2,
            auth: function (req, res, next) {
                const token = req.params.token || req.body.token || req.query.token || req.headers[ 'x-access-token' ] || req.headers.token;
                const access_key = req.headers['x-access-key'] || req.query.access_key || req.body.access_key;

                if(access_key !== 'bGRrX3NvZnR3YXJlX3RlYW0tQDIwMjEK'){
                    return res.status(403).json({ error: true, message: 'Permission denied', code: 403 });
                }

				return next();

                if (token) {
                    /**
                     *  ------user data--------
                     * "user": {
                            "id": 34,
                            "name": "Hien Vinh",
                            "avatar": "https://vsports-files.s3.ap-southeast-1.amazonaws.com/1614234917294.jpg",
                            "phone": "",
                            "email": "hienvinh24@gmail.com",
                            "type": "admin",
                            "birthday": "1992-12-24T13:00:00.000Z",
                            "height": 168,
                            "weight": 12,
                            "national_id": "188",
                            "national_id_front": null,
                            "national_id_back": null,
                            "foot": "right",
                            "latitude": 10.7382,
                            "longitude": 106.68,
                            "facebook": "",
                            "instagram": "",
                            "tiktok": "",
                            "twitter": "",
                            "linkedin": "",
                            "youtube": "",
                            "created_at": "2020-03-22T05:15:32.000Z",
                            "updated_at": "2020-03-22T05:15:32.000Z",
                            "status": "active"
                        }
                     */

                    var optionsAuth = { method: 'GET',
                        url: `https://apis.vsports.com.vn/v1/auth/check`,
                        headers: { 
                            'Content-Type': 'application/json',
                            'Authorization': `Bearer ${token}`
                        }
                    };
                    request(optionsAuth, async (error, response, body) => {
                        if (error) throw new Error(error);

                        if (body == 'Unauthorized')
                            return res.json({ error: true, message: 'token_invalid' });

                        let responseAuth = JSON.parse(body);

                        if (responseAuth && responseAuth.data) {
                            let { token, user } = responseAuth.data;
                            if (user) {
                                req.user = user;
                                req.token = token;
                                next();
                            } else {
                                return res.json({success: false, message: 'Error: Permission denied.'});
                            }
                        } else {
                            return res.json({success: false, message: 'Error: Permission denied.'});
                        }
                    })
                } else {
                    return res.status(403).send({
                        success: false,
                        message: 'No token provided.'
                    });
                }
            }
        },
        admin: {
            bin: 3,
            auth: (req, res, next) => {
				let { envAccess, token } = getParams(req);

				if(!token){
					return checkAndResponseForEachEnv({
						res,
						envAccess,
						typeResponse: TYPE_RESPONSE.NOT_PROVIDE_TOKEN
					})
				}

				jwt.verify(token, cfJwt.secret, async (error, decoded) => {
					if (error) {
						return checkAndResponseForEachEnv({ 
							res,
							envAccess,
							typeResponse: TYPE_RESPONSE.TOKEN_INVALID
						});
					}

					if (!ADMIN_ACCESS.includes(+decoded.role)) {
						return checkAndResponseForEachEnv({ 
							res, 
							envAccess, 
							typeResponse: TYPE_RESPONSE.PERMISSION_DENIED
						});
					}

					req.user 		= decoded;
					req.envAccess   = envAccess;
					next();
				});
            }
        },
		editor: {
            bin: 4,
            auth: (req, res, next) => {
				let { envAccess, token } = getParams(req);

				if(!token){
					return checkAndResponseForEachEnv({
						res,
						envAccess,
						typeResponse: TYPE_RESPONSE.NOT_PROVIDE_TOKEN
					})
				}

				jwt.verify(token, cfJwt.secret, async (error, decoded) => {
					if (error) {
						return checkAndResponseForEachEnv({ 
							res,
							envAccess,
							typeResponse: TYPE_RESPONSE.TOKEN_INVALID
						});
					}

					if (!EDITOR_ACCESS.includes(+decoded.role)) {
						return checkAndResponseForEachEnv({ 
							res, 
							envAccess, 
							typeResponse: TYPE_RESPONSE.PERMISSION_DENIED
						});
					}

					req.user 		= decoded;
					req.envAccess   = envAccess;
					next();
				});
            }
        },
    },

    authorization: function (req, res, next) {
        var hasRole = false;
        var currentRole = null;

        for (var itemRole in this.role) {
            if (!hasRole) {
                if (res.bindingRole.config.auth.includes(this.role[ itemRole ].bin)) {
                    hasRole = true;
                    currentRole = this.role[ itemRole ];
                }
            }
        }

        currentRole.auth(req, res, next);
    }
};