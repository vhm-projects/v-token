
// TRẠNG THÁI
exports.ADMIN_STATUS = [
    { value: 0, text: 'Khóa' },
    { value: 1, text: 'Hoạt động' },
]

/**
 * MIME Types image
 */
 exports.MIME_TYPES_IMAGE = [ 
	'image/jpeg', 
	'image/pjpeg', 
	'image/png', 
	'image/svg+xml'
];

// LOẠI TEMPLATE MAP
exports.TYPE_TEMPLATE_MAP = [
    { value: 0, text: 'Mảng' },
    { value: 1, text: 'Chữ' },
    { value: 2, text: 'Khối' },
]

// LOẠI TEMPLATE MAP
exports.IS_FOR = [
    { value: 0, text: 'Không Lặp' },
    { value: 1, text: 'Có Lặp' },
]

// LOẠI TEMPLATE MAP
exports.TYPE_TEMPLATE_KEY_API = [
    { value: 1, text: 'TEXT' },
    { value: 2, text: 'TIME' },
    { value: 3, text: 'IMAGE' },
]

exports.FORMAT_TIME = [
    { value: 'LT',  text: '11:35 AM' },
    { value: 'LTS', text: '11:35:47 AM' },
    { value: 'L',   text: '08/01/2021' },
    { value: 'l',   text: '8/1/2021' },
    { value: 'LL',  text: '1 tháng 8 năm 2021' },
]

exports.LOCALE = [
    { value: 'vi', text: 'Việt Nam' },
    { value: 'en', text: 'United' },
    { value: 'zh-cn', text: 'Trung Quốc' },
    { value: 'ja', text: 'Nhật' },
    { value: 'ko', text: 'Hàn Quốc' },
]

exports.IS_SHORT_CUT = [
    { value: 1, text: 'Có' },
    { value: 0, text: 'Không' },
]

exports.SHAPE = [
    { value: -1, text: 'Giữ Nguyên Khối' },
    { value: 1,  text: 'Tròn' },
]

// GIỚI TÍNH
exports.GENDER_TYPE = [
    { value: 0, text: 'Nữ' },
    { value: 1, text: 'Nam' },
    { value: 2, text: 'Khác' },
]

// GIỚI TÍNH
exports.TYPE_METHOD = [
    { value: 'GET', text: 'GET' },
    { value: 'POST', text: 'POST' },
    { value: 'PUT', text: 'PUT' },
    { value: 'DELETE', text: 'DELETE' },
]


// RULE
exports.TYPE_RULE = {
	CONFIRM_SEASON: 1, 		// Xác minh giải đấu
	NEWBORD_FOLLOWER: 2, 	// Newborn Follower
	APPROVE_TEAM: 3, 		// Duyệt đội tham dự giải đấu
	CREATE_SCHEDULE: 4, 	// Tạo lịch thi đấu
	LIKE_POST: 5, 	        // Like bài viết
	AWARDS: 6, 	            // Trao giải thưởng
	CREATE_POST: 7,			// Tạo bài viết
	MINIGAME: 8,			// Minigame
	UPDATE_RESULT: 9,		// Cập nhật kết quả giải đấu
	EXCHANGE_FRUIT: 10,		// THay đổi điểm từ V SHOP
}

// RULE WITH KEY
exports.TYPE_RULE_WITH_KEY = {
    1: { value: 1, key: 'CONFIRM_SEASON', text: 'Xác minh giải đấu' },
    2: { value: 2, key: 'NEWBORD_FOLLOWER', text: 'Newborn Follower' },
    3: { value: 3, key: 'APPROVE_TEAM', text: 'Duyệt đội tham dự giải đấu' },
    4: { value: 4, key: 'CREATE_SCHEDULE', text: 'Tạo lịch thi đấu' },
    5: { value: 5, key: 'LIKE_POST', text: 'Like bài viết' },
    6: { value: 6, key: 'AWARDS', text: 'Trao giải thưởng' },
    7: { value: 7, key: 'CREATE_POST', text: 'Tạo bài viết' },
    8: { value: 8, key: 'MINIGAME', text: 'Minigame' },
    9: { value: 9, key: 'UPDATE_RESULT', text: 'Cập nhật kết quả giải đấu' },
}
