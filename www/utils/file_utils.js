"use strict";

let fs          = require('fs');
let path        = require('path');
let promise     = require('bluebird');
let http        = require('https');
let stringUtils = require('./string_utils');
let uuidv1      = require('uuidv1');

exports.uploadFile = function (file, path, args, callback) {
    file.mv(path, function (err) {
        if (err) {
            callback(false, args);
        } else {
            callback(true, args);
        }
    });
};

exports.baseUploadImageCode = function (code, path) {
    return new promise(function (resolve, reject) {
        fs.writeFile(path, code.replace(/^data:image\/\w+;base64,/, ''), {encoding: 'base64'}, function (err) {
            fs.chmod(path, '0777');
            return resolve({error: false, message: 'Upload success'});
        });
    });
};

exports.baseUploadImageFile = function (file, path) {
    return new promise(function (resolve, reject) {
        if (file == null) {
            return resolve({error: true, message: 'File not found'});
        } else {
            file.mv(path, function (err) {
                if (err) {
                    return resolve({error: true, message: 'File does not upload'});
                } else {
                    return resolve({error: false, message: 'Upload success'});
                }
            });
        }
    });
};

exports.checkAndCreateFolder = function (path) {
    let incs = path.split("/");
    let baseBath = '';
    incs.forEach(function (inc, index) {
        if (inc.trim() != "") {
            baseBath = baseBath + '/' + inc.trim();
            if (!fs.existsSync(baseBath)) {
                fs.mkdirSync(baseBath, '0777');
            }
        }
    });
    return baseBath;
};


exports.deleteFile = function (path) {
    fs.exists(path, function (exists) {
        if (exists) {
            fs.unlink(path);
        }
    });
};


exports.downloadFileOtherService = function(url, dest, cb) {
    return new Promise(async resolve => {
        try {
            const filePath = url.split('.');
            const newFileName = `${uuidv1()}.${filePath[ filePath.length - 1 ]}`;

            let uploadPathOutput = path.resolve(__dirname, '../../files/');

            let file = fs.createWriteStream(`${uploadPathOutput}/${newFileName}`);
            let stream = http.get(url, response => {
                response.pipe(file)
            })
            file.on('finish', function () {
                return resolve({ error: false, message: 'download_file_success', pathfile: `/files/${newFileName}` })
            });
        } catch (error) {
            return resolve({ error: true, message: error });   
        }
    })
};