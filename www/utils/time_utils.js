"use strict";

let moment   = require('moment');
let momentTZ = require('moment-timezone');
let timeConf = require('../config/cf_time');

Date.prototype.addDays = function(days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}

function setTimeZone(date) {
    return momentTZ.tz(date, timeConf._default_tme_zone);
}

exports.getCurrentTime = function () {
    return setTimeZone(new Date()).format('Y-MM-DD H:m:sZ');
};

exports.parseFormat1 = function (oldTimeFormat) {
    return setTimeZone(oldTimeFormat).format('Y-MM-DD | H:m');
};

exports.parseFormat2 = function (oldTimeFormat) {
    return setTimeZone(oldTimeFormat).format('H:mm DD-MM-Y');
};

exports.parseFormat3 = function (oldTimeFormat) {
    return setTimeZone(oldTimeFormat).format('MM-DD-Y');
};

exports.parseTimeFormat4 = function (oldTimeFormat) {
    return setTimeZone(oldTimeFormat).format('HH:mm DD/MM/Y');
};

exports.parseTimeFormat5 = function (oldTimeFormat) {
    return setTimeZone(oldTimeFormat).format('DD/MM/Y');
};

exports.parseTimeFormatOption = function (oldTimeFormat, format) {
    return setTimeZone(oldTimeFormat).format(format);
};


/**
 * compare time
 * if time1 > time2: return 1
 * if time1 < time2: return 2
 * if time1 = time2: return 0
 * @param time1
 * @param time2
 * @returns {number}
 */
exports.compareTwoTime = function (time1, time2) {
    let a = (new Date(time1)).getTime();
    let b = (new Date(time2)).getTime();
    if (a > b) {
        return 1;
    } else if (b > a) {
        return 2;
    } else {
        return 0;
    }
};

exports.getTimeBetween = function (time1, time2) {
    let a = (new Date(time1)).getTime();
    let b = (new Date(time2)).getTime();
    return (a - b) / (1000);
};

exports.addMinuteToDate = function (dateAdded, minute) {
    return new Date((new Date(dateAdded)).getTime() + minute * 60000);
};

exports.subMinuteToDate = function (subAdded, minute) {
    return new Date((new Date(subAdded)).getTime() - minute * 60000);
};

exports.getDaysOfWeek = (currDate = new Date()) => {
	let week = [];

	for (let i = 1; i <= 7; i++) {
		let first = currDate.getDate() - currDate.getDay() + i;
		let day = new Date(currDate.setDate(first)).toISOString().slice(0, 10);
		week.push(day)
	}

	return week;
}

exports.endOfWeek = (currentDate = new Date()) => {
    let day = currentDate.getDay();
    let date = currentDate.getDate();
    let lastday = day !== 0 ? date - (day) + 7 : date;

    return new Date(currentDate.setDate(lastday));
}

exports.calculateExpireDate = (currentTimestamp, expireTimestamp) => {
    const currentDate = new Date(currentTimestamp);
    const expireDate  = new Date(expireTimestamp);

    // Get date
    const dateCurrent = currentDate.getDate();
    const dateExpire = expireDate.getDate();

    // Get month
    const monthCurrent = currentDate.getMonth();
    const monthExpire = expireDate.getMonth();

    // Get year
    const yearCurrent = currentDate.getFullYear();
    const yearExpire = expireDate.getFullYear();

    const current = moment([yearCurrent, monthCurrent, dateCurrent]);
    const expire = moment([yearExpire, monthExpire, dateExpire]);

    return expire.diff(current, 'day');
}
