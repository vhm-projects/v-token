"use strict";

exports._base_logo1_url = "";
exports._base_logo2_url = "";
exports.webName = "AST - Voucher";

exports.constantScripts = [
    /**
     * GIÁ TRỊ(_data: là biến được truyền qua) USED.js
     *      - obj: {}
     *      - array: []
     * 1/ nếu là array ?
     *      + CÓ:
     *          - 1.2/ khởi tạo object đầu tiên của array (nếu arr.length > 0)
     *          - 1.3/ có 2 luồng show dữ liệu đổ 
     *              - luồng 1: lấy object đầu tiên(1.2) -> đổ: 'tên bảng', 'vòng thi đấu', ... (các dạng key dùng chung)
     *              - luồng 2: array -> for cho các giá trị cần như: 'tên đội nhà' (trong template danh_sách kết_quả_thi_đấu)
     *      + KHÔNG:
     *          - CHI TIẾT TRẬN ĐẤU -> kiểm tra các thông tin có giá trị chung hay không?
     */
    // ATTR: 'Danh Sách Trận Đấu'
    // -----------BEGIN: Loại có điểm chung: 'itemObjOfClassName[0].set'
    { //✅ đã cover được 
        label: 'Tên bảng', 
        id: 1,
        _script: function () {
            // ------------------------------------------------------------------_data[`${template_key_apis[index].key}`]
            return itemObjOfClassName && itemObjOfClassName[0].set('text', `Bảng ${_data.table_name}`);    
        }
    }, 
    { //✅ đã cover được 
        label: 'Vòng thi đấu',
        id: 2,
        _script: function () {
            return itemObjOfClassName &&  itemObjOfClassName[0].set('text', `Vòng ${_data.round}`)
        }
    },
    { //✅ đã cover 
        label: 'Địa điểm thi đấu',
        id: 4,
        _script: function () {
            return itemObjOfClassName && itemObjOfClassName[0].set('text', `${_data.stadium}`)
        }
    },  
    // -----------END: Loại có điểm chung: 'itemObjOfClassName[0].set'

    // -----------BEGIN: Loại có điểm chung: LOGO
    {
        label: 'Logo đội nhà', //template_map
        id: 6,
        _script: function () {
            return itemObjOfClassName.forEach((item, index) => {
                // this.canvas.remove.apply(this.canvas, [item]);
                let absoluteTop_team_home ;
                let absoluteLeft_team_home;

                let width_team_home     = item.scaleX * item.width;
                let height_team_home    = item.scaleY * item.height;
                let fillRule_team_home  = item.fillRule;

                var m = item.calcTransformMatrix();
                absoluteLeft_team_home  = m[4] - (Math.abs(width_team_home)/2);
                absoluteTop_team_home   = m[5] - (Math.abs(height_team_home)/2) ;

                if(_data[index]){

                    this.canvas.discardActiveObject();

                     //team_home_detail = (template_key_api.key) 
                    this.requestAPI(`/download-file-other-service?link=${_data[index].team_home_detail.logo}`, "GET").then((response) => {
                        if(response.data.error || !response.data){
                            console.log({error: response.data.error});
                            toastr.error('Dữ liệu trả về rỗng!!!')
                            return;
                        }
                        // return response.data.pathfile;
                        fabric.Image.fromURL(`${response.data.pathfile}`, (myImg) => {
                
                            //i create an extra var for to change some image properties
                           
                            myImg.scaleToHeight(height_team_home);
                            myImg.scaleToWidth(width_team_home);
                            myImg.set({ 
                                left: absoluteLeft_team_home, 
                                top: absoluteTop_team_home,
                                clipTo: function (ctx) {
                                    ctx.arc(0, 0, myImg.width/2, 0, Math.PI * 2, true);
                                }
                            });
                            myImg.fillRule = fillRule_team_home;
                            this.canvas.add(myImg); 
                        });

                    })
                }
                listChidObjsGroup.remove(item)

                // if (item) {
                //     console.log({item});
                //     this.canvas.remove(item);
                //     this.canvas.discardActiveObject();
                //     console.log({item});
                // }
                this.canvas.renderAll();
            })
        }
    }, 
    {
        label: 'Logo đội khách',
        id: 8,
        _script: function () {
            return itemObjOfClassName.forEach((item, index) => {
                this.canvas.remove.apply(this.canvas, [item]);

                let absoluteTop_team_away ;
                let absoluteLeft_team_away;

                let width_team_away = item.scaleX * item.width;
                let height_team_away = item.scaleY * item.height;
                let fillRule_team_away = item.fillRule;

                var m = item.calcTransformMatrix();
                absoluteLeft_team_away = m[4] - (Math.abs(width_team_away)/2);
                absoluteTop_team_away = m[5] - (Math.abs(height_team_away)/2) ;
             
                if(_data[index]){
                    this.canvas.discardActiveObject();
                    this.requestAPI(`/download-file-other-service?link=${_data[index].team_away_detail.logo}`, "GET").then((response) => {
                        if(response.data.error || !response.data){
                            console.log({error: response.data.error});
                            toastr.error('Dữ liệu trả về rỗng!!!')
                            return;
                        }
                        // return response.data.pathfile;
                        fabric.Image.fromURL(`${response.data.pathfile}`, (myImg) => {
                            //i create an extra var for to change some image properties
                           
                            myImg.scaleToHeight(height_team_away);
                            myImg.scaleToWidth(width_team_away);
                            myImg.set({ left: absoluteLeft_team_away, top: absoluteTop_team_away,
                                clipTo: function (ctx) {
                                    ctx.arc(0, 0, myImg.width/2, 0, Math.PI * 2, true);
                                }});
                            myImg.fillRule = fillRule_team_away;
                            this.canvas.add(myImg); 
                            this.canvas.remove(item)
                            this.canvas.renderAll();
                        });
                    })
                }
                listChidObjsGroup.remove(item)
            })
        }
    }, 
    // -----------END: Loại có điểm chung: LOGO

    // -----------BEGIN: Loại có điểm chung: FOR -> LẤY DỮ LIỆU
    {
        label: 'Đội nhà',
        id: 5,
        _script: function () {
            return itemObjOfClassName.forEach((item, index) => {
                if(_data[index]){
                    let nameFootball = _data[index].team_home_detail.name;

                    let nameSplice = ""
                    let dem = 0

                    if(nameFootball && nameFootball.length > 16){
                        let lastIndexOf = nameFootball.lastIndexOf(' ')
                        for(let i = 0; i< nameFootball.length; i++) {
                            if(nameFootball[i] == " "){
                                dem++;
                                if(dem == 1){
                                    nameSplice += nameFootball.slice(0,1);
                                    nameSplice += ".";
                                    nameSplice += nameFootball.slice(i+1, i+2);
                                    nameSplice += ".";
                                }
                                else{
                                    if(lastIndexOf == i) {
                                        nameSplice += nameFootball.slice(lastIndexOf + 1, nameFootball.length)
                                    } 
                                    else {
                                        nameSplice += nameFootball.slice(i+1, i+2);
                                        nameSplice += ".";
                                    }
                                }
                            }
                        }
                        item.set('text', `${nameSplice}`)
                    } else {
                        item.set('text', `${_data[index].team_home_detail.name}`)
                    }
                }
            })
        }
    }, 
    {
        label: 'Đội khách',
        id: 7,
        _script: function () {
            return itemObjOfClassName.forEach((item, index) => {
                if(_data[index]){
                    if(_data[index]){
                        let nameFootball = _data[index].team_away_detail.name;

                        let nameSplice = ""
                        let dem = 0

                        if(nameFootball && nameFootball.length > 16){
                            let lastIndexOf = nameFootball.lastIndexOf(' ')
                            for(let i = 0; i< nameFootball.length; i++) {
                                if(nameFootball[i] == " "){
                                    dem++;
                                    if(dem == 1){
                                        nameSplice += nameFootball.slice(0,1);
                                        nameSplice += ".";
                                        nameSplice += nameFootball.slice(i+1, i+2);
                                        nameSplice += ".";
                                    }
                                    else{
                                        if(lastIndexOf == i) {
                                            nameSplice += nameFootball.slice(lastIndexOf + 1, nameFootball.length)
                                        } 
                                        else {
                                            nameSplice += nameFootball.slice(i+1, i+2);
                                            nameSplice += ".";
                                        }
                                    }
                                }
                            }
                            item.set('text', `${nameSplice}`)
                        } else {
                            item.set('text', `${_data[index].team_away_detail.name}`)
                        }
                    }
                }
            })
        }
    }, 
    // -----------BEGIN: Loại có điểm chung: THỜI_GIAN 
    { //✅ đã cover  
        label: 'Thời gian thi đấu',
        id: 3,
        _script: function () {
            return itemObjOfClassName.forEach((item, index) => {
                if(_data[index]) {
                    var time = new Date(_data[index].date_time);
                    let timeConvert = moment(time).format('LT');
                    // itemObjOfClassName[i].set('text', time.toLocaleString())
                    item.set('text', timeConvert)
                }
            })
        }
    }, 
    // -----------END: Loại có điểm chung: THỜI_GIAN
    // ATTR: 'Kết Qủa Thi Đấu' (tất cả như nhau) ✅
    {// ✅đã covr
        label: 'Số penalty đội nhà',
        id: 9,
        _script: function () {
            return itemObjOfClassName.forEach((item, index) => {
                if(_data[index]){
                    ///------------------------------------------------------------------findValByKey(TEMPLATE_KEY_API.key)
                    // _data ~ dataMapTemplate.matchs
                    item.set('text', `${_data[index].penalty_home}`)
                }
            })
        }
    }, 
    {// ✅đã covr
        label: 'Bàn thắng đội nhà',
        id: 10,
        _script: function () {
            return itemObjOfClassName.forEach((item, index) => {
                if(_data[index]){
                    ///------------------------------------------------------------------findValByKey(TEMPLATE_KEY_API.key)
                    // _data ~ dataMapTemplate.matchs
                    item.set('text', `${_data[index].score_home}`)
                }
            })
        }
    }, 
    {// ✅đã covr
        label: 'Số penalty đội khách',
        id: 11,
        _script: function () {
            return itemObjOfClassName.forEach((item, index) => {
                if(_data[index]){
                     ///------------------------------------------------------------------findValByKey(TEMPLATE_KEY_API.key)
                    // _data ~ dataMapTemplate.matchs
                    item.set('text', `${_data[index].penalty_away}`)
                }
            })
        }
    }, 
    {// ✅đã covr
        label: 'Bàn thắng đội khách',
        id: 12,
        _script: function () {
            return itemObjOfClassName.forEach((item, index) => {
                if(_data[index]){
                    ///------------------------------------------------------------------findValByKey(TEMPLATE_KEY_API.key)
                    // _data ~ dataMapTemplate.matchs
                    item.set('text', `${_data[index].score_away}`)
                }
            })
        }
    },
    // Bảng xếp hạng 
    {// ✅đã covr
        label: 'Tên đội bóng',
        id: 13,
        _script: function () {
            return itemObjOfClassName.forEach((item, index) => {
                if(_data[index]){
                    ///------------------------------------------------------------------findValByKey(TEMPLATE_KEY_API.key)
                    // _data ~ dataMapTemplate.data
                    item.set('text', `${_data[index].name}`)
                }
            })
        }
    },
    {// ✅đã covr
        label: 'Điểm',
        id: 14,
        _script: function () {
            return itemObjOfClassName.forEach((item, index) => {
                if(_data[index]){
                    ///------------------------------------------------------------------findValByKey(TEMPLATE_KEY_API.key)
                    // _data ~ dataMapTemplate.data
                    item.set('text', `${_data[index].scores}`)
                }
            })
        }
    },
    {// ✅đã covr
        label: 'Tổng trận đấu',
        id: 15,
        _script: () => itemObjOfClassName.forEach((item, index) => {
            if(_data[index]){
                 ///------------------------------------------------------------------findValByKey(TEMPLATE_KEY_API.key)
                // _data ~ dataMapTemplate.data
                item.set('text', `${_data[index].total_matchs}`)
            }
        })
    },
    {// ✅đã covr
        label: 'Tổng trận thắng',
        id: 16,
        _script: () => itemObjOfClassName.forEach((item, index) => {
            if(_data[index]){
                 ///------------------------------------------------------------------findValByKey(TEMPLATE_KEY_API.key)
                // _data ~ dataMapTemplate.data
                item.set('text', `${_data[index].total_wins}`)
            }
        })
    },
    {// ✅đã covr
        label: 'Tổng trận hòa',
        id: 17,
        _script: () => itemObjOfClassName.forEach((item, index) => {
            if(_data[index]){
                 ///------------------------------------------------------------------findValByKey(TEMPLATE_KEY_API.key)
                // _data ~ dataMapTemplate.data
                item.set('text', `${_data[index].total_equals}`)
            }
        })
    },
    {// ✅đã covr
        label: 'Tổng trận thua',
        id: 18,
        _script: () => {
            return itemObjOfClassName.forEach((item, index) => {
                if(_data[index]){
                    item.set('text', `${_data[index].total_loses}`)
                }
            })
        }
    },
    {// ✅đã covr
        label: 'Tổng thẻ đỏ',
        id: 19,
        _script: () => {
            return itemObjOfClassName.forEach((item, index) => {
                if(_data[index]){
                    item.set('text', `${_data[index].total_red_cards}`)
                }
            })
        }
    },
    {// ✅đã covr
        label: 'Tổng thẻ vàng',
        id: 20,
        _script: () => {
            return itemObjOfClassName.forEach((item, index) => {
                if(_data[index]){
                    item.set('text', `${_data[index].total_yellow_cards}`)
                }
            })
        }
    },
    // -----------END: Loại có điểm chung: FOR -> LẤY DỮ LIỆU
    // TODO Chi tiết trận đấu
    {
        label: 'Tổng thẻ đỏ đội nhà',
        id: 22,
        _script: () => {
            return itemObjOfClassName.forEach((item, index) => {
                let dem = 0
                dataMapTemplate.details.forEach(card => {
                    if(card.type == 'red_card_home')
                        dem ++
                })
                if(dataMapTemplate){
                    item.set('text', `${dem}`)
                }
            })
        }
    },
    {
        label: 'Tổng thẻ vàng đội nhà',
        id: 23,
        _script: () => {
            return itemObjOfClassName.forEach((item, index) => {
                let dem = 0
                dataMapTemplate.details.forEach(card => {
                    if(card.type == 'yellow_card_home')
                        dem ++
                })
                if(dataMapTemplate){
                    item.set('text', `${dem}`)
                }
            })
        }
    },
    {
        label: 'Logo Đội Nhà - Chi Tiết Trận Đấu',
        id: 24,
        _script: () => {
            return itemObjOfClassName.forEach((item, index) => {
                let width_team_home = item.scaleX * item.width;
                let height_team_home = item.scaleY * item.height;
                let fillRule_team_home = item.fillRule;

                let absoluteTop_team_home ;
                let absoluteLeft_team_home;
                var m = item.calcTransformMatrix();
                absoluteLeft_team_home = m[4] - (Math.abs(width_team_home)/2);
                absoluteTop_team_home = m[5] - (Math.abs(height_team_home)/2) ;

                if(dataMapTemplate){
                    this.canvas.discardActiveObject();
                    this.requestAPI(`/download-file-other-service?link=${dataMapTemplate.team_home_logo}`, "GET").then((response) => {
                        if(response.data.error || !response.data){
                            console.log({error: response.data.error});
                            toastr.error('Dữ liệu trả về rỗng!!!')
                            return;
                        }
                        // return response.data.pathfile;
                        fabric.Image.fromURL(`${response.data.pathfile}`, (myImg) => {
                            //i create an extra var for to change some image properties
                           
                            myImg.scaleToHeight(height_team_home);
                            myImg.scaleToWidth(width_team_home);
                            myImg.set({ left: absoluteLeft_team_home, top: absoluteTop_team_home,
                                clipTo: function (ctx) {
                                    ctx.arc(0, 0, myImg.width/2, 0, Math.PI * 2, true);
                                }});
                            myImg.fillRule = fillRule_team_home;
                            this.canvas.add(myImg); 
                            this.canvas.remove.apply(this.canvas, [item]);
                        });
                    })
                    let arrSoccerHome = []

                    for (const type in listTypeGroup) {
                        if(type == "goal_home"){
                            arrSoccerHome = [...arrSoccerHome, ...listTypeGroup[type]]
                        }
                        if(type == "goal_away"){
                            arrSoccerHome = [...arrSoccerHome, ...listTypeGroup[type]]
                        }
                        if(type == "yellow_card_home"){
                            arrSoccerHome = [...arrSoccerHome, ...listTypeGroup[type]]
                        }
                        if(type == "red_card_home"){
                            arrSoccerHome = [...arrSoccerHome, ...listTypeGroup[type]]
                        }
                    }
                    let arrSoccerHomeSort = arrSoccerHome.sort(this.sortAscendingTypeSoccer)
        
                    if(arrSoccerHomeSort) {
                        /**
                         * tìm bottom của group
                         */
                        var m = objectGroupOfTemplate.calcTransformMatrix();

                        /**
                         * m[5]: top(begin) của group
                         * objectGroupOfTemplate.height: chiều cao của group
                         */
                        let bottomOfGroup = m[5] + (objectGroupOfTemplate.height * 1/2);

                        // this.canvas.add(new fabric.IText('HELLO NEYYY', {
                        //     left: 0,
                        //     top: bottomOfGroup,
                        //     fill: 'white',
                        //     fontFamily: 'Arial',
                        //     fontSize: 25,
                        //     originX: 'left',
                        // }))

                        let count = 0
                        arrSoccerHomeSort.forEach((card, index) => {
                            if(card.type == "goal_home" && card.is_own_goal != 1) {
                                let topForRender =  bottomOfGroup + count*35 + 35;

                                this.canvas.add(new fabric.IText(`${card.user_name} ${Number(card.time) == 0 ? '' : `${Number(card.time) / 60}'`}`, {
                                    left: absoluteLeft_team_home + item.width,
                                    top:  topForRender,
                                    fill: 'white',
                                    fontFamily: 'Arial',
                                    fontSize: fontSizePlayer,
                                    textAlign: 'right',
                                    originX: 'right',
                                }))
                                fabric.Image.fromURL('/template/assets/image/football_ic.png', (myImg) => {
                                    myImg.set({ left: absoluteLeft_team_home + item.width + 30, top: topForRender});
                                    myImg.scaleToHeight(25);
                                    myImg.scaleToWidth(25);
                                    this.canvas.add(myImg); 
                                });
                                count += 1
                            }

                            if(card.type == "goal_away" && card.is_own_goal == 1) {
                                let topForRender =  bottomOfGroup + count*35 + 35;

                                this.canvas.add(new fabric.IText(`${card.user_name} ${Number(card.time) == 0 ? '' : `${Number(card.time) / 60}'`} (OG)`, {
                                    left: absoluteLeft_team_home + item.width,
                                    top:topForRender,
                                    fill: 'white',
                                    textAlign: 'right',
                                    fontFamily: 'Arial',
                                    fontSize: fontSizePlayer,
                                    originX: 'right',
                                }))
                                fabric.Image.fromURL('/template/assets/image/football_ic.png', (myImg) => {
                                    myImg.set({ left: absoluteLeft_team_home + item.width + 30, top: topForRender });
                                    myImg.scaleToHeight(25);
                                    myImg.scaleToWidth(25);
                                    this.canvas.add(myImg); 
                                });
                                count += 1
                            }
                                
                            if(card.type == "yellow_card_home") {
                                let topForRender =  bottomOfGroup + count*35 + 35;

                                this.canvas.add(new fabric.IText(`${card.user_name} ${Number(card.time) == 0 ? '' : `${Number(card.time) / 60}'`}`, {
                                    left: absoluteLeft_team_home + item.width,
                                    top: topForRender,
                                    fill: 'white',
                                    fontFamily: 'Arial',
                                    fontSize: fontSizePlayer,
                                    textAlign: 'right',
                                    originX: 'right',
                                }))
                                this.canvas.add(new fabric.Rect({
                                    left: absoluteLeft_team_home + item.width + 40, 
                                    top: topForRender+ 10,
                                    fill: 'yellow',
                                    width: 20,
                                    height: 25,
                                    originX: 'center',
                                    originY: 'center',
                                    strokeWidth: 0
                                }));
                                count += 1
                            }

                            if(card.type == "red_card_home") {
                                let topForRender =  bottomOfGroup + count*35 + 35;

                                this.canvas.add(new fabric.IText(`${card.user_name} ${Number(card.time) == 0 ? '' : `${Number(card.time) / 60}'`}`, {
                                    left: absoluteLeft_team_home + item.width,
                                    top: topForRender,
                                    fill: 'white',
                                    fontFamily: 'Arial',
                                    fontSize: fontSizePlayer,
                                    textAlign: 'right',
                                    originX: 'right',
                                }))
                                this.canvas.add(new fabric.Rect({
                                    left: absoluteLeft_team_home + item.width + 40, 
                                    top: topForRender+ 10,
                                    fill: 'red',
                                    width: 20,
                                    height: 25,
                                    originX: 'center',
                                    originY: 'center',
                                    strokeWidth: 0
                                }));
                                count += 1
                            }
                        })
                    }
                    this.canvas.renderAll();
                }
                listChidObjsGroup.remove(item)
            })
        }
    },
    {
        label: 'Số penalty đội khách',
        id: 25,
        _script: () => {
            return itemObjOfClassName.forEach((item, index) => {
                if(dataMapTemplate){
                    item.set('text', `${dataMapTemplate.penalty_away}`)
                }
            })
        }
    },
    {
        label: 'Tổng thẻ đỏ đội khách',
        id: 26,
        _script: () => {
            return itemObjOfClassName.forEach((item, index) => {
                let dem = 0
                dataMapTemplate.details.forEach(card => {
                    if(card.type == 'red_card_away')
                        dem ++
                })
                if(dataMapTemplate){
                    item.set('text', `${dem}`)
                }
            })
        }
    },
    {
        label: 'Tổng thẻ vàng đội khách',
        id: 27,
        _script: () => {
            return itemObjOfClassName.forEach((item, index) => {
                let dem = 0
                dataMapTemplate.details.forEach(card => {
                    if(card.type == 'yellow_card_away')
                        dem ++
                })
                if(dataMapTemplate){
                    item.set('text', `${dem}`)
                }
            })
        }
    },
    {
        label: 'Logo Đội Khách - Chi Tiết Trận Đấu',
        id: 28,
        _script: () => itemObjOfClassName.forEach((item, index) => {
            let absoluteTop_team_away ;
            let absoluteLeft_team_away;

            let width_team_away = item.scaleX * item.width;
            let height_team_away = item.scaleY * item.height;
            let fillRule_team_away = item.fillRule;

            var m = item.calcTransformMatrix();

            absoluteLeft_team_away = m[4] - (Math.abs(width_team_away)/2);
            absoluteTop_team_away = m[5] - (Math.abs(height_team_away)/2) ;

            var m2 = objectGroupOfTemplate.calcTransformMatrix();
            let bottomOfGroup = m2[5] + (objectGroupOfTemplate.height * 1/2);    
            if(dataMapTemplate){
                this.canvas.discardActiveObject();
                this.requestAPI(`/download-file-other-service?link=${dataMapTemplate.team_away_logo}`, "GET").then((response) => {
                    if(response.data.error || !response.data){
                        console.log({error: response.data.error});
                        toastr.error('Dữ liệu trả về rỗng!!!')
                        return;
                    }
                    fabric.Image.fromURL(`${response.data.pathfile}`, (myImg) => {
                        myImg.scaleToHeight(height_team_away);
                        myImg.scaleToWidth(width_team_away);
                        myImg.set({ left: absoluteLeft_team_away, top: absoluteTop_team_away,
                            clipTo: function (ctx) {
                                ctx.arc(0, 0, myImg.width/2, 0, Math.PI * 2, true);
                            }});
                        myImg.fillRule = fillRule_team_away;
                        this.canvas.add(myImg); 
                        this.canvas.remove(item)
                    });
                })
                let arrSoccerAway = []

                for (const type in listTypeGroup) {
                    if(type == "goal_away"){
                        arrSoccerAway = [...arrSoccerAway, ...listTypeGroup[type]]
                    }
                    if(type == "goal_home"){
                        arrSoccerAway = [...arrSoccerAway, ...listTypeGroup[type]]
                    }
                    if(type == "yellow_card_away"){
                        arrSoccerAway = [...arrSoccerAway, ...listTypeGroup[type]]
                    }
                    if(type == "red_card_away"){
                        arrSoccerAway = [...arrSoccerAway, ...listTypeGroup[type]]
                    }
                }
                let arrSoccerAwaySort = arrSoccerAway.sort(this.sortAscendingTypeSoccer)
                let count = 0;
                if(arrSoccerAwaySort) {
                    arrSoccerAwaySort.forEach((card, index) => {
                        if(card.type == "goal_away" && card.is_own_goal != 1) {
                            let countForRender =  bottomOfGroup + count*35 + 35
                            this.canvas.add(new fabric.IText(`${card.user_name} ${Number(card.time) == 0 ? '' : `${Number(card.time) / 60}'`}`, {
                                left: absoluteLeft_team_away,
                                top: countForRender,
                                fill: 'white',
                                textAlign: 'left',
                                fontFamily: 'Arial',
                                fontSize: fontSizePlayer,
                                originX: 'left',
                            }))
                            fabric.Image.fromURL('/template/assets/image/football_ic.png', (myImg) => {
                                myImg.set({ left: absoluteLeft_team_away - 50, top: countForRender });
                                myImg.scaleToHeight(25);
                                myImg.scaleToWidth(25);
                                this.canvas.add(myImg); 
                            });
                            count++;
                        }

                        if(card.type == "goal_home" && card.is_own_goal == 1) {
                            let countForRender =  bottomOfGroup + count*35 + 35

                            this.canvas.add(new fabric.IText(`${card.user_name} ${Number(card.time) == 0 ? '' : `${Number(card.time) / 60}'`} (OG)`, {
                                left: absoluteLeft_team_away,
                                top: countForRender,
                                fill: 'white',
                                textAlign: 'left',
                                fontFamily: 'Arial',
                                fontSize: fontSizePlayer,
                                originX: 'left',
                            }))
                            fabric.Image.fromURL('/template/assets/image/football_ic.png', (myImg) => {
                                myImg.set({ left: absoluteLeft_team_away - 50, top: countForRender });
                                myImg.scaleToHeight(25);
                                myImg.scaleToWidth(25);
                                this.canvas.add(myImg); 
                            });
                            count++;
                        }

                        if(card.type == "yellow_card_away") {
                            let countForRender =  bottomOfGroup + count*35 + 35

                            this.canvas.add(new fabric.IText(`${card.user_name} ${Number(card.time) == 0 ? '' : `${Number(card.time) / 60}'`}`, {
                                left: absoluteLeft_team_away,
                                top: countForRender ,
                                fill: 'white',
                                fontFamily: 'Arial',
                                fontSize: fontSizePlayer,
                                textAlign: 'left',
                                originX: 'left',
                            }))
                            this.canvas.add(new fabric.Rect({
                                left: absoluteLeft_team_away - 40, 
                                top: countForRender + 10,
                                fill: 'yellow',
                                width: 20,
                                height: 25,
                                originX: 'center',
                                originY: 'center',
                                strokeWidth: 0
                            }));
                            count++;
                        }

                        if(card.type == "red_card_away") {
                            let countForRender =  bottomOfGroup + count*35 + 35
                            this.canvas.add(new fabric.IText(`${card.user_name} ${Number(card.time) == 0 ? '' : `${Number(card.time) / 60}'`}`, {
                                left: absoluteLeft_team_away,
                                top: countForRender ,
                                fill: 'white',
                                fontFamily: 'Arial',
                                fontSize: fontSizePlayer,
                                textAlign: 'left',
                                originX: 'left',
                            }))
                            this.canvas.add(new fabric.Rect({
                                left: absoluteLeft_team_away - 40, 
                                top: countForRender + 10,
                                fill: 'red',
                                width: 20,
                                height: 25,
                                originX: 'center',
                                originY: 'center',
                                strokeWidth: 0
                            }));
                            count++;
                        }
                    })
                }

                this.canvas.renderAll();
            }

            listChidObjsGroup.remove(item)
        })
    },
]

exports.ADMIN_ACCESS    = [0];
exports.EDITOR_ACCESS   = [0,1];

exports.BROWSER_ACCESS 	= 1;
exports.MOBILE_ACCESS  	= 2;

exports.TYPE_RESPONSE 	= {
    NOT_PROVIDE_TOKEN: 1,
    TOKEN_INVALID: 2,
    PERMISSION_DENIED: 3,
}

