"use strict";

const moment = require('moment');

/**
 * BASE
 */
const BaseModel 					= require('./intalize/base_model');
const { TYPE_RULE_WITH_KEY } 		= require('../config/cf_constants')
const timeUtils 					= require('../utils/time_utils');

/**
 * COLLECTIONS
 */
const HISTORY_COLL 					= require('../database/history-coll');


class Model extends BaseModel {
    constructor() {
        super();
    }

	getListHistoryReport({ fromDate, toDate }){
		return new Promise(async resolve => {
			try {
				const conditionObj = {};

				if(fromDate && toDate){
					conditionObj.modifyAt = {
						$gte: new Date(moment(fromDate).startOf('day').format()),
						$lte: new Date(moment(toDate).endOf('day').format())
					}
				}

				let listHistoryReport = await HISTORY_COLL.aggregate([
                    {
                        $match: conditionObj
                    },
					{
						$group: {
							_id: { $dateToString: { format: "%Y-%m-%d", date: "$modifyAt" } },
							dataFormat: { 
								$first: { 
									$dateToString: { format: "%d/%m/%Y", date: "$modifyAt" }
								}
							},
							totalNewUser: {
								$sum: { $cond: ["$isNewUser", 1, 0] }
							},
							modifyAt: { $first: "$modifyAt" },
							user: { $first: "$user" },
							totalToken: { $sum: "$afterPoint" },
						}
					},
                ]).sort({ _id: -1 });

				return resolve({ 
					code: 200, 
					data: { listHistoryReport }
				});
			} catch (error) {
				console.error(error);
				return resolve({ code: 500, message: error.message });
			}
		})
	}

	getListHistoryGroupByUser({ page, limit = 25 }){
		return new Promise(async resolve => {
			try {
				if(isNaN(page)) page = 1;
				if(isNaN(limit)) limit = 25;

				const skip = (page - 1) * limit;

				let totalRecord = await HISTORY_COLL.aggregate([
					{
						$group: {
							_id: { user: "$user" },
							userID: { $first: "$user" },
							lastHistory: { $last: "$$ROOT" }
						}
					},
					{
						$count: "count"
					}
				]);

				let listHistory = await HISTORY_COLL.aggregate([
					{
						$group: {
							_id: { user: "$user" },
							userID: { $first: "$user" },
							lastHistory: { $last: "$$ROOT" }
						}
					},
					{
						$sort: { createAt: -1 }
					},
					{ 
						"$facet": {
							metadata: [
								{ $count: "recordsTotal" } ,
								{ $addFields: { recordsFiltered: 5 } },
							],
							data: [ { $skip: +skip }, { $limit: +limit } ]
						} 
					}
				])

				let listHistoryProjection = listHistory[0].data.map(async (history, index) => {
					const { userID, lastHistory } = history;

					const infoLastHistory = await HISTORY_COLL
						.findById(lastHistory._id)
						.populate({
							path: 'milestone.item',
							populate: 'campaignID'
						})
						.lean();

					let campaignName = '';
					let milestoneName = '';

					if(infoLastHistory.milestone && infoLastHistory.milestone.item){
						if(infoLastHistory.milestone.item.campaignID){
							campaignName = infoLastHistory.milestone.item.campaignID.campaignName;
						}
						milestoneName = infoLastHistory.milestone.item.milestoneName;
					}

					return {
						index: index + 1,
						userID: `
							<a href="/admin/history/info?user=${userID}">
								<span class="text-warning">
									${userID}
								</span>
							</a>
						`,
						afterPoint: `
							<span class="text-warning">
								${infoLastHistory.afterPoint}
							</span>
						`,
						createAt: moment(infoLastHistory.createAt).format('HH:mm - DD/MM/YYYY'),
						campaignName,
						milestoneName
					}
				})

				listHistoryProjection = await Promise.all(listHistoryProjection);
				// console.log({ __AFTER: listHistoryProjection })

				return resolve({ 
					code: 200,
					recordsTotal: (totalRecord && totalRecord[0] && totalRecord[0].count) || 0,
					recordsFiltered: (totalRecord && totalRecord[0] && totalRecord[0].count) || 0,
					data: listHistoryProjection || []
				});
			} catch (error) {
				console.error(error)
				return resolve({ code: 500, message: error.message });
			}
		})
	}

	getListHistoryPointByUser({ userID, milestoneID, type, fromDate, toDate, keyword, page, limit = 25 }){
		return new Promise(async resolve => {
			try {
				let conditionObj = {};
				userID 		&& (conditionObj.user = userID);
				milestoneID && (conditionObj["milestone.item"] = milestoneID);

				if(fromDate && toDate){
					conditionObj.createAt = {
						$gte: new Date(moment(fromDate).startOf('day').format()),
						$lte: new Date(moment(toDate).endOf('day').format())
					}
				}

				if(keyword){
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [
                        { user: new RegExp(key, 'i') }
                    ]
                }

				if(isNaN(page)) page = 1;
				if(isNaN(limit)) limit = 25;

				const skip = (page - 1) * limit;
				const totalHistoryUser = await HISTORY_COLL.countDocuments(conditionObj);

				let listHistoryPointByUser = await HISTORY_COLL
					.find(conditionObj)
					.populate('milestone.item')
					.skip(+skip)
					.limit(+limit)
					.sort({ "metadata.seasonID": -1, _id: -1 })
					.lean();

				if(!listHistoryPointByUser){
					return resolve({ 
						code: 200,
						recordsTotal: totalHistoryUser,
						recordsFiltered: totalHistoryUser,
						data: [] 
					});
				}

				let listHistoryProjection = listHistoryPointByUser.map((history, index) => {
					const { milestone, metadata } = history;
					let status = '';

					if(metadata && metadata.status){
						status = '<div class="badge badge-custom round badge-secondary">Đang chờ</div>';
					} else{
						status = '<div class="badge badge-custom round badge-success">Đã tích điểm</div>';
					}

					if(type === 'statistic'){
						return {
							index: index + 1,
							userID: history.user,
							currentPoint: `
								<span class="text-warning">
									${history.currentPoint}
								</span>
							`,
							newPoint: `
								<span class="text-warning">
									${history.newPoint}
								</span>
							`,
							afterPoint: `
								<span class="text-warning">
									${history.afterPoint}
								</span>
							`,
							status,
							createAt: moment(history.createAt).format('HH:mm - DD/MM/YYYY'),
						}
					}

					return {
						index: index + 1,
						seasonID: (history.metadata && history.metadata.seasonID) ? history.metadata.seasonID : '',
						rule: TYPE_RULE_WITH_KEY[history.rule].text,
						currentPoint: `
							<span class="text-warning">
								${history.currentPoint}
							</span>
						`,
						newPoint: `
							<span class="text-warning">
								${history.newPoint}
							</span>
						`,
						afterPoint: `
							<span class="text-warning">
								${history.afterPoint}
							</span>
						`,
						status,
						milestoneName: milestone.item && milestone.item.milestoneName,
						createAt: moment(history.createAt).format('HH:mm - DD/MM/YYYY'),
					}
				})

				return resolve({
					code: 200, 
					recordsTotal: totalHistoryUser,
					recordsFiltered: totalHistoryUser,
					data: listHistoryProjection || [] 
				});
			} catch (error) {
				console.error(error);
				return resolve({ code: 500, message: error.message });
			}
		})
	}

	getInfoPointByUser({ userID }){
		return new Promise(async resolve => {
			try {
				if(!userID)
					return resolve({ code: 404, message: "Request Param userID invalid" });

				let lastHistoryPoint = await HISTORY_COLL
					.findOne({ user: userID })
					.populate('milestone.item')
					.sort({ _id: -1 })
					.lean();

				if(!lastHistoryPoint)
					return resolve({ code: 403, message: "Can't get info current point of user" });

				return resolve({ 
					code: 200, 
					data: {
						infoCurrentPoint: {
							userID: lastHistoryPoint.user,
							point: lastHistoryPoint.afterPoint,
						},
						lastHistoryPoint,
					}
				});
			} catch (error) {
				console.error(error);
				return resolve({ code: 500, message: error.message });
			}
		})
	}

	getInfoHistory({ historyID }){
		return new Promise(async resolve => {
			try {
				if(!historyID)
					return resolve({ code: 404, message: "Request Param historyID invalid" });

				let infoHistory = await HISTORY_COLL
					.findById(historyID)
					.populate('milestone.item')
					.sort({ _id: -1 })
					.lean();

				if(!infoHistory)
					return resolve({ code: 403, message: "Can't get info history" });

				return resolve({ code: 200, data: infoHistory });
			} catch (error) {
				console.error(error);
				return resolve({ code: 500, message: error.message });
			}
		})
	}

	getListHistoryGroupBySeason({ page, limit = 25 }){
		return new Promise(async resolve => {
			try {
				if(isNaN(page)) page = 1;
				if(isNaN(limit)) limit = 25;

				const skip = (page - 1) * limit;

				let totalRecord = await HISTORY_COLL.aggregate([
					{
						$group: {
							_id: { post: "$metadata.seasonID" },
							total: { $sum: 1 },
						}
					},
					{
						$count: "count"
					}
				]);

				let listHistory = await HISTORY_COLL.aggregate([
					{
						$match: {
							"metadata.seasonID": { $exists: true }
						}
					},
					{
						$group: {
							_id: { seasonID: "$metadata.seasonID" },
							history: { $first: "$$ROOT" },
						}
					},
					{ $skip: +skip },
					{ $limit: +limit },
					{ $sort: { _id: -1 } },
				])

				let listHistoryProjection = listHistory.map(async ({ history }, index) => {
					const { metadata } = history;

					let totalPoint = await HISTORY_COLL.aggregate([
						{
							$match: {
								"metadata.seasonID": metadata?.seasonID
							}
						},
						{
							$group: {
								_id: null,
								sum: { $sum: "$newPoint" }
							}
						}
					]);

					return {
						index: index + 1,
						seasonID: `
							<a href="/admin/rules?seasonID=${metadata?.seasonID ?? ''}">
								<span class="text-warning">
									${metadata?.seasonID ?? ''}
								</span>
							</a>
						`,
						total: `
							<span class="text-warning">
								${totalPoint[0]?.sum || 0}
							</span>
						`,
						createAt: moment(history.createAt).format('HH:mm - DD/MM/YYYY'),
					}
				})

				listHistoryProjection = await Promise.all(listHistoryProjection);

				return resolve({ 
					code: 200,
					recordsTotal: (totalRecord && totalRecord[0] && totalRecord[0].count) || 0,
					recordsFiltered: (totalRecord && totalRecord[0] && totalRecord[0].count) || 0,
					data: listHistoryProjection || []
				});
			} catch (error) {
				console.error(error)
				return resolve({ code: 500, message: error.message });
			}
		})
	}

	getListHistoryByRuleSeason({ rule, seasonID, fromDate, toDate, keyword, page, limit = 25 }){
		return new Promise(async resolve => {
			try {
				let conditionObj = { rule, "metadata.seasonID": seasonID };

				if(fromDate && toDate){
					conditionObj.createAt = {
						$gte: new Date(moment(fromDate).startOf('day').format()),
						$lte: new Date(moment(toDate).endOf('day').format())
					}
				}

				if(keyword){
                    let key = keyword.split(" ");
                    key = '.*' + key.join(".*") + '.*';

                    conditionObj.$or = [
                        { user: new RegExp(key, 'i') }
                    ]
                }

				if(isNaN(page)) page = 1;
				if(isNaN(limit)) limit = 25;

				console.log({ conditionObj })

				const skip = (page - 1) * limit;
				const totalHistoryUser = await HISTORY_COLL.countDocuments(conditionObj);

				let listHistoryPointByUser = await HISTORY_COLL
					.find(conditionObj)
					.populate('milestone.item')
					.skip(+skip)
					.limit(+limit)
					.sort({ "metadata.seasonID": -1, _id: -1 })
					.lean();

				if(!listHistoryPointByUser){
					return resolve({ 
						code: 200,
						recordsTotal: totalHistoryUser,
						recordsFiltered: totalHistoryUser,
						data: [] 
					});
				}

				let listHistoryProjection = listHistoryPointByUser.map((history, index) => {
					const { metadata } = history;
					let status = '';

					if(metadata && metadata.status){
						status = '<div class="badge badge-custom round badge-secondary">Đang chờ</div>';
					} else{
						status = '<div class="badge badge-custom round badge-success">Đã tích điểm</div>';
					}

					return {
						index: index + 1,
						userID: history.user,
						currentPoint: `
							<span class="text-warning">
								${history.currentPoint}
							</span>
						`,
						newPoint: `
							<span class="text-warning">
								${history.newPoint}
							</span>
						`,
						afterPoint: `
							<span class="text-warning">
								${history.afterPoint}
							</span>
						`,
						status,
						createAt: moment(history.createAt).format('HH:mm - DD/MM/YYYY'),
					}
				})

				return resolve({
					code: 200, 
					recordsTotal: totalHistoryUser,
					recordsFiltered: totalHistoryUser,
					data: listHistoryProjection || [] 
				});
			} catch (error) {
				console.error(error);
				return resolve({ code: 500, message: error.message });
			}
		})
	}

	getLastedHistoryByUser({ userID }){
		return new Promise(async resolve => {
			try {
				if(!userID)
					return resolve({ code: 404, message: "Request Param userID invalid" });

				let infoHistory = await HISTORY_COLL
					.findOne({ user: userID })
					.select('currentPoint newPoint afterPoint')
					.sort({ createAt: -1 })
					.lean();

				if(!infoHistory)
					return resolve({ code: 403, message: "Can't get info history" });

				return resolve({ code: 200, data: infoHistory });
			} catch (error) {
				console.error(error);
				return resolve({ code: 500, message: error.message });
			}
		})
	}

	insert({ rule, currentPoint, newPoint, user }){
		return new Promise(async resolve => {
			try {
				const EXCHANGE_FRUIT = 10; // THay đổi điểm từ V SHOP

				if(!user)
					return resolve({ code: 404, message: "Request Param user invalid" });
				
				if(!rule || rule != EXCHANGE_FRUIT)
					return resolve({ code: 404, message: "Request Param rule invalid" });
				
				if(Number.isNaN(Number(currentPoint))) 
					return resolve({ code: 404, message: "Request Param currentPoint invalid" });

				if(Number.isNaN(Number(newPoint)))
					return resolve({ code: 404, message: "Request Param newPoint invalid" });

				const infoHistoryAfterInserts = await HISTORY_COLL.create({
					currentPoint, rule, user,
					isNewUser: false,
					newPoint: newPoint,
					afterPoint: currentPoint + newPoint,
					createAt: new Date(),
				});

				if(!infoHistoryAfterInserts)
					return resolve({ code: 403, message: "Can't insert history" });

				return resolve({ code: 200, data: infoHistoryAfterInserts });
			} catch (error) {
				console.error(error);
				return resolve({ code: 500, message: error.message });
			}
		})
	}
}

exports.MODEL = new Model;
