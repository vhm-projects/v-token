"use strict";

/**
 * TOOLS
 */
const ObjectID 						= require('mongoose').Types.ObjectId;
const moment						= require('moment');
const lodash						= require('lodash');
const BaseModel 					= require('./intalize/base_model');
const timeUtils 					= require('../utils/time_utils');
const agenda						= require('../config/cf_agenda');
const request						= require('request');
const V_SHOP_DOMAIN                 = process.env.V_SHOP_DOMAIN || 'http://localhost:5000';
/**
 * CONSTANTS
 */
const { TYPE_RULE } 				= require('../config/cf_constants');

/**
 * COLLECTIONS
 */
const HISTORY_COLL 					= require('../database/history-coll');


// CONFIRM SEASON
const { 
	CAMPAIGN__CONFIRM_COLL,
	MILESTONE__CAMPAIGN_CONFIRM_COLL
} = require('../packages/campaign_confirm');

// NEWBORN FOLLOWER
const { 
	CAMPAIGN__NEWBORD_FOLLOWER_COLL,
	MILESTONE__CAMPAIGN_NEWBORD_FOLLOWER_COLL
} = require('../packages/campaign_newbord_follower');

// LIMIT NEWBORN
const {
	CAMPAIGN__LIMIT_NEWBORN_COLL
} = require('../packages/campaign_limit_newborn');

// APPROVE TEAM
const { 
	CAMPAIGN__APPROVE_TEAM_COLL,
	MILESTONE__CAMPAIGN_APPROVE_TEAM_COLL
} = require('../packages/campaign_approve_team');

// CREATE SCHEDULE
const { 
	CAMPAIGN__CREATE_SCHEDULE_COLL,
	MILESTONE__CAMPAIGN_CREATE_SCHEDULE_COLL
} = require('../packages/campaign_create_schedule');

// LIKE POST
const { 
	CAMPAIGN__LIKE_COLL,
	MILESTONE__CAMPAIGN_LIKE_COLL
} = require('../packages/campaign_like');

// AWARDS
const { 
	CAMPAIGN__AWARDS_COLL,
	MILESTONE__CAMPAIGN_AWARDS_COLL
} = require('../packages/campaign_awards');

// CREATE POST
const { 
	CAMPAIGN__CREATE_POST_COLL,
	MILESTONE__CAMPAIGN_CREATE_POST_COLL,
	MILESTONE__POINT_CREATE_POST_COLL
} = require('../packages/campaign_create_post');

// MINIGAME
const { 
	CAMPAIGN__MINIGAME_COLL,
	MILESTONE__CAMPAIGN_MINIGAME_COLL,
} = require('../packages/campaign_minigame');

// UPDATE RESULT
const { 
	CAMPAIGN__UPDATE_RESULT_COLL,
	MILESTONE__CAMPAIGN_UPDATE_RESULT_COLL,
} = require('../packages/campaign_update_result');


// (async function run() {
// 	let client = await MongoClient.connect("mongodb://localhost:27017");
// 	agenda = new Agenda().mongo(client.db("v-token"), "jobs");

// 	await new Promise(resolve => agenda.once('ready', resolve));
// })(); //IIFE func - run now


class Model extends BaseModel {
    constructor() {
        super();

		const runDefineTask = async () => {
			/**
			 * define all EVENT for COMSUMER
			 */
			agenda.define('event_get_point', async (job, done) => {
				console.log(`========================================`);
				console.log(`done event_get_point!!!!`);
				console.log({ __JOB: job.attrs });

				const { milestoneID } = job.attrs.data;

				const listHistoryPending = await HISTORY_COLL
					.find({
						"milestone.item": milestoneID,
						"metadata.status": "pending"
					})
					.sort({ _id: 1 })
					.lean();

				if(listHistoryPending && listHistoryPending.length){
					for (const history of listHistoryPending) {
						let { _id: historyID, rule, user, milestone, metadata } = history;

						let lastCurrentPointOfUser = await HISTORY_COLL
							.findOne({ 
								user: user,
								"metadata.status": { $ne: 'pending' }
							})
							.sort({ _id: -1 })

						let currentPoint = 0;
						let newPoint	 = metadata.point;

						if(lastCurrentPointOfUser){
							currentPoint = lastCurrentPointOfUser.afterPoint;
						}

						delete metadata.status;
						delete metadata.point;

						await HISTORY_COLL.create({
							currentPoint, rule, user, milestone, metadata,
							isNewUser: false,
							newPoint: newPoint,
							afterPoint: currentPoint + newPoint,
							createAt: timeUtils.getCurrentTime()
						})

						const TICH_DIEM = 1;
						let syncPointCustomer = await this.syncPonitVSHOP({ 
							userSQL: user, 
							currentPoint, 
							newPoint, 
							TYPE_REDEM: TICH_DIEM
						});
						

						await HISTORY_COLL.findByIdAndUpdate(historyID, {
							"metadata.status": "updated"
						})
					}
				}

				await agenda.cancel({ _id: job.attrs._id });
				done();
			});

			await new Promise(resolve => agenda.once('ready', resolve));
			agenda.start();
		}

		runDefineTask().catch(error => {
			console.error({ ERROR_DEFINE_TASK: error });
			process.exit(-1);
		});

    }

	/**
	 * @param {userConfirm} Người xác minh giải đấu 
	 * @param {seasonID} Mùa giải
	 * @returns Promise
	 */
	checkRuleCampaignConfirm({ userConfirm, seasonID }){
		return new Promise(async resolve => {
			try {
				const isConfirmed = await HISTORY_COLL.findOne({
					rule: TYPE_RULE.CONFIRM_SEASON,
					"metadata.seasonID": seasonID
				})

				if(isConfirmed)
					return resolve({ code: 403, message: 'Season is confirmed' });

				// INFO CAMPAIGN RUNNING
				const infoCampaignRunning = await CAMPAIGN__CONFIRM_COLL.findOne({
					status: 1,
					dateEnd: { $gte: new Date() }
				});

				if(!infoCampaignRunning)
					return resolve({ code: 403, message: 'Campaign not run or expired' });

				// GET LIST MILESTONE
				const listMilestoneConfirm = await MILESTONE__CAMPAIGN_CONFIRM_COLL
					.find({ campaignID: infoCampaignRunning._id })
					.lean();

				if(!listMilestoneConfirm || !listMilestoneConfirm.length)
					return resolve({ code: 403, message: "Campaign don't have a milestone" });

				// TOTAL HISTORY CAMPAIGN CONFIRM OF USER
				const totalConfirm = await HISTORY_COLL.countDocuments({
					rule: TYPE_RULE.CONFIRM_SEASON,
					user: userConfirm,
				})

				// GET CURRENT MILESTONE OF USER
				let currentMilestone = null;
				for (const milestone of listMilestoneConfirm) {
					const fromNumConfirm = milestone.fromNumConfirm;
					const toNumConfirm   = milestone.toNumConfirm;

					// FIRST TIME CONFIRM
					if(totalConfirm === 0){
						currentMilestone = milestone;
						break;
					}

					// FAKE NEXT TOTAL CONFIRM
					let totalConfirmNext = totalConfirm + 1;
					if(toNumConfirm !== -1){
						if(totalConfirmNext >= fromNumConfirm && totalConfirmNext <= toNumConfirm){
							currentMilestone = milestone;
							break;
						}
					} else{
						if(totalConfirmNext >= fromNumConfirm){
							currentMilestone = milestone;
							break;
						}
					}
				}

				if(!currentMilestone)
					return resolve({ code: 403, message: "Not found current milestone of user" });

				// GET CURRENT POINT OF USER (LAST HISTORY)
				let currentPointOfUser = await HISTORY_COLL
					.findOne({ user: userConfirm })
					.sort({ _id: -1 })

				let isNewUser = true;

				if(currentPointOfUser){
					currentPointOfUser = currentPointOfUser.afterPoint;
					isNewUser = false;
				} else{
					currentPointOfUser = 0;
				}

				const createHistory = await HISTORY_COLL.create({
					rule: TYPE_RULE.CONFIRM_SEASON,
					currentPoint: currentPointOfUser,
					newPoint: currentMilestone.tokenAmount,
					afterPoint: currentPointOfUser + currentMilestone.tokenAmount,
					isNewUser,
					user: userConfirm,
					cumulativeAmount: totalConfirm + 1,
					milestone: {
						kind: 'milestone_campaign_confirm',
						item: currentMilestone._id
					},
					metadata: {
						seasonID,
					},
					createAt: timeUtils.getCurrentTime()
				})

				return resolve({ code: 200, data: createHistory });
			} catch (error) {
				return resolve({ code: 500, message: error.message });
			}
		})
	}

	/**
	 * @param {userID} Người follow giải đấu 
	 * @param {authorID} Author giải đấu 
	 * @param {seasonID} Mùa giải
	 * @returns Promise
	 */
	checkRuleCampaignNewbordFollower({ userID, authorID, seasonID }){
		return new Promise(async resolve => {
			try {
				// GET LAST HISTORY OF USER BY RULE
				const lastHistoryByRule = await HISTORY_COLL.findOne({
					rule: TYPE_RULE.NEWBORD_FOLLOWER,
					user: authorID,
					metadata: {
						userID,
						seasonID
					}
				}).sort({ _id: -1 });

				// User not follow season
				if(!lastHistoryByRule){

					// INFO CAMPAIGN RUNNING
					const infoCampaignRunning = await CAMPAIGN__NEWBORD_FOLLOWER_COLL.findOne({ 
						status: 1,
						dateEnd: { $gte: new Date() }
					});

					if(!infoCampaignRunning)
						return resolve({ code: 403, message: 'Campaign not run or expired' });

					// GET LIST MILESTONE OF CAMPAIGN
					let listMilestoneFollower = await MILESTONE__CAMPAIGN_NEWBORD_FOLLOWER_COLL.find({
						campaignID: infoCampaignRunning._id
					}).lean();

					if(!listMilestoneFollower || !listMilestoneFollower.length)
						return resolve({ code: 403, message: "Campaign don't have a milestone" });

					// GET TOTAL FOLLOWER OF SEASON
					let totalFollowerOfSeason = await HISTORY_COLL.countDocuments({
						rule: TYPE_RULE.NEWBORD_FOLLOWER,
						user: authorID,
						"metadata.seasonID": seasonID,
					});

					// GET CURRENT MILESTONE OF USER
					let currentMilestone = null;
					for (const milestone of listMilestoneFollower) {
						const fromFollower 	= milestone.fromFollower;
						const toFollower 	= milestone.toFollower;

						// FIRST TIME CONFIRM
						if(totalFollowerOfSeason === 0){
							currentMilestone = milestone;
							break;
						}

						// FAKE NEXT TOTAL FOLLOWER
						let totalFollowerNext = totalFollowerOfSeason + 1;
						if(toFollower !== -1){
							if(totalFollowerNext >= fromFollower && totalFollowerNext <= toFollower){
								currentMilestone = milestone;
								break;
							}
						} else{
							if(totalFollowerNext >= fromFollower){
								currentMilestone = milestone;
								break;
							}
						}
					}

					if(!currentMilestone)
						return resolve({ code: 403, message: "Not found current milestone of user" });

					let currentPoint = 0;
					let newPoint 	 = 0;
					let afterPoint 	 = 0;
					let isNewUser 	 = true;

					// GET LIMIT NEWBORN FOLLOW SEASON
					const infoCampaignLimitNewborn = await CAMPAIGN__LIMIT_NEWBORN_COLL.findOne({
						status: 1,
						dateEnd: { $gte: new Date() }
					})

					// TOTAL FOLLOW OF USER WITH SEASON
					const totalFollowOfUserNewborn = await HISTORY_COLL.countDocuments({
						rule: TYPE_RULE.NEWBORD_FOLLOWER,
						user: authorID,
						"metadata.userID": userID
					});

					let currentPointOfUser = await HISTORY_COLL
						.findOne({ user: authorID })
						.sort({ _id: -1 })

					if(currentPointOfUser){
						currentPoint = currentPointOfUser.afterPoint;
						isNewUser = false;
					}

					if(infoCampaignLimitNewborn){
						// If user newborn follow less than or equal limit newborn follow season, then add point
						if(totalFollowOfUserNewborn <= infoCampaignLimitNewborn.limitNewbornFollowSeason){
							newPoint 	= currentMilestone.tokenAmount;
							afterPoint 	= currentPoint + currentMilestone.tokenAmount;
						} else{
							afterPoint 	= currentPoint;
						}
					} else{
						newPoint 	= currentMilestone.tokenAmount;
						afterPoint 	= currentPoint + currentMilestone.tokenAmount;
					}

					let createHistory = await HISTORY_COLL.create({
						rule: TYPE_RULE.NEWBORD_FOLLOWER,
						user: authorID,
						currentPoint,
						newPoint,
						afterPoint,
						isNewUser,
						cumulativeAmount: totalFollowerOfSeason + 1,
						milestone: {
							kind: 'milestone_campaign_newbord_follower',
							item: currentMilestone._id
						},
						metadata: {
							userID,
							seasonID
						},
						createAt: timeUtils.getCurrentTime()
					})

					return resolve({ code: 200, data: createHistory });
				}

				return resolve({ code: 403, message: "User already followed" });
			} catch (error) {
				return resolve({ code: 500, message: error.message });
			}
		})
	}

	/**
	 * @param {authorID} Author giải đấu 
	 * @param {leaguesID} Giải đấu
	 * @param {seasonID} Mùa giải
	 * @param {teamID} Đội tham gia
	 * @returns Promise
	 */
	checkRuleCampaignApproveTeam({ authorID, leagueID, seasonID, teamID }){
		return new Promise(async resolve => {
			try {
				// GET LAST HISTORY OF USER BY RULE
				const lastHistoryByRule = await HISTORY_COLL.findOne({
					rule: TYPE_RULE.APPROVE_TEAM,
					user: authorID,
					"metadata.leagueID": leagueID,
					"metadata.teamID": teamID,
				}).sort({ _id: -1 });

				if(!lastHistoryByRule){
					// INFO CAMPAIGN RUNNING
					const infoCampaignRunning = await CAMPAIGN__APPROVE_TEAM_COLL.findOne({
						status: 1,
						dateEnd: { $gte: new Date() }
					});

					if(!infoCampaignRunning)
						return resolve({ code: 403, message: 'Campaign not run or expired' });

					// GET LIST MILESTONE
					const listMilestones = await MILESTONE__CAMPAIGN_APPROVE_TEAM_COLL
						.find({ campaignID: infoCampaignRunning._id })
						.lean();

					if(!listMilestones || !listMilestones.length)
						return resolve({ code: 403, message: "Campaign don't have a milestone" });

					// TOTAL HISTORY CAMPAIGN APPROVE-TEAM
					const totalApproveTeamForLeagues = await HISTORY_COLL.countDocuments({
						rule: TYPE_RULE.APPROVE_TEAM,
						user: authorID,
						"metadata.leagueID": leagueID,
					})

					// GET CURRENT MILESTONE OF USER
					let currentMilestone = null;
					for (const milestone of listMilestones) {
						const fromNumTeam = milestone.fromNumTeam;
						const toNumTeam   = milestone.toNumTeam;

						// FIRST TIME APPROVE TEAM 
						if(totalApproveTeamForLeagues === 0){
							currentMilestone = milestone;
							break;
						}

						// FAKE NEXT TOTAL APPROVE TEAM OF AUTHOR
						let totalApproveTeamNext = totalApproveTeamForLeagues + 1;
						if(toNumTeam !== -1){
							if(totalApproveTeamNext >= fromNumTeam && totalApproveTeamNext <= toNumTeam){
								currentMilestone = milestone;
								break;
							}
						} else{
							if(totalApproveTeamNext >= fromNumTeam){
								currentMilestone = milestone;
								break;
							}
						}
					}

					if(!currentMilestone)
						return resolve({ code: 403, message: "Not found current milestone of user" });

					// GET CURRENT POINT OF USER (LAST HISTORY)
					let currentPointOfUser = await HISTORY_COLL
						.findOne({ user: authorID })
						.sort({ _id: -1 })

					let isNewUser = true;

					if(currentPointOfUser){
						currentPointOfUser = currentPointOfUser.afterPoint;
						isNewUser = false;
					} else{
						currentPointOfUser = 0;
					}

					const createHistory = await HISTORY_COLL.create({
						rule: TYPE_RULE.APPROVE_TEAM,
						currentPoint: currentPointOfUser,
						newPoint: currentMilestone.tokenAmount,
						afterPoint: currentPointOfUser + currentMilestone.tokenAmount,
						user: authorID,
						isNewUser,
						cumulativeAmount: totalApproveTeamForLeagues + 1,
						milestone: {
							kind: 'milestone_campaign_approve_team',
							item: currentMilestone._id
						},
						metadata: {
							leagueID,
							seasonID,
							teamID
						},
						createAt: timeUtils.getCurrentTime()
					})

					return resolve({ code: 200, data: createHistory });
				}

				return resolve({ code: 403, message: "Team already joined" });
			} catch (error) {
				return resolve({ code: 500, message: error.message });
			}
		})
	}

	/**
	 * @param {authorID} Author giải đấu 
	 * @param {leaguesID} Giải đấu
	 * @param {seasonID} Mùa giải
	 * @returns Promise
	 */
	checkRuleCampaignCreateSchedule({ authorID, leagueID, seasonID }){
		return new Promise(async resolve => {
			try {
				// INFO CAMPAIGN RUNNING
				const infoCampaignRunning = await CAMPAIGN__CREATE_SCHEDULE_COLL.findOne({
					status: 1,
					dateEnd: { $gte: new Date() }
				});

				if(!infoCampaignRunning)
					return resolve({ code: 403, message: 'Campaign not run or expired' });

				// GET LIST MILESTONE
				const listMilestones = await MILESTONE__CAMPAIGN_CREATE_SCHEDULE_COLL
					.find({ campaignID: infoCampaignRunning._id })
					.lean();

				if(!listMilestones || !listMilestones.length)
					return resolve({ code: 403, message: "Campaign don't have a milestone" });

				// TOTAL HISTORY CAMPAIGN CREATE SCHEDULE FOR LEAGUES
				const totalCreateSchedule = await HISTORY_COLL.countDocuments({
					rule: TYPE_RULE.CREATE_SCHEDULE,
					user: authorID,
					"metadata.leagueID": leagueID
				})

				// GET CURRENT MILESTONE OF USER
				let currentMilestone = null;
				for (const milestone of listMilestones) {
					const fromNumCreate = milestone.fromNumCreate;
					const toNumCreate   = milestone.toNumCreate;

					// FIRST TIME APPROVE TEAM 
					if(totalCreateSchedule === 0){
						currentMilestone = milestone;
						break;
					}

					// FAKE NEXT TOTAL APPROVE TEAM OF AUTHOR
					let totalCreateScheduleNext = totalCreateSchedule + 1;
					if(toNumCreate !== -1){
						if(totalCreateScheduleNext >= fromNumCreate && totalCreateScheduleNext <= toNumCreate){
							currentMilestone = milestone;
							break;
						}
					} else{
						if(totalCreateScheduleNext >= fromNumCreate){
							currentMilestone = milestone;
							break;
						}
					}
				}

				if(!currentMilestone)
					return resolve({ code: 403, message: "Not found current milestone of user" });

				// GET CURRENT POINT OF USER (LAST HISTORY)
				let currentPointOfUser = await HISTORY_COLL
					.findOne({ user: authorID })
					.sort({ _id: -1 })

				let isNewUser = true;

				if(currentPointOfUser){
					currentPointOfUser = currentPointOfUser.afterPoint;
					isNewUser = false;
				} else{
					currentPointOfUser = 0;
				}

				const createHistory = await HISTORY_COLL.create({
					rule: TYPE_RULE.CREATE_SCHEDULE,
					currentPoint: currentPointOfUser,
					newPoint: currentMilestone.tokenAmount,
					afterPoint: currentPointOfUser + currentMilestone.tokenAmount,
					user: authorID,
					isNewUser,
					cumulativeAmount: totalCreateSchedule + 1,
					milestone: {
						kind: 'milestone_campaign_create_schedule',
						item: currentMilestone._id
					},
					metadata: {
						leagueID,
						seasonID,
					},
					createAt: timeUtils.getCurrentTime()
				})

				return resolve({ code: 200, data: createHistory });
			} catch (error) {
				return resolve({ code: 500, message: error.message });
			}
		})
	}

	/**
	 * @param {authorID} Author bài viết 
	 * @param {postID} ID bài viết
	 * @param {likerID} Người like bài viết
	 * @returns Promise
	 */
	checkRuleCampaignLikePost({ authorID, postID, likerID }){
		return new Promise(async resolve => {
			try {
				// GET LAST HISTORY OF USER BY RULE
				const lastHistoryByRule = await HISTORY_COLL.findOne({
					rule: TYPE_RULE.LIKE_POST,
					user: authorID,
					"metadata.likerID": likerID,
					"metadata.postID": postID
				}).sort({ _id: -1 });

				// User not like post
				if(!lastHistoryByRule){

					// INFO CAMPAIGN RUNNING
					const infoCampaignRunning = await CAMPAIGN__LIKE_COLL.findOne({ 
						status: 1,
						dateEnd: { $gte: new Date() }
					});

					if(!infoCampaignRunning)
						return resolve({ code: 403, message: 'Campaign not run or expired' });

					// GET LIST MILESTONE OF CAMPAIGN
					let listMilestone = await MILESTONE__CAMPAIGN_LIKE_COLL.find({
						campaignID: infoCampaignRunning._id
					}).lean();

					if(!listMilestone || !listMilestone.length)
						return resolve({ code: 403, message: "Campaign don't have a milestone" });

					// GET TOTAL LIKE OF POST
					let totalLikePost = await HISTORY_COLL.countDocuments({
						rule: TYPE_RULE.LIKE_POST,
						user: authorID,
						"metadata.postID": postID,
					});

					// GET CURRENT MILESTONE OF USER
					let currentMilestone = null;
					for (const milestone of listMilestone) {
						const fromNumLike 	= milestone.fromNumLike;
						const toNumLike 	= milestone.toNumLike;

						// FIRST TIME LIKE
						if(totalLikePost === 0){
							currentMilestone = milestone;
							break;
						}

						// FAKE NEXT TOTAL LIKE
						let totalLikeNext = totalLikePost + 1;
						if(toNumLike !== -1){
							if(totalLikeNext >= fromNumLike && totalLikeNext <= toNumLike){
								currentMilestone = milestone;
								break;
							}
						} else{
							if(totalLikeNext >= fromNumLike){
								currentMilestone = milestone;
								break;
							}
						}
					}

					if(!currentMilestone)
						return resolve({ code: 403, message: "Not found current milestone of user" });

					let isNewUser = true;

					let currentPointOfUser = await HISTORY_COLL
						.findOne({ user: authorID })
						.sort({ _id: -1 })

					if(currentPointOfUser){
						currentPointOfUser = currentPointOfUser.afterPoint;
						isNewUser = false;
					} else{
						currentPointOfUser = 0;
					}

					let createHistory = await HISTORY_COLL.create({
						rule: TYPE_RULE.LIKE_POST,
						user: authorID,
						currentPoint: currentPointOfUser,
						newPoint: currentMilestone.tokenAmount,
						afterPoint: currentPointOfUser + currentMilestone.tokenAmount,
						isNewUser,
						cumulativeAmount: totalLikePost + 1,
						milestone: {
							kind: 'milestone_campaign_like',
							item: currentMilestone._id
						},
						metadata: {
							likerID,
							postID
						},
						createAt: timeUtils.getCurrentTime()
					})

					return resolve({ code: 200, data: createHistory });
				}

				return resolve({ code: 403, message: "User already liked" });
			} catch (error) {
				return resolve({ code: 500, message: error.message });
			}
		})
	}

	/**
	 * @param {authorID} Author bài viết 
	 * @param {leagueID} Giải đấu
	 * @param {seasonID} Mùa giải
	 * @param {typeReward} Loại trao giải
	 * @returns Promise
	 */
	checkRuleCampaignAwards({ authorID, leagueID, seasonID, typeReward }){
		return new Promise(async resolve => {
			try {
				// CHECK PARAMS
				if(![1,2,3].includes(+typeReward))
					return resolve({ code: 403, message: 'Request params typeReward invalid' });

				// TOTAL AWARDS LEAGUE
				const totalAwardsOfLeague = await HISTORY_COLL.countDocuments({
					rule: TYPE_RULE.AWARDS,
					"metadata.leagueID": leagueID
				});

				// TOTAL AWARDS LEAGUE OF AUTHOR
				const totalAwardsLeagueOfAuthor = await HISTORY_COLL.countDocuments({
					rule: TYPE_RULE.AWARDS,
					user: authorID,
					"metadata.leagueID": leagueID
				});

				if(totalAwardsLeagueOfAuthor === 3 || totalAwardsOfLeague === 3)
					return resolve({ code: 403, message: "League already awards" });

				// INFO CAMPAIGN RUNNING
				const infoCampaignRunning = await CAMPAIGN__AWARDS_COLL.findOne({ 
					status: 1,
					dateEnd: { $gte: new Date() }
				});

				if(!infoCampaignRunning)
					return resolve({ code: 403, message: 'Campaign not run or expired' });

				// GET LIST MILESTONE OF CAMPAIGN
				let listMilestone = await MILESTONE__CAMPAIGN_AWARDS_COLL.find({
					campaignID: infoCampaignRunning._id
				}).lean();

				if(!listMilestone || !listMilestone.length)
					return resolve({ code: 403, message: "Campaign don't have a milestone" });

				const listAwardsLeague = await HISTORY_COLL
					.find({
						rule: TYPE_RULE.AWARDS,
						user: authorID,
						"metadata.leagueID": leagueID
					})
					.select('metadata')
					.lean();

				// CHECK TYPE REWARD EXISTS
				if(listAwardsLeague && listAwardsLeague.length){
					for (const history of listAwardsLeague) {
						if(+history.metadata.typeReward === +typeReward)
							return resolve({ code: 403, message: "Type Reward is exists" })
					}
				}

				let currentPoint 	 = 0;
				let newPoint 	 	 = 0;
				let afterPoint 	 	 = 0;
				let isNewUser 	 	 = true;
				let currentMilestone = null;

				let currentPointOfUser = await HISTORY_COLL
					.findOne({ user: authorID })
					.sort({ _id: -1 })

				if(currentPointOfUser){
					currentPoint = currentPointOfUser.afterPoint;
					afterPoint 	 = currentPointOfUser.afterPoint;
					isNewUser 	 = false;
				}

				// GET TOTAL AWARDS OF AUTHOR
				let listGroupLeague = await HISTORY_COLL.aggregate([
					{
						$match: {
							rule: TYPE_RULE.AWARDS,
							user: authorID
						}
					},
					{
						$group: {
							_id: { league: "$metadata.leagueID" },
							totalAward: { $sum: 1 },
						}
					}
				]);

				let totalAwards = listGroupLeague.filter(leagueGroup => leagueGroup.totalAward === 3).length;

				// GET CURRENT MILESTONE OF USER
				for (const milestone of listMilestone) {
					const fromNumAwards = milestone.fromNumAwards;
					const toNumAwards 	= milestone.toNumAwards;

					// FIRST TIME LIKE
					if(totalAwards === 0){
						currentMilestone = milestone;
						break;
					}

					// FAKE NEXT TOTAL AWARDS
					let totalAwardsNext = totalAwards + 1;
					if(toNumAwards !== -1){
						if(totalAwardsNext >= fromNumAwards && totalAwardsNext <= toNumAwards){
							currentMilestone = milestone;
							break;
						}
					} else{
						if(totalAwardsNext >= fromNumAwards){
							currentMilestone = milestone;
							break;
						}
					}
				}

				if(!currentMilestone)
					return resolve({ code: 403, message: "Not found current milestone of user" });

				// Đã trao thưởng 2 lần
				if(totalAwardsLeagueOfAuthor === 2){
					newPoint 	= currentMilestone.tokenAmount;
					afterPoint 	= currentPoint + currentMilestone.tokenAmount;
				}

				let createHistory = await HISTORY_COLL.create({
					rule: TYPE_RULE.AWARDS,
					user: authorID,
					currentPoint,
					newPoint,
					afterPoint,
					isNewUser,
					milestone: {
						kind: 'milestone_campaign_awards',
						item: currentMilestone && currentMilestone._id
					},
					metadata: {
						leagueID,
						seasonID,
						typeReward
					},
					createAt: timeUtils.getCurrentTime()
				})

				return resolve({ code: 200, data: createHistory });
			} catch (error) {
				return resolve({ code: 500, message: error.message });
			}
		})
	}

	/**
	 * @param {authorID} User tạo bài viết 
	 * @param {postID} ID bài viết
	 * @returns Promise
	 */
	checkRuleCampaignCreatePost({ authorID, postID }){
		return new Promise(async resolve => {
			try {
				// CHECK POST IS CREATED
				const infoPost = await HISTORY_COLL.findOne({
					rule: TYPE_RULE.CREATE_POST,
					user: authorID,
					"metadata.postID": postID,
				});

				if(infoPost)
					return resolve({ code: 403, message: 'Post is created' });

				// INFO CAMPAIGN RUNNING
				const infoCampaignRunning = await CAMPAIGN__CREATE_POST_COLL.findOne({ 
					status: 1,
					dateEnd: { $gte: new Date() }
				});

				if(!infoCampaignRunning)
					return resolve({ code: 403, message: 'Campaign not run or expired' });

				// GET LIST MILESTONE OF CAMPAIGN
				let listMilestone = await MILESTONE__CAMPAIGN_CREATE_POST_COLL.find({
					campaignID: infoCampaignRunning._id
				}).lean();

				if(!listMilestone || !listMilestone.length)
					return resolve({ code: 403, message: "Campaign don't have a milestone" });

				let lastCurrentPointOfUser = await HISTORY_COLL
					.findOne({ user: authorID })
					.sort({ _id: -1 })

				for (const milestone of listMilestone) {
					const { _id: milestoneID, campaignID, numPost, period, continuous, limit, delayTime, tokenAmount } = milestone;
					let newPoint 			= 0;
					let currentPointOfUser 	= 0;

					let lastCurrentPoint = await HISTORY_COLL.findOne({
						user: authorID,
					}).sort({ _id: -1 });

					let lastTimeMilestoneCreatePost = await HISTORY_COLL.findOne({
						rule: TYPE_RULE.CREATE_POST,
						user: authorID,
						"milestone.item": milestoneID
					}).sort({ _id: -1 });

					if(lastCurrentPoint){
						currentPointOfUser = lastCurrentPoint.afterPoint;
					} else if(lastCurrentPointOfUser){
						currentPointOfUser = lastCurrentPointOfUser.afterPoint;
					}

					// Đều đặn mỗi ngày
					if(continuous){
						/**
						 * STEP BY STEP
						 * => Lấy lịch sử cuối theo rule và milestone
						 * => Lấy tổng bài viết đã tạo đều đặn và nhóm theo chuỗi (chain)
						 * => Kiểm tra giới hạn cột mốc
						 * => Chưa đạt giới hạn cột mốc:
						 * --- Kiểm tra hôm nay là ngày tiếp theo hay không
						 * --- Kiểm tra lần cuối tạo bài viết theo milestone
						 * --- Nếu tổng bài viết trong chuỗi bằng chu kì => kết thúc chuỗi
						 * --- Kiểm tra lần cuối tạo bài viết có phải lần kết thúc chuỗi không => qua chuỗi mới
						 * => Đã đạt giới hạn cột mốc:
						 * --- Ngắt chuỗi
						 * => Tạo lịch sử
						 */
						let isContinuous 	= false;
						let isFinalChain 	= false;
						let isToday 		= false;
						let isEndMilestone 	= false;
						let infoMetadata	= {};

						// Lấy chuối lịch sử tạo bài viết theo mốc
						const chainDayPost = await HISTORY_COLL.findOne({
							rule: TYPE_RULE.CREATE_POST,
							user: authorID,
							"milestone.item": milestoneID,
							"metadata.continuous": true,
						}).sort({ _id: -1 })

						let totalHistoryGroupByDay = await HISTORY_COLL.aggregate([
							{
								$match: {
									rule: TYPE_RULE.CREATE_POST,
									user: authorID,
									"milestone.item": ObjectID(milestoneID),
									"metadata.continuous": true,
								}
							},
							{
								$group: {
									_id: { day: "$metadata.day", chain: "$metadata.chain" },
									day: { $first: "$metadata.day" },
									chain: { $first: "$metadata.chain" },
								}
							},
						]);
						totalHistoryGroupByDay = totalHistoryGroupByDay.map(history => history._id);

						let groupChain = lodash(totalHistoryGroupByDay)
							.groupBy('chain')
							.map((items, chain) => ({
								chain,
								day: lodash.map(items, 'day')
							})).value();

						if(groupChain.length === limit){
							isEndMilestone = groupChain.find(history => history.day.length !== period);
							isEndMilestone = isEndMilestone ? false : true;
						}

						if(!isEndMilestone){
							if(chainDayPost){
								const lastDay 	 = new Date(chainDayPost.createAt);
								const currentDay = new Date(); // "2021-09-24"
								const calDate 	 = timeUtils.calculateExpireDate(currentDay, lastDay);

								// Kiểm tra hôm nay có phải là ngày tiếp theo không
								if(calDate === -1){
									const date = lastTimeMilestoneCreatePost.metadata.final ? 1 : chainDayPost.metadata.day + 1;
									const pointMilestone = await MILESTONE__POINT_CREATE_POST_COLL
										.findOne({ milestoneID, date })
										.lean();

									newPoint = pointMilestone ? pointMilestone.point : tokenAmount;
									isContinuous = true;
								}

								if(calDate === 0){
									isContinuous = true;
									isToday		 = true;
								}
							}

							let chain = 1, day = 0;

							if(lastTimeMilestoneCreatePost){
								let totalPostByMilestone = await HISTORY_COLL.aggregate([
									{
										$match: {
											rule: TYPE_RULE.CREATE_POST,
											user: authorID,
											"milestone.item": ObjectID(milestoneID),
											"metadata.chain": lastTimeMilestoneCreatePost.metadata.chain,
											"metadata.continuous": true
										}
									},
									{
										$group: {
											_id: { day: "$metadata.day" },
											total: { $sum: 1 },
										}
									}
								]);
								totalPostByMilestone = (totalPostByMilestone && totalPostByMilestone.length) || 0;

								if(totalPostByMilestone + 1 === period && !isToday){
									isFinalChain = true;
								}

								if(totalPostByMilestone === period && isToday){
									isFinalChain = true;
								}

								chain = lastTimeMilestoneCreatePost.metadata.chain;
								day   = lastTimeMilestoneCreatePost.metadata.day;
							} else{
								// Lần đầu tạo bài viết theo mốc
								const pointMilestone = await MILESTONE__POINT_CREATE_POST_COLL
									.findOne({ milestoneID })
									.lean();

								if(pointMilestone){
									newPoint = pointMilestone.point;
								} else{
									newPoint = tokenAmount;
								}

								isContinuous = true;
							}

							infoMetadata = {
								postID,
								day: isToday ? day : day + 1,
								continuous: isContinuous,
								chain: chain
							}

							// Qua chuỗi mới
							if(lastTimeMilestoneCreatePost && lastTimeMilestoneCreatePost.metadata.final && !isToday){
								isFinalChain 			= false;
								infoMetadata.day 		= 1;
								infoMetadata.continuous = true;
								infoMetadata.chain 		= chain + 1;
							}

							if(isFinalChain){
								infoMetadata.final = isFinalChain;
							}

							// Không đều đặn mỗi ngày
							if(!isContinuous){
								infoMetadata = {
									postID,
									continuous: false
								}
							}

						} else{
							infoMetadata = {
								postID,
								continuous: false
							}
						}

						let jobs = await agenda.jobs({
							"data.campaignID": campaignID.toString(),
                        	"data.milestoneID": milestoneID.toString(),
						})

						if(+delayTime && +newPoint && jobs.length){
							infoMetadata.status = 'pending';
							infoMetadata.point  = newPoint;
							newPoint = 0;
						}

						await HISTORY_COLL.create({
							rule: TYPE_RULE.CREATE_POST,
							user: authorID,
							currentPoint: currentPointOfUser,
							newPoint,
							afterPoint: currentPointOfUser + newPoint,
							isNewUser: lastCurrentPointOfUser ? false : true,
							milestone: {
								kind: 'milestone_campaign_create_post',
								item: milestoneID
							},
							metadata: infoMetadata,
							createAt: timeUtils.getCurrentTime()
						})

					} else{
						/**
						 * STEP BY STEP
						 * => Lấy lịch sử cuối theo rule và milestone
						 * => Lấy tổng bài viết đã tạo hôm nay
						 * => Kiểm tra hôm nay là ngày tiếp theo hay ngày hiện tại
						 * => Check:
						 * --- TH1: 1 ngày / 2 bài, giới hạn 2 lần
						 * --- TH2: 2 ngày / 1 bài, giới hạn 2 lần
						 * => Lấy tổng lịch sử mốc đã tích điểm thành công
						 * => Kiểm tra giới hạn cột mốc
						 * => Tạo lịch sử
						 */
						let infoMetadata = { postID };

						// Lấy chuối lịch sử tạo bài viết theo mốc
						const chainDayPost = await HISTORY_COLL.findOne({
							rule: TYPE_RULE.CREATE_POST,
							user: authorID,
							"milestone.item": milestoneID,
							"metadata.continuous": true,
						}).sort({ createAt: -1 })

						// const dateStart = new Date();
						// dateStart.setUTCHours(0,0,0,0);

						// const dateEnd = new Date();
						// dateEnd.setUTCHours(23,59,59,999);

						const totalPostToday = await HISTORY_COLL.countDocuments({
							rule: TYPE_RULE.CREATE_POST,
							user: authorID,
							"milestone.item": milestoneID,
							"metadata.continuous": true,
							createAt: {
								$gte: new Date(moment(Date.now()).startOf('day').format()),
								$lte: new Date(moment(Date.now()).endOf('day').format())
							}
						})

						// Kiểm tra hôm nay có phải là ngày tiếp theo không
						if(chainDayPost){
							const lastDay 	 = new Date(chainDayPost.createAt);
							const currentDay = new Date();
							const calDate 	 = timeUtils.calculateExpireDate(currentDay, lastDay);

							// Ngày tiếp theo hoặc Ngày hiện tại
							if(calDate === -1 || calDate === 0){
								// 1 ngày / 2 bài
								if(period === 1 && totalPostToday + 1 === numPost){
									newPoint = tokenAmount;
									infoMetadata.done = true;
								}

								// 2 ngày / 1 bài
								if(period > 1 && totalPostToday + 1 === numPost){
									newPoint = tokenAmount;
									infoMetadata.done = true;
								}

								infoMetadata.continuous = true;
							} 
							else{
								infoMetadata.continuous = false;
							}

						} else{
							if(period > 1 && numPost === 1){
								newPoint = tokenAmount;
								infoMetadata.done = true;
							}

							infoMetadata.continuous = true;
						}

						const totalChainMilestone = await HISTORY_COLL.countDocuments({
							rule: TYPE_RULE.CREATE_POST,
							user: authorID,
							"milestone.item": milestoneID,
							"metadata.done": true
						})

						// Đạt giới hạn cột mốc
						if(totalChainMilestone === limit){
							newPoint = 0;
							infoMetadata.continuous = false;
						}

						// console.log({ infoMetadata, totalPostToday, totalChainMilestone })

						await HISTORY_COLL.create({
							rule: TYPE_RULE.CREATE_POST,
							user: authorID,
							currentPoint: currentPointOfUser,
							newPoint,
							afterPoint: currentPointOfUser + newPoint,
							isNewUser: lastCurrentPointOfUser ? false : true,
							milestone: {
								kind: 'milestone_campaign_create_post',
								item: milestoneID
							},
							metadata: infoMetadata,
							createAt: timeUtils.getCurrentTime()
						})
					}

				}

				resolve({ code: 200, message: 'success' });
			} catch (error) {
				resolve({ code: 500, message: error.message });
			}
		})
	}

	/**
	 * @param {authorID} ID Author bình chọn
	 * @param {minigameID} ID Minigame
	 * @param {postID} ID Bài viết
	 * @param {voterID} ID User bình chọn
	 * @returns Promise
	 */
	checkRuleCampaignMinigame({ authorID, minigameID, postID, voterID }){
		return new Promise(async resolve => {
			try {
				if(isNaN(postID))
					return resolve({ code: 403, message: 'Request params postID invalid' });

				if(voterID && isNaN(voterID))
					return resolve({ code: 403, message: 'Request params voterID invalid' });

				voterID = +voterID;
				postID  = +postID;

				// CHECK USER VOTE POST OF AUTHOR IN MINIGAME
				const checkUserVotedPost = await HISTORY_COLL.findOne({
					rule: TYPE_RULE.MINIGAME,
					user: authorID,
					"metadata.minigameID": minigameID,
					"metadata.postID": postID,
					$and: [
						{ "metadata.voterID": voterID },
						{ "metadata.voterID": { $exists: true } }
					]
				}).lean();

				if(checkUserVotedPost)
					return resolve({ code: 403, message: 'User is voted' });

				// CHECK EXISTS POST OF AUTHOR IN MINIGAME
				const checkExistsPost = await HISTORY_COLL.findOne({
					rule: TYPE_RULE.MINIGAME,
					user: authorID,
					"metadata.minigameID": minigameID,
					"metadata.postID": postID
				}).lean();

				if(checkExistsPost && !voterID)
					return resolve({ code: 403, message: 'Post is created' });

				// INFO CAMPAIGN RUNNING
				const infoCampaignRunning = await CAMPAIGN__MINIGAME_COLL.findOne({
					status: 1,
					dateEnd: { $gte: new Date() }
				});

				if(!infoCampaignRunning)
					return resolve({ code: 403, message: 'Campaign not run or expired' });

				// GET LIST MILESTONE OF CAMPAIGN
				let listMilestone = await MILESTONE__CAMPAIGN_MINIGAME_COLL.find({
					campaignID: infoCampaignRunning._id
				}).lean();

				if(!listMilestone || !listMilestone.length)
					return resolve({ code: 403, message: "Campaign don't have a milestone" });

				let totalPost = await HISTORY_COLL.aggregate([
					{
						$match: {
							rule: TYPE_RULE.MINIGAME,
							user: authorID,
							"metadata.minigameID": minigameID,
						}
					},
					{
						$group: {
							_id: { post: "$metadata.postID" },
							total: { $sum: 1 },
						}
					},
				]);

				let totalVote = await HISTORY_COLL.countDocuments({
					rule: TYPE_RULE.MINIGAME,
					user: authorID,
					"metadata.minigameID": minigameID,
					"metadata.voterID": { $exists: true }
				})

				let checkExistMiniGame = await HISTORY_COLL.findOne({
					rule: TYPE_RULE.MINIGAME,
					user: authorID,
					"metadata.minigameID": minigameID,
				}).lean();

				let checkIsNewPost = await HISTORY_COLL.findOne({
					rule: TYPE_RULE.MINIGAME,
					user: authorID,
					"metadata.minigameID": minigameID,
					"metadata.postID": postID,
				}).lean();

				let currentMilestone = null;
				let newPoint 		 = 0;
				let isNewUser 		 = true;
				let isNewPost		 = false;
				let isReachLimit 	 = false;

				if(!checkIsNewPost) isNewPost = true;

				let listHistoryReachedLimit = await HISTORY_COLL.find({
					rule: TYPE_RULE.MINIGAME,
					user: authorID,
					"metadata.minigameID": minigameID,
					"metadata.reached_limit": true
				}).select('milestone').lean();

				let listMilestoneSkip = listHistoryReachedLimit.map(history => history.milestone && history.milestone.item.toString());

				// GET CURRENT MILESTONE OF USER
				for (const milestone of listMilestone) {
					let numVote = milestone.numVote;
					let numPost = milestone.numPost;

					if(listMilestoneSkip.includes(milestone._id.toString())) continue;
					if(!checkExistMiniGame) break;

					totalPost = isNewPost ? totalPost.length + 1 : totalPost.length;

					if(totalVote >= numVote && totalPost >= numPost){
						currentMilestone = milestone;
						break;
					}
				}

				let currentPointOfUser = await HISTORY_COLL
					.findOne({ user: authorID })
					.sort({ _id: -1 })

				if(currentPointOfUser){
					currentPointOfUser = currentPointOfUser.afterPoint;
					isNewUser = false;
				} else{
					currentPointOfUser = 0;
				}

				if(!currentMilestone){
					await HISTORY_COLL.create({
						rule: TYPE_RULE.MINIGAME,
						user: authorID,
						currentPoint: currentPointOfUser,
						newPoint: 0,
						afterPoint: currentPointOfUser,
						isNewUser,
						milestone: {
							kind: 'milestone_campaign_minigame',
							item: null
						},
						metadata: {
							minigameID,
							postID,
							voterID: voterID
						},
						createAt: timeUtils.getCurrentTime()
					})

					return resolve({ code: 200, message: 'success' });
				}

				const totalScoreMilestone = await HISTORY_COLL.countDocuments({
					rule: TYPE_RULE.MINIGAME,
					user: authorID,
					"milestone.item": currentMilestone._id,
					newPoint: { $ne: 0 }
				})

				// Giới hạn tích điểm
				if(totalScoreMilestone < currentMilestone.limit){
					newPoint = currentMilestone.tokenAmount;
				}

				if(totalScoreMilestone + 1 === currentMilestone.limit){
					isReachLimit = true;
				}

				const infoMeta = {
					minigameID,
					postID,
					voterID
				}

				if(isReachLimit){
					infoMeta.reached_limit = isReachLimit;
				}

				await HISTORY_COLL.create({
					rule: TYPE_RULE.MINIGAME,
					user: authorID,
					currentPoint: currentPointOfUser,
					newPoint: newPoint,
					afterPoint: currentPointOfUser + newPoint,
					isNewUser,
					milestone: {
						kind: 'milestone_campaign_minigame',
						item: currentMilestone._id
					},
					metadata: infoMeta,
					createAt: timeUtils.getCurrentTime()
				})

				return resolve({ code: 200, message: 'success' });
			} catch (error) {
				return resolve({ code: 500, message: error.message });
			}
		})
	}

	/**
	 * @param {authorID} ID Author giải đấu
	 * @param {leagueID} ID Giải đấu
	 * @param {seasonID} ID Mùa giải
	 * @param {matchID} ID Trận đấu
	 * @returns Promise
	 */
	checkRuleCampaignUpdateResult({ authorID, leagueID, seasonID, matchID }){
		return new Promise(async resolve => {
			try {
				// CHECK UPDATED MATCH
				const checkUpdateMatch = await HISTORY_COLL.findOne({
					rule: TYPE_RULE.UPDATE_RESULT,
					user: authorID,
					"metadata.leagueID": leagueID,
					"metadata.seasonID": seasonID,
					"metadata.matchID": matchID
				}).lean();

				if(checkUpdateMatch)
					return resolve({ code: 403, message: 'Match is updated' });

				// INFO CAMPAIGN RUNNING
				const infoCampaignRunning = await CAMPAIGN__UPDATE_RESULT_COLL.findOne({ 
					status: 1,
					dateEnd: { $gte: new Date() }
				});

				if(!infoCampaignRunning)
					return resolve({ code: 403, message: 'Campaign not run or expired' });

				// GET LIST MILESTONE OF CAMPAIGN
				const listMilestone = await MILESTONE__CAMPAIGN_UPDATE_RESULT_COLL.find({
					campaignID: infoCampaignRunning._id
				}).lean();

				if(!listMilestone || !listMilestone.length)
					return resolve({ code: 403, message: "Campaign don't have a milestone" });

				// INIT VARIABLE
				let currentMilestone = null;
				let newPoint 		 = 0;
				let lastDayOfWeek	 = '';
				let isNewUser 		 = true;

				const totalMatchUpdated = await HISTORY_COLL.countDocuments({
					rule: TYPE_RULE.UPDATE_RESULT,
					user: authorID,
					"metadata.leagueID": leagueID,
					"metadata.seasonID": seasonID,
					"metadata.skip": false
				})

				for (const milestone of listMilestone) {
					const numMinMatch = milestone.numMinMatch;

					if(totalMatchUpdated + 1 === numMinMatch){
						currentMilestone = milestone;
					}
				}

				const lastMatchUpdated = await HISTORY_COLL.findOne({
					rule: TYPE_RULE.UPDATE_RESULT,
					user: authorID,
					"metadata.leagueID": leagueID,
					"metadata.seasonID": seasonID,
				}).sort({ _id: -1 });

				let currentPointOfUser = await HISTORY_COLL
					.findOne({ user: authorID })
					.sort({ _id: -1 })

				if(currentPointOfUser){
					currentPointOfUser = currentPointOfUser.afterPoint;
					isNewUser = false;
				} else{
					currentPointOfUser = 0;
				}

				if(!lastMatchUpdated){
					lastDayOfWeek = timeUtils.endOfWeek();
				} else{
					lastDayOfWeek = lastMatchUpdated.metadata.lastDayOfWeek;
				}

				let currentDay = new Date(moment().startOf('day').format());
				let expireDay  = new Date(moment(lastDayOfWeek).startOf('day').format());
				let calDate    = timeUtils.calculateExpireDate(currentDay, expireDay);

				// Quá tuần
				if(calDate < 0){
					await HISTORY_COLL.updateMany({
						rule: TYPE_RULE.UPDATE_RESULT,
						user: authorID,
						"metadata.leagueID": leagueID,
						"metadata.seasonID": seasonID,
						"metadata.skip": false
					}, {
						$set: {
							"metadata.skip": true
						}
					})
				} else{
					if(currentMilestone){
						newPoint = currentMilestone.numMatchGetPoint * currentMilestone.tokenAmount;
					}
				}

				// console.log({ lastDayOfWeek, currentDay, expireDay, calDate });

				await HISTORY_COLL.create({
					rule: TYPE_RULE.UPDATE_RESULT,
					user: authorID,
					currentPoint: currentPointOfUser,
					newPoint: newPoint,
					afterPoint: currentPointOfUser + newPoint,
					isNewUser,
					milestone: {
						kind: 'milestone_campaign_update_result',
						item: currentMilestone && currentMilestone._id
					},
					metadata: {
						leagueID,
						seasonID,
						matchID,
						skip: false,
						lastDayOfWeek
					},
					createAt: timeUtils.getCurrentTime()
				})

				return resolve({ code: 200, message: 'success' });
			} catch (error) {
				return resolve({ code: 500, message: error.message });
			}
		})
	}
	/**
	 * @param {userSQL} ID Customer SQL
	 * @param {currentPoint} Điểm hiện tại
	 * @param {newPoint} Điểm được cộng
	 * @param {TYPE_REDEM} loại điểm || 1: TÍCH ĐIỂM/ 2: ĐỔI ĐIỂM
	 * @returns Promise
	 */
	syncPonitVSHOP({ userSQL, currentPoint, newPoint, TYPE_REDEM }){
		return new Promise(async resolve => {
			try {
				var options = {
					'method': 'POST',
					'url': `${V_SHOP_DOMAIN}/api/customer/update-customer-point`,
					'headers': {
					  'Content-Type': 'application/x-www-form-urlencoded',
					  "postman_test_sandbox": "ldk_postman",
					  "user-agent": "VTOKEN",
					},
					form: {
					  'userSQL': userSQL,
					  'currentPoint': currentPoint,
					  'newPoint': newPoint,
					  'TYPE_REDEM': TYPE_REDEM
					}
				};
				request(options, function (error, response) {
				if (error) throw new Error(error);
					console.log(response.body);
					return resolve({ error: false, data: response.body })
				});

				return resolve({ code: 200, message: 'success' });
			} catch (error) {
				return resolve({ code: 500, message: error.message });
			}
		})
	}

}

exports.MODEL = new Model;
