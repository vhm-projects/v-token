const CAMPAIGN__UPDATE_RESULT_MODEL 	  		= require('./models/campaign_update_result').MODEL;
const CAMPAIGN__UPDATE_RESULT_COLL  	  		= require('./databases/campaign_update_result-coll');
const CAMPAIGN__UPDATE_RESULT_ROUTES         	= require('./apis/campaign_update_result');
const { CF_ROUTINGS_CAMPAIGN_UPDATE_RESULT } 	= require('./constants/campaign_update_result.uri');

const MILESTONE__CAMPAIGN_UPDATE_RESULT_MODEL 	= require('./models/milestone_campaign_update_result').MODEL;
const MILESTONE__CAMPAIGN_UPDATE_RESULT_COLL  	= require('./databases/milestone_update_result-coll');

module.exports = {
	CAMPAIGN__UPDATE_RESULT_MODEL,
	CAMPAIGN__UPDATE_RESULT_COLL,
	CAMPAIGN__UPDATE_RESULT_ROUTES,

	MILESTONE__CAMPAIGN_UPDATE_RESULT_MODEL,
	MILESTONE__CAMPAIGN_UPDATE_RESULT_COLL,

	CF_ROUTINGS_CAMPAIGN_UPDATE_RESULT,
}
