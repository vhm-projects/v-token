const BASE_ROUTE = '/admin';

const CF_ROUTINGS_CAMPAIGN_UPDATE_RESULT = {
	// CHAMPAIGN UPDATE-RESULT
	ADD_CAMPAIGN_UPDATE_RESULT:    `${BASE_ROUTE}/add-campaign-update-result`,
	UPDATE_CAMPAIGN_UPDATE_RESULT: `${BASE_ROUTE}/update-campaign-update-result`,
	INFO_CAMPAIGN_UPDATE_RESULT:   `${BASE_ROUTE}/info-campaign-update-result`,
    DELETE_CAMPAIGN_UPDATE_RESULT: `${BASE_ROUTE}/delete-campaign-update-result`,
    GET_LIST_CAMPAIGN_UPDATE_RESULT: `${BASE_ROUTE}/list-campaign-update-result`,

	// MILESTONE
	ADD_MILESTONE_CAMPAIGN_UPDATE_RESULT:    `${BASE_ROUTE}/add-milestone-campaign-update-result`,
	UPDATE_MILESTONE_CAMPAIGN_UPDATE_RESULT: `${BASE_ROUTE}/update-milestone-campaign-update-result`,
	INFO_MILESTONE_CAMPAIGN_UPDATE_RESULT:   `${BASE_ROUTE}/info-milestone-campaign-update-result`,
    DELETE_MILESTONE_CAMPAIGN_UPDATE_RESULT: `${BASE_ROUTE}/delete-milestone-campaign-update-result`,
    GET_LIST_MILESTONE_CAMPAIGN_UPDATE_RESULT: `${BASE_ROUTE}/list-milestone-campaign-update-result`,

    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_CAMPAIGN_UPDATE_RESULT = CF_ROUTINGS_CAMPAIGN_UPDATE_RESULT;
