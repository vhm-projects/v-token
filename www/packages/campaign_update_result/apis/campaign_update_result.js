"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path 										= require('path');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                       		= require('../../../routing/child_routing');
const roles                             		= require('../../../config/cf_role');
const { CF_ROUTINGS_CAMPAIGN_UPDATE_RESULT }	= require('../constants/campaign_update_result.uri');

/**
 * MODELS
 */
const CAMPAIGN__UPDATE_RESULT_MODEL				= require('../models/campaign_update_result').MODEL;
const MILESTONE__CAMPAIGN_UPDATE_RESULT_MODEL	= require('../models/milestone_campaign_update_result').MODEL;
const IMAGE_MODEL								= require('../../image/models/image').MODEL;


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * ================ ****************************** ===============
             * ================ QUẢN LÝ CAMPAIGN UPDATE-RESULT ===============
             * ================ ****************************** ===============
             */

			/**
             * Function: Tạo chiến dịch cập nhật kết quả giải đấu (API)
             * Date: 25/08/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_CAMPAIGN_UPDATE_RESULT.ADD_CAMPAIGN_UPDATE_RESULT]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'view',
					title: 'Tạo chiến dịch cập nhật kết quả giải đấu - V-TOKEN',
					code: CF_ROUTINGS_CAMPAIGN_UPDATE_RESULT.ADD_CAMPAIGN_UPDATE_RESULT,
					inc: path.resolve(__dirname, '../views/campaign/add_campaign_update_result.ejs'),
                    view: 'index_admin.ejs'
                },
                methods: {
					get: [ function(req, res){
						ChildRouter.renderToView(req, res);
					}],
                    post: [ async function (req, res) {
                        let { campaignName, dateStart, dateEnd, content, icon } = req.body;

						if(icon){
							const image = await IMAGE_MODEL.insert(icon);
							icon = image && image.data && image.data._id;
						}

                        const infoAfterInsert = await CAMPAIGN__UPDATE_RESULT_MODEL.insert({ 
                            campaignName, dateStart, dateEnd, content, icon
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

			/**
			 * Function: Xóa chiến dịch cập nhật kết quả giải đấu (API)
			 * Date: 25/08/2021
			 * Dev: MinhVH
			 */
			[CF_ROUTINGS_CAMPAIGN_UPDATE_RESULT.DELETE_CAMPAIGN_UPDATE_RESULT]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'json',
                },
                methods: {
                    delete: [ async function (req, res) {
						const { campaignID } = req.query;

                        const infoAfterDelete = await CAMPAIGN__UPDATE_RESULT_MODEL.delete({ campaignID });
                        res.json(infoAfterDelete);
                    }]
                },
            },

             /**
             * Function: Cập nhật chiến dịch cập nhật kết quả giải đấu (API)
             * Date: 25/08/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_CAMPAIGN_UPDATE_RESULT.UPDATE_CAMPAIGN_UPDATE_RESULT]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'view',
					title: 'Cập nhật chiến dịch cập nhật kết quả giải đấu - V-TOKEN',
					code: CF_ROUTINGS_CAMPAIGN_UPDATE_RESULT.UPDATE_CAMPAIGN_UPDATE_RESULT,
					inc: path.resolve(__dirname, '../views/campaign/update_campaign_update_result.ejs'),
                    view: 'index_admin.ejs'
                },
                methods: {
					get: [ async function (req, res) {
						const { campaignID } = req.query;

                        const infoCampaign = await CAMPAIGN__UPDATE_RESULT_MODEL.getInfo({ campaignID });
                        ChildRouter.renderToView(req, res, {
							infoCampaign: infoCampaign.data || {}
						})
                    }],
                    put: [ async function (req, res) {
                        let { campaignID, campaignName, dateStart, dateEnd, content, icon, status } = req.body;

						if(icon){
							const image = await IMAGE_MODEL.insert(icon);
							icon = image && image.data && image.data._id;
						}

                        const infoAfterUpdate = await CAMPAIGN__UPDATE_RESULT_MODEL.update({ 
                            campaignID, campaignName, dateStart, dateEnd, content, icon, status
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

			/**
             * Function: Thông tin chiến dịch cập nhật kết quả giải đấu (API)
             * Date: 25/08/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_CAMPAIGN_UPDATE_RESULT.INFO_CAMPAIGN_UPDATE_RESULT]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
						const { campaignID } = req.query;

                        const infoCampaign = await CAMPAIGN__UPDATE_RESULT_MODEL.getInfo({ campaignID });
                        res.json(infoCampaign);
                    }]
                },
            },

			/**
             * Function: Danh sách chiến dịch cập nhật kết quả giải đấu (API)
             * Date: 25/08/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_CAMPAIGN_UPDATE_RESULT.GET_LIST_CAMPAIGN_UPDATE_RESULT]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'view',
					title: 'Danh sách chiến dịch cập nhật kết quả giải đấu - V-TOKEN',
					code: CF_ROUTINGS_CAMPAIGN_UPDATE_RESULT.GET_LIST_CAMPAIGN_UPDATE_RESULT,
					inc: path.resolve(__dirname, '../views/campaign/list_campaign_update_result.ejs'),
                    view: 'index_admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
						const { status, type } = req.query;
                        const listCampaign = await CAMPAIGN__UPDATE_RESULT_MODEL.getList({ status });

						if(type === 'API'){
							return res.json({ listCampaign: listCampaign.data || [] });
						}

                        ChildRouter.renderToView(req, res, {
							listCampaign: listCampaign.data || []
						});
                    }]
                },
            },


			/**
             * ============ **************************************** ===============
             * ============ QUẢN LÝ MILESTONE CAMPAIGN UPDATE-RESULT ===============
             * ============ **************************************** ===============
             */

			/**
             * Function: Tạo cột mốc chiến dịch cập nhật kết quả giải đấu (API)
             * Date: 25/08/2021
             * Dev: MinhVH
             */
			 [CF_ROUTINGS_CAMPAIGN_UPDATE_RESULT.ADD_MILESTONE_CAMPAIGN_UPDATE_RESULT]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'view',
					title: 'Tạo cột mốc cập nhật kết quả giải đấu - V-TOKEN',
					code: CF_ROUTINGS_CAMPAIGN_UPDATE_RESULT.ADD_MILESTONE_CAMPAIGN_UPDATE_RESULT,
					inc: path.resolve(__dirname, '../views/milestone/add_milestone_update_result.ejs'),
                    view: 'index_admin.ejs'
                },
                methods: {
					get: [ async function(req, res){
						const { campaignID } = req.query;
                        const infoCampaign = await CAMPAIGN__UPDATE_RESULT_MODEL.getInfo({ campaignID });

						ChildRouter.renderToView(req, res, {
							infoCampaign: infoCampaign.data || {}
						});
					}],
                    post: [ async function (req, res) {
                        const { campaignID, milestoneName, numMinMatch, numMatchGetPoint, tokenAmount } = req.body;

                        const infoAfterInsert = await MILESTONE__CAMPAIGN_UPDATE_RESULT_MODEL.insert({ 
                            campaignID, milestoneName, numMinMatch, numMatchGetPoint, tokenAmount
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

			/**
             * Function: Thông tin cột mốc chiến dịch cập nhật kết quả giải đấu (API)
             * Date: 25/08/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_CAMPAIGN_UPDATE_RESULT.UPDATE_MILESTONE_CAMPAIGN_UPDATE_RESULT]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'view',
					title: 'Cập nhật cột mốc cập nhật kết quả giải đấu - V-TOKEN',
					code: CF_ROUTINGS_CAMPAIGN_UPDATE_RESULT.UPDATE_MILESTONE_CAMPAIGN_UPDATE_RESULT,
					inc: path.resolve(__dirname, '../views/milestone/update_milestone_update_result.ejs'),
                    view: 'index_admin.ejs'
                },
                methods: {
					get: [ async function(req, res){
						const { milestoneID, campaignID } = req.query;

                        const infoMilestone = await MILESTONE__CAMPAIGN_UPDATE_RESULT_MODEL.getInfo({ milestoneID });
                        const infoCampaign = await CAMPAIGN__UPDATE_RESULT_MODEL.getInfo({ campaignID });

						ChildRouter.renderToView(req, res, {
							infoMilestone: infoMilestone.data || {},
							infoCampaign: infoCampaign.data || {}
						});
					}],
                    put: [ async function (req, res) {
                        const { milestoneID, milestoneName, numMinMatch, numMatchGetPoint, tokenAmount } = req.body;

                        const infoAfterUpdate = await MILESTONE__CAMPAIGN_UPDATE_RESULT_MODEL.update({ 
                            milestoneID, milestoneName, numMinMatch, numMatchGetPoint, tokenAmount
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

			/**
			 * Function: Xóa cột mốc chiến dịch cập nhật kết quả giải đấu (API)
			 * Date: 25/08/2021
			 * Dev: MinhVH
			 */
			[CF_ROUTINGS_CAMPAIGN_UPDATE_RESULT.DELETE_MILESTONE_CAMPAIGN_UPDATE_RESULT]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'json',
                },
                methods: {
                    delete: [ async function (req, res) {
						const { milestoneID } = req.query;

                        const infoAfterDelete = await MILESTONE__CAMPAIGN_UPDATE_RESULT_MODEL.delete({ milestoneID });
                        res.json(infoAfterDelete);
                    }]
                },
            },


			/**
			 * Function: Thông tin cột mốc chiến dịch cập nhật kết quả giải đấu (API)
			 * Date: 25/08/2021
			 * Dev: MinhVH
			 */
			[CF_ROUTINGS_CAMPAIGN_UPDATE_RESULT.INFO_MILESTONE_CAMPAIGN_UPDATE_RESULT]: {
			config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
						const { milestoneID } = req.query;

                        const infoMilestone = await MILESTONE__CAMPAIGN_UPDATE_RESULT_MODEL.getInfo({ milestoneID });
                        res.json(infoMilestone);
                    }]
                },
            },

			/**
			 * Function: Danh sách cột mốc chiến dịch cập nhật kết quả giải đấu (API)
			 * Date: 25/08/2021
			 * Dev: MinhVH
			 */
			[CF_ROUTINGS_CAMPAIGN_UPDATE_RESULT.GET_LIST_MILESTONE_CAMPAIGN_UPDATE_RESULT]: {
				config: {
					auth: [ roles.role.editor.bin ],
					type: 'view',
					title: 'Danh sách cột mốc cập nhật kết quả giải đấu - V-TOKEN',
					code: CF_ROUTINGS_CAMPAIGN_UPDATE_RESULT.GET_LIST_MILESTONE_CAMPAIGN_UPDATE_RESULT,
					inc: path.resolve(__dirname, '../views/milestone/list_milestone_update_result.ejs'),
                    view: 'index_admin.ejs'
				},
				methods: {
					get: [ async function(req, res){
						const { campaignID, type } = req.query;
                        const infoCampaign = await CAMPAIGN__UPDATE_RESULT_MODEL.getInfo({ campaignID });
						const listMilestone = await MILESTONE__CAMPAIGN_UPDATE_RESULT_MODEL.getList({ campaignID });

						if(type === 'API'){
							return res.json({
								infoCampaign: infoCampaign.data || {},
								listMilestone: listMilestone.data || []
							});
						}

						ChildRouter.renderToView(req, res, {
							infoCampaign: infoCampaign.data || {},
							listMilestone: listMilestone.data || []
						});
					}],
				},
			},

        }
    }
};
