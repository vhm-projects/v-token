"use strict";

const Schema 	= require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

/**
 * COLLECTION MILESTONE CAMPAIGN UPDATE-RESULT CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('milestone_campaign_update_result', {
	milestoneID: {
		type: String,
		unique: true,
		required: true
	},
	campaignID: {
		type: Schema.Types.ObjectId,
		ref: 'campaign_update_result'
	},
	milestoneName: {
		type: String,
		required: true
	},
	numMinMatch: {
		type: Number,
		required: true
	},
	numMatchGetPoint: {
		type: Number,
		required: true
	},
	tokenAmount: {
		type: Number,
		required: true
	},
});
