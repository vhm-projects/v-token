const CAMPAIGN__LIMIT_NEWBORN_MODEL 		 = require('./models/campaign_limit_newborn').MODEL;
const CAMPAIGN__LIMIT_NEWBORN_COLL  		 = require('./databases/campaign_limit_newborn-coll');
const CAMPAIGN__LIMIT_NEWBORN_ROUTES         = require('./apis/campaign_limit_newborn');
const { CF_ROUTINGS_CAMPAIGN_LIMIT_NEWBORN } = require('./constants/campaign_limit_newborn.uri');

module.exports = {
	CAMPAIGN__LIMIT_NEWBORN_MODEL,
	CAMPAIGN__LIMIT_NEWBORN_COLL,
	CAMPAIGN__LIMIT_NEWBORN_ROUTES,
	CF_ROUTINGS_CAMPAIGN_LIMIT_NEWBORN,
}
