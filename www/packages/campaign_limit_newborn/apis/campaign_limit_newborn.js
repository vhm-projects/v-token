"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                           	= require('../../../routing/child_routing');
const roles                                 	= require('../../../config/cf_role');
const { CF_ROUTINGS_CAMPAIGN_LIMIT_NEWBORN }	= require('../constants/campaign_limit_newborn.uri');

/**
 * MODELS
 */
const CAMPAIGN__LIMIT_NEWBORN_MODEL				= require('../models/campaign_limit_newborn').MODEL;


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * ================ ****************************** ===============
             * ================ QUẢN LÝ CAMPAIGN LIMIT-NEWBORN ===============
             * ================ ****************************** ===============
             */

			/**
             * Function: Tạo chiến dịch limit newborn (API)
             * Date: 25/08/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_CAMPAIGN_LIMIT_NEWBORN.ADD_CAMPAIGN_LIMIT_NEWBORN]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'view',
					title: 'Tạo chiến dịch giới hạn newborn follow - V-TOKEN',
					code: CF_ROUTINGS_CAMPAIGN_LIMIT_NEWBORN.ADD_CAMPAIGN_LIMIT_NEWBORN,
					inc: path.resolve(__dirname, '../views/campaign/add_campaign_limit_newborn.ejs'),
                    view: 'index_admin.ejs'
                },
                methods: {
					get: [ function(req, res){
						ChildRouter.renderToView(req, res);
					}],
                    post: [ async function (req, res) {
                        const { 
							campaignName, dateStart, dateEnd, limitNewbornFollowSeason, limitNewbornFollowClub
						} = req.body;

                        const infoAfterInsert = await CAMPAIGN__LIMIT_NEWBORN_MODEL.insert({ 
                            campaignName, dateStart, dateEnd, limitNewbornFollowSeason, limitNewbornFollowClub
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

			/**
			 * Function: Xóa chiến dịch limit newborn (API)
			 * Date: 25/08/2021
			 * Dev: MinhVH
			 */
			[CF_ROUTINGS_CAMPAIGN_LIMIT_NEWBORN.DELETE_CAMPAIGN_LIMIT_NEWBORN]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'json',
                },
                methods: {
                    delete: [ async function (req, res) {
						const { campaignID } = req.query;

                        const infoAfterDelete = await CAMPAIGN__LIMIT_NEWBORN_MODEL.delete({ campaignID });
                        res.json(infoAfterDelete);
                    }]
                },
            },

             /**
             * Function: Cập nhật chiến dịch limit newborn (API)
             * Date: 25/08/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_CAMPAIGN_LIMIT_NEWBORN.UPDATE_CAMPAIGN_LIMIT_NEWBORN]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'view',
					title: 'Cập nhật chiến dịch giới hạn newborn follow - V-TOKEN',
					code: CF_ROUTINGS_CAMPAIGN_LIMIT_NEWBORN.UPDATE_CAMPAIGN_LIMIT_NEWBORN,
					inc: path.resolve(__dirname, '../views/campaign/update_campaign_limit_newborn.ejs'),
                    view: 'index_admin.ejs'
                },
                methods: {
					get: [ async function (req, res) {
						const { campaignID } = req.query;

                        const infoCampaign = await CAMPAIGN__LIMIT_NEWBORN_MODEL.getInfo({ campaignID });
                        ChildRouter.renderToView(req, res, {
							infoCampaign: infoCampaign.data || {}
						})
                    }],
                    put: [ async function (req, res) {
                        const { 
							campaignID, campaignName, dateStart, dateEnd, status, 
							limitNewbornFollowSeason, limitNewbornFollowClub
						} = req.body;

                        const infoAfterUpdate = await CAMPAIGN__LIMIT_NEWBORN_MODEL.update({ 
                            campaignID, campaignName, dateStart, dateEnd, status, 
							limitNewbornFollowSeason, limitNewbornFollowClub
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

			/**
             * Function: Thông tin chiến dịch limit newborn (API)
             * Date: 25/08/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_CAMPAIGN_LIMIT_NEWBORN.INFO_CAMPAIGN_LIMIT_NEWBORN]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'view',
					title: 'Thông tin chiến dịch giới hạn newborn follow - V-TOKEN',
					code: CF_ROUTINGS_CAMPAIGN_LIMIT_NEWBORN.INFO_CAMPAIGN_LIMIT_NEWBORN,
					inc: path.resolve(__dirname, '../views/campaign/detail_campaign_limit_newborn.ejs'),
                    view: 'index_admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
						const { campaignID, type } = req.query;

                        const infoCampaign = await CAMPAIGN__LIMIT_NEWBORN_MODEL.getInfo({ campaignID });

						if(type === 'API'){
							return res.json({ infoCampaign: infoCampaign.data || {} });
						}

						ChildRouter.renderToView(req, res, {
							infoCampaign: infoCampaign.data || {}
						})
                    }]
                },
            },

			/**
             * Function: Danh sách chiến dịch limit newborn (API)
             * Date: 25/08/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_CAMPAIGN_LIMIT_NEWBORN.GET_LIST_CAMPAIGN_LIMIT_NEWBORN]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'view',
					title: 'Danh sách chiến dịch giới hạn newborn follow - V-TOKEN',
					code: CF_ROUTINGS_CAMPAIGN_LIMIT_NEWBORN.GET_LIST_CAMPAIGN_LIMIT_NEWBORN,
					inc: path.resolve(__dirname, '../views/campaign/list_campaign_limit_newborn.ejs'),
                    view: 'index_admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
						const { status, type } = req.query;
                        const listCampaign = await CAMPAIGN__LIMIT_NEWBORN_MODEL.getList({ status });

						if(type === 'API'){
							return res.json({ listCampaign: listCampaign.data || [] });
						}

                        ChildRouter.renderToView(req, res, {
							listCampaign: listCampaign.data || []
						});
                    }]
                },
            },


        }
    }
};
