"use strict";

const BASE_COLL = require('../../../database/intalize/base-coll');

/**
 * COLLECTION CAMPAIGN LIMIT NEWBORN CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('campaign_limit_newborn', {
	campaignID: {
		type: String,
		unique: true,
		required: true
	},
	campaignName: {
		type: String,
		required: true
	},
	dateStart: {
		type: Date,
		required: true
	},
	dateEnd: {
		type: Date,
		required: true
	},
	limitNewbornFollowSeason: {
		type: Number,
		required: true
	},
	limitNewbornFollowClub: {
		type: Number,
		required: true
	},
	/**
	 * Trạng thái hoạt động.
	 * 0. Không chạy
	 * 1. Đang chạy
	 * 2. Đã kết thúc
	 */
	status: {
		type: Number,
		default: 0
	},
});
