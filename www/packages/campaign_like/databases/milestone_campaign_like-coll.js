"use strict";

const Schema 	= require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

/**
 * COLLECTION MILESTONE CAMPAIGN LIKE CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('milestone_campaign_like', {
	milestoneID: {
		type: String,
		unique: true,
		required: true
	},
	campaignID: {
		type: Schema.Types.ObjectId,
		ref: 'campaign_like'
	},
	milestoneName: {
		type: String,
		required: true
	},
	fromNumLike: {
		type: Number,
		required: true
	},
	toNumLike: {
		type: Number
	},
	tokenAmount: {
		type: Number,
		required: true
	},
});
