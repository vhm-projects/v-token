"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                       = require('../../../routing/child_routing');
const roles                             = require('../../../config/cf_role');
const { CF_ROUTINGS_CAMPAIGN_LIKE }	    = require('../constants/campaign_like.uri');

/**
 * MODELS
 */
const CAMPAIGN__LIKE_MODEL			    = require('../models/campaign_like').MODEL;
const MILESTONE__CAMPAIGN_LIKE_MODEL	= require('../models/milestone_campaign_like').MODEL;
const IMAGE_MODEL						= require('../../image/models/image').MODEL;


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * ================ ********************* ===============
             * ================ QUẢN LÝ CAMPAIGN LIKE ===============
             * ================ ********************* ===============
             */

			/**
             * Function: Tạo chiến dịch like (API)
             * Date: 25/08/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_CAMPAIGN_LIKE.ADD_CAMPAIGN_LIKE]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'view',
					title: 'Tạo chiến dịch like bài viết - V-TOKEN',
					code: CF_ROUTINGS_CAMPAIGN_LIKE.ADD_CAMPAIGN_LIKE,
					inc: path.resolve(__dirname, '../views/campaign/add_campaign_like.ejs'),
                    view: 'index_admin.ejs'
                },
                methods: {
					get: [ function(req, res){
						ChildRouter.renderToView(req, res);
					}],
                    post: [ async function (req, res) {
                        let { campaignName, dateStart, dateEnd, content, icon } = req.body;

						if(icon){
							const image = await IMAGE_MODEL.insert(icon);
							icon = image && image.data && image.data._id;
						}

                        const infoAfterInsert = await CAMPAIGN__LIKE_MODEL.insert({ 
                            campaignName, dateStart, dateEnd, content, icon
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

			/**
			 * Function: Xóa chiến dịch like (API)
			 * Date: 25/08/2021
			 * Dev: MinhVH
			 */
			[CF_ROUTINGS_CAMPAIGN_LIKE.DELETE_CAMPAIGN_LIKE]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'json',
                },
                methods: {
                    delete: [ async function (req, res) {
						const { campaignID } = req.query;

                        const infoAfterDelete = await CAMPAIGN__LIKE_MODEL.delete({ campaignID });
                        res.json(infoAfterDelete);
                    }]
                },
            },

             /**
             * Function: Cập nhật chiến dịch like (API)
             * Date: 25/08/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_CAMPAIGN_LIKE.UPDATE_CAMPAIGN_LIKE]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'view',
					title: 'Cập nhật chiến dịch like bài viết - V-TOKEN',
					code: CF_ROUTINGS_CAMPAIGN_LIKE.UPDATE_CAMPAIGN_LIKE,
					inc: path.resolve(__dirname, '../views/campaign/update_campaign_like.ejs'),
                    view: 'index_admin.ejs'
                },
                methods: {
					get: [ async function (req, res) {
						const { campaignID } = req.query;

                        const infoCampaign = await CAMPAIGN__LIKE_MODEL.getInfo({ campaignID });
                        ChildRouter.renderToView(req, res, {
							infoCampaign: infoCampaign.data || {}
						})
                    }],
                    put: [ async function (req, res) {
                        let { campaignID, campaignName, dateStart, dateEnd, content, icon, status } = req.body;

						if(icon){
							const image = await IMAGE_MODEL.insert(icon);
							icon = image && image.data && image.data._id;
						}

                        const infoAfterUpdate = await CAMPAIGN__LIKE_MODEL.update({ 
                            campaignID, campaignName, dateStart, dateEnd, content, icon, status
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

			/**
             * Function: Thông tin chiến dịch like (API)
             * Date: 25/08/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_CAMPAIGN_LIKE.INFO_CAMPAIGN_LIKE]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
						const { campaignID } = req.query;

                        const infoCampaign = await CAMPAIGN__LIKE_MODEL.getInfo({ campaignID });
                        res.json(infoCampaign);
                    }]
                },
            },

			/**
             * Function: Danh sách chiến dịch like (API)
             * Date: 25/08/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_CAMPAIGN_LIKE.GET_LIST_CAMPAIGN_LIKE]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'view',
					title: 'Danh sách chiến dịch like bài viết - V-TOKEN',
					code: CF_ROUTINGS_CAMPAIGN_LIKE.GET_LIST_CAMPAIGN_LIKE,
					inc: path.resolve(__dirname, '../views/campaign/list_campaign_like.ejs'),
                    view: 'index_admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
						const { status, type } = req.query;
                        const listCampaign = await CAMPAIGN__LIKE_MODEL.getList({ status });

						if(type === 'API'){
							return res.json({ listCampaign: listCampaign.data || [] });
						}

                        ChildRouter.renderToView(req, res, {
							listCampaign: listCampaign.data || []
						});
                    }]
                },
            },


			/**
             * ============ ******************************* ===============
             * ============ QUẢN LÝ MILESTONE CAMPAIGN LIKE ===============
             * ============ ******************************* ===============
             */

			/**
             * Function: Tạo cột mốc chiến dịch like (API)
             * Date: 25/08/2021
             * Dev: MinhVH
             */
			 [CF_ROUTINGS_CAMPAIGN_LIKE.ADD_MILESTONE_CAMPAIGN_LIKE]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'view',
					title: 'Tạo cột mốc like bài viết - V-TOKEN',
					code: CF_ROUTINGS_CAMPAIGN_LIKE.ADD_MILESTONE_CAMPAIGN_LIKE,
					inc: path.resolve(__dirname, '../views/milestone/add_milestone_like.ejs'),
                    view: 'index_admin.ejs'
                },
                methods: {
					get: [ async function(req, res){
						const { campaignID } = req.query;
                        const infoCampaign = await CAMPAIGN__LIKE_MODEL.getInfo({ campaignID });

						ChildRouter.renderToView(req, res, {
							infoCampaign: infoCampaign.data || {}
						});
					}],
                    post: [ async function (req, res) {
                        const { campaignID, milestoneName, fromNumLike, toNumLike, tokenAmount } = req.body;

                        const infoAfterInsert = await MILESTONE__CAMPAIGN_LIKE_MODEL.insert({ 
                            campaignID, milestoneName, fromNumLike, toNumLike, tokenAmount
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

			/**
             * Function: Thông tin cột mốc chiến dịch like (API)
             * Date: 25/08/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_CAMPAIGN_LIKE.UPDATE_MILESTONE_CAMPAIGN_LIKE]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'view',
					title: 'Cập nhật cột mốc like bài viết - V-TOKEN',
					code: CF_ROUTINGS_CAMPAIGN_LIKE.UPDATE_MILESTONE_CAMPAIGN_LIKE,
					inc: path.resolve(__dirname, '../views/milestone/update_milestone_like.ejs'),
                    view: 'index_admin.ejs'
                },
                methods: {
					get: [ async function(req, res){
						const { milestoneID, campaignID } = req.query;

                        const infoMilestone = await MILESTONE__CAMPAIGN_LIKE_MODEL.getInfo({ milestoneID });
                        const infoCampaign = await CAMPAIGN__LIKE_MODEL.getInfo({ campaignID });

						ChildRouter.renderToView(req, res, {
							infoMilestone: infoMilestone.data || {},
							infoCampaign: infoCampaign.data || {}
						});
					}],
                    put: [ async function (req, res) {
                        const { milestoneID, milestoneName, fromNumLike, toNumLike, tokenAmount } = req.body;

                        const infoAfterUpdate = await MILESTONE__CAMPAIGN_LIKE_MODEL.update({ 
                            milestoneID, milestoneName, fromNumLike, toNumLike, tokenAmount
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

			/**
			 * Function: Xóa cột mốc chiến dịch like (API)
			 * Date: 25/08/2021
			 * Dev: MinhVH
			 */
			[CF_ROUTINGS_CAMPAIGN_LIKE.DELETE_MILESTONE_CAMPAIGN_LIKE]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'json',
                },
                methods: {
                    delete: [ async function (req, res) {
						const { milestoneID } = req.query;

                        const infoAfterDelete = await MILESTONE__CAMPAIGN_LIKE_MODEL.delete({ milestoneID });
                        res.json(infoAfterDelete);
                    }]
                },
            },


			/**
			 * Function: Thông tin cột mốc chiến dịch like (API)
			 * Date: 25/08/2021
			 * Dev: MinhVH
			 */
			[CF_ROUTINGS_CAMPAIGN_LIKE.INFO_MILESTONE_CAMPAIGN_LIKE]: {
			config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
						const { milestoneID } = req.query;

                        const infoMilestone = await MILESTONE__CAMPAIGN_LIKE_MODEL.getInfo({ milestoneID });
                        res.json(infoMilestone);
                    }]
                },
            },

			/**
			 * Function: Danh sách cột mốc chiến dịch like (API)
			 * Date: 25/08/2021
			 * Dev: MinhVH
			 */
			[CF_ROUTINGS_CAMPAIGN_LIKE.GET_LIST_MILESTONE_CAMPAIGN_LIKE]: {
				config: {
					auth: [ roles.role.editor.bin ],
					type: 'view',
					title: 'Danh sách cột mốc like bài viết - V-TOKEN',
					code: CF_ROUTINGS_CAMPAIGN_LIKE.GET_LIST_MILESTONE_CAMPAIGN_LIKE,
					inc: path.resolve(__dirname, '../views/milestone/list_milestone_like.ejs'),
                    view: 'index_admin.ejs'
				},
				methods: {
					get: [ async function(req, res){
						const { campaignID, type } = req.query;
                        const infoCampaign = await CAMPAIGN__LIKE_MODEL.getInfo({ campaignID });
						const listMilestone = await MILESTONE__CAMPAIGN_LIKE_MODEL.getList({ campaignID });

						if(type === 'API'){
							return res.json({
								infoCampaign: infoCampaign.data || {},
								listMilestone: listMilestone.data || []
							});
						}

						ChildRouter.renderToView(req, res, {
							infoCampaign: infoCampaign.data || {},
							listMilestone: listMilestone.data || []
						});
					}],
				},
			},

        }
    }
};
