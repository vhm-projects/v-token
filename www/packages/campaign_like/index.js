const CAMPAIGN__LIKE_MODEL 		   	= require('./models/campaign_like').MODEL;
const CAMPAIGN__LIKE_COLL  		   	= require('./databases/campaign_like-coll');
const CAMPAIGN__LIKE_ROUTES         = require('./apis/campaign_like');
const { CF_ROUTINGS_CAMPAIGN_LIKE } = require('./constants/campaign_like.uri');

const MILESTONE__CAMPAIGN_LIKE_MODEL = require('./models/milestone_campaign_like').MODEL;
const MILESTONE__CAMPAIGN_LIKE_COLL  = require('./databases/milestone_campaign_like-coll');

module.exports = {
	CAMPAIGN__LIKE_MODEL,
	CAMPAIGN__LIKE_COLL,
	CAMPAIGN__LIKE_ROUTES,

	MILESTONE__CAMPAIGN_LIKE_MODEL,
	MILESTONE__CAMPAIGN_LIKE_COLL,

	CF_ROUTINGS_CAMPAIGN_LIKE,
}
