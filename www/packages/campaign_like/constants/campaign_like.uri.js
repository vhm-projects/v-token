const BASE_ROUTE = '/admin';

const CF_ROUTINGS_CAMPAIGN_LIKE = {
	// CHAMPAIGN LIKE
	ADD_CAMPAIGN_LIKE:    `${BASE_ROUTE}/add-campaign-like`,
	UPDATE_CAMPAIGN_LIKE: `${BASE_ROUTE}/update-campaign-like`,
	INFO_CAMPAIGN_LIKE:   `${BASE_ROUTE}/info-campaign-like`,
    DELETE_CAMPAIGN_LIKE: `${BASE_ROUTE}/delete-campaign-like`,
    GET_LIST_CAMPAIGN_LIKE: `${BASE_ROUTE}/list-campaign-like`,

	// MILESTONE
	ADD_MILESTONE_CAMPAIGN_LIKE:    `${BASE_ROUTE}/add-milestone-campaign-like`,
	UPDATE_MILESTONE_CAMPAIGN_LIKE: `${BASE_ROUTE}/update-milestone-campaign-like`,
	INFO_MILESTONE_CAMPAIGN_LIKE:   `${BASE_ROUTE}/info-milestone-campaign-like`,
    DELETE_MILESTONE_CAMPAIGN_LIKE: `${BASE_ROUTE}/delete-milestone-campaign-like`,
    GET_LIST_MILESTONE_CAMPAIGN_LIKE: `${BASE_ROUTE}/list-milestone-campaign-like`,

    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_CAMPAIGN_LIKE = CF_ROUTINGS_CAMPAIGN_LIKE;
