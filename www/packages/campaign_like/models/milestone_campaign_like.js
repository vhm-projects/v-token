"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID                      			= require('mongoose').Types.ObjectId;

/**
 * INTERNAL PACKAGE
 */
const { randomStringNumber } 					= require('../../../utils/string_utils');

/**
 * BASES
 */
const BaseModel 								= require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const MILESTONE__CAMPAIGN_LIKE_COLL	            = require('../databases/milestone_campaign_like-coll');


class Model extends BaseModel {
    constructor() {
        super(MILESTONE__CAMPAIGN_LIKE_COLL);
    }

	checkMilestoneIDExists(milestoneID){
		return new Promise(resolve => {
			(async function recursiveCheckMilestoneID(milestoneID){
				let checkExists = await MILESTONE__CAMPAIGN_LIKE_COLL.findOne({ milestoneID });
				if(checkExists){
					milestoneID = randomStringNumber(5);
					recursiveCheckMilestoneID(milestoneID);
				} else{
					resolve(milestoneID);
				}
			})(milestoneID)
		})
	}

	insert({ campaignID, milestoneName, fromNumLike, toNumLike = -1, tokenAmount }) {
        return new Promise(async resolve => {
            try {
                if(!milestoneName)
                    return resolve({ error: true, message: 'Tên cột mốc chiến dịch không hợp lệ' });

				if(!ObjectID.isValid(campaignID))
                    return resolve({ error: true, message: "ID chiến dịch không hợp lệ" });

                let checkExists = await MILESTONE__CAMPAIGN_LIKE_COLL.findOne({
					milestoneName,
					campaignID
                });
                if(checkExists)
                    return resolve({ error: true, message: "Tên cột mốc chiến dịch đã tồn tại" });

				if(!fromNumLike)
                    return resolve({ error: true, message: "Phạm vi like không hợp lệ" });

				// Convert to number
				fromNumLike = +fromNumLike;
				toNumLike 	= +toNumLike;

				if(fromNumLike > toNumLike && toNumLike !== -1)
                    return resolve({ error: true, message: "Phạm vi like không hợp lệ" });

				let lastMilestone = await MILESTONE__CAMPAIGN_LIKE_COLL
					.findOne({ campaignID })
					.sort({ createAt: -1 })
					.lean();

                if (lastMilestone) {
                    let lastToNumLike = lastMilestone.toNumLike;

                    if(lastToNumLike === -1)
                        return resolve({ error: true, message: "Phạm vi like đã không giới hạn" });
    
                    if(fromNumLike <= lastToNumLike || fromNumLike !== lastToNumLike + 1)
                        return resolve({ error: true, message: "Phạm vi like không hợp lệ" });
                }

				let milestoneID = await this.checkMilestoneIDExists(randomStringNumber(5));
                let dataInsert = {
					campaignID,
					milestoneID,
                    milestoneName, 
                    fromNumLike,
                    toNumLike,
					tokenAmount
                }

                let infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'Không thể tạo cột mốc chiến dịch' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	update({ milestoneID, milestoneName, fromNumLike, toNumLike = -1, tokenAmount }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(milestoneID))
                    return resolve({ error: true, message: "ID cột mốc không hợp lệ" });

                let checkExists = await MILESTONE__CAMPAIGN_LIKE_COLL.findById(milestoneID);
                if(!checkExists)
                    return resolve({ error: true, message: "cột mốc chiến dịch không tồn tại" });

				if(checkExists.milestoneName !== milestoneName){
					let checkNameExist = await MILESTONE__CAMPAIGN_LIKE_COLL.findOne({ 
						campaignID: checkExists.campaignID,
						milestoneName,
					});
					if(checkNameExist) {
						return resolve({ error: true, message: 'Tên cột mốc chiến dịch đã tồn tại' });
					}
				}

				if(+fromNumLike > +toNumLike && +toNumLike !== -1)
                    return resolve({ error: true, message: "Phạm vi like không hợp lệ" });

                let dataUpdateObj = { toNumLike };
                milestoneName 	&& (dataUpdateObj.milestoneName  = milestoneName);
                fromNumLike 	&& (dataUpdateObj.fromNumLike    = fromNumLike);
                tokenAmount		&& (dataUpdateObj.tokenAmount  	 = tokenAmount);

                const infoAfterUpdate = await this.updateWhereClause({ _id: milestoneID }, dataUpdateObj);
				if(!infoAfterUpdate)
					return resolve({ error: true, message: 'Không thể cập nhật cột mốc chiến dịch' });

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getInfo({ milestoneID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(milestoneID))
                    return resolve({ error: true, message: "ID cột mốc không hợp lệ" });

                let infoCampaign = await MILESTONE__CAMPAIGN_LIKE_COLL.findById(milestoneID);
                if(!infoCampaign)
                    return resolve({ error: true, message: "Cột mốc chiến dịch không tồn tại" });

                return resolve({ error: false, data: infoCampaign });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    delete({ milestoneID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(milestoneID))
                    return resolve({ error: true, message: "ID cột mốc không hợp lệ" });

				let infoAfterDelete = await MILESTONE__CAMPAIGN_LIKE_COLL.findByIdAndRemove(milestoneID);

                if(!infoAfterDelete) 
                    return resolve({ error: true, message: "Không thể xoá cột mốc chiến dịch" });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getList({ campaignID }) {
        return new Promise(async resolve => {
            try {
				if(!ObjectID.isValid(campaignID))
					return resolve({ error: true, message: "ID chiến dịch không hợp lệ" });

                let listMilestone = await MILESTONE__CAMPAIGN_LIKE_COLL
					.find({ campaignID })
					.sort({ fromNumLike: -1, modifyAt: -1 })
					.lean();

                if(!listMilestone)
                    return resolve({ error: true, message: "Không thể lấy danh sách cột mốc" });

                return resolve({ error: false, data: listMilestone });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
