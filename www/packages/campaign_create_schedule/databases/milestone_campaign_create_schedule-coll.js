"use strict";

const Schema 	= require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

/**
 * COLLECTION MILESTONE CAMPAIGN CREATE SCHEDULE CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('milestone_campaign_create_schedule', {
	milestoneID: {
		type: String,
		unique: true,
		required: true
	},
	campaignID: {
		type: Schema.Types.ObjectId,
		ref: 'campaign_create_schedule'
	},
	milestoneName: {
		type: String,
		required: true
	},
	fromNumCreate: {
		type: Number,
		required: true
	},
	toNumCreate: {
		type: Number
	},
	tokenAmount: {
		type: Number,
		required: true
	},
});
