const BASE_ROUTE = '/admin';

const CF_ROUTINGS_CAMPAIGN_CREATE_SCHEDULE = {
	// CHAMPAIGN CREATE SCHEDULE
	ADD_CAMPAIGN_CREATE_SCHEDULE:    `${BASE_ROUTE}/add-campaign-create-schedule`,
	UPDATE_CAMPAIGN_CREATE_SCHEDULE: `${BASE_ROUTE}/update-campaign-create-schedule`,
	INFO_CAMPAIGN_CREATE_SCHEDULE:   `${BASE_ROUTE}/info-campaign-create-schedule`,
    DELETE_CAMPAIGN_CREATE_SCHEDULE: `${BASE_ROUTE}/delete-campaign-create-schedule`,
    GET_LIST_CAMPAIGN_CREATE_SCHEDULE: `${BASE_ROUTE}/list-campaign-create-schedule`,

	// MILESTONE
	ADD_MILESTONE_CAMPAIGN_CREATE_SCHEDULE:    `${BASE_ROUTE}/add-milestone-campaign-create-schedule`,
	UPDATE_MILESTONE_CAMPAIGN_CREATE_SCHEDULE: `${BASE_ROUTE}/update-milestone-campaign-create-schedule`,
	INFO_MILESTONE_CAMPAIGN_CREATE_SCHEDULE:   `${BASE_ROUTE}/info-milestone-campaign-create-schedule`,
    DELETE_MILESTONE_CAMPAIGN_CREATE_SCHEDULE: `${BASE_ROUTE}/delete-milestone-campaign-create-schedule`,
    GET_LIST_MILESTONE_CAMPAIGN_CREATE_SCHEDULE: `${BASE_ROUTE}/list-milestone-campaign-create-schedule`,

    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_CAMPAIGN_CREATE_SCHEDULE = CF_ROUTINGS_CAMPAIGN_CREATE_SCHEDULE;
