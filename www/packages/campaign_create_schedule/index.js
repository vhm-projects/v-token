const CAMPAIGN__CREATE_SCHEDULE_MODEL 		   = require('./models/campaign_create_schedule').MODEL;
const CAMPAIGN__CREATE_SCHEDULE_COLL  		   = require('./databases/campaign_create_schedule-coll');
const CAMPAIGN__CREATE_SCHEDULE_ROUTES         = require('./apis/campaign_create_schedule');
const { CF_ROUTINGS_CAMPAIGN_CREATE_SCHEDULE } = require('./constants/campaign_create_schedule.uri');

const MILESTONE__CAMPAIGN_CREATE_SCHEDULE_MODEL = require('./models/milestone_campaign_create_schedule').MODEL;
const MILESTONE__CAMPAIGN_CREATE_SCHEDULE_COLL  = require('./databases/milestone_campaign_create_schedule-coll');

module.exports = {
	CAMPAIGN__CREATE_SCHEDULE_MODEL,
	CAMPAIGN__CREATE_SCHEDULE_COLL,
	CAMPAIGN__CREATE_SCHEDULE_ROUTES,
	CF_ROUTINGS_CAMPAIGN_CREATE_SCHEDULE,

	MILESTONE__CAMPAIGN_CREATE_SCHEDULE_MODEL,
	MILESTONE__CAMPAIGN_CREATE_SCHEDULE_COLL
}
