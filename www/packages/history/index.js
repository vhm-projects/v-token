const HISTORY_ROUTES          	= require('./apis/history');
const { CF_ROUTINGS_HISTORY } 	= require('./constants/history.uri');

module.exports = {
    HISTORY_ROUTES,
    CF_ROUTINGS_HISTORY
}
