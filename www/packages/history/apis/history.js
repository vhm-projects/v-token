"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path 									= require('path');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                           = require('../../../routing/child_routing');
const roles                                 = require('../../../config/cf_role');
const { CF_ROUTINGS_HISTORY } 				= require('../constants/history.uri');
const agenda                                = require('../../../config/cf_agenda');

/**
 * MODELS, COLLECTIONS
 */
const HISTORY_MODEL							= require('../../../models/history').MODEL;
const HISTORY_COLL							= require('../../../database/history-coll');


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * ========================== ************************ ================================
             * ========================== QUẢN LÝ HISTORY - USER   ================================
             * ========================== ************************ ================================
             */

            /**
             * Function: Danh sách điểm  (API)
             * Date: 07/09/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_HISTORY.GET_LIST_HISTORY]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { fromDate, toDate } = req.query;
						const listHistoryPoint = await HISTORY_MODEL.getListHistoryReport({ fromDate, toDate });

                        res.json(listHistoryPoint.data || []);
                    }]
                },
            },

            /**
             * Function: Danh sách điểm  (VIEW, API)
             * Date: 07/09/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_HISTORY.API_GET_LIST_HISTORY_GROUP_BY_USER]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { start, length } = req.query;
                        const page = Number(start)/Number(length) + 1;

						const listHistoryPoint = await HISTORY_MODEL.getListHistoryGroupByUser({ page, limit: length });
                        res.json(listHistoryPoint || [])
                    }]
                },
            },

            /**
             * Function: Danh sách điểm  (VIEW, API)
             * Date: 07/09/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_HISTORY.API_GET_LIST_HISTORY_BY_USER]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { user, type, milestoneID, fromDate, toDate, keyword, start, length } = req.query;
                        const page = Number(start)/Number(length) + 1;

						const listHistoryPoint = await HISTORY_MODEL.getListHistoryPointByUser({ 
							userID: user, type, milestoneID, fromDate, toDate, keyword, page, limit: length 
						});
                        res.json(listHistoryPoint || []);
                    }],
                },
            },

			/**
             * Function: Danh sách điểm  (VIEW, API)
             * Date: 07/09/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_HISTORY.GET_LIST_HISTORY_BY_USER_EMBED]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'view',
					title: 'Danh sách lịch sử user nhận token - V-TOKEN',
					code: 'CF_ROUTINGS_HISTORY.GET_LIST_HISTORY_BY_USER_EMBED',
					inc: path.resolve(__dirname, '../views/list_history_iframe.ejs'),
                    view: 'index_admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        const { user } = req.query;
                        const totalPointOfUser = await HISTORY_COLL.findOne({ user }).sort({ _id: -1 });

						ChildRouter.renderToView(req, res, {
                            totalPointOfUser: totalPointOfUser ? totalPointOfUser.afterPoint : 0
                        })
                    }]
                },
            },

            /**
             * Function: Danh sách điểm theo user  (VIEW, API)
             * Date: 14/09/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_HISTORY.GET_LIST_HISTORY_BY_USER]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'view',
					title: 'Danh sách lịch sử tích điểm theo user - V-TOKEN',
					code: 'CF_ROUTINGS_HISTORY.GET_LIST_HISTORY_BY_USER',
					inc: path.resolve(__dirname, '../views/list_history_by_user.ejs'),
                    view: 'index_admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
						ChildRouter.renderToView(req, res, {
                            listHistoryPoint: []
						})
                    }]
                },
            },

            /**
             * Function: Danh sách điểm  (VIEW, API)
             * Date: 07/09/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_HISTORY.GET_LIST_HISTORY_GROUP_BY_USER]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'view',
					title: 'Danh sách lịch sử user nhận token - V-TOKEN',
					code: 'CF_ROUTINGS_HISTORY.GET_LIST_HISTORY_GROUP_BY_USER',
					inc: path.resolve(__dirname, '../views/list_history.ejs'),
                    view: 'index_admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        const { type } = req.query;
						const listHistoryPoint = await HISTORY_MODEL.getListHistoryGroupByUser({});

                        if(type === 'API'){
                            return res.json({
                                listHistoryPoint: listHistoryPoint.data || []
                            })
                        }

						ChildRouter.renderToView(req, res, {
							listHistoryPoint: listHistoryPoint.data || []
						})
                    }]
                },
            },

            [CF_ROUTINGS_HISTORY.API_ADD_HISTORY_BY_USER]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { rule, currentPoint, newPoint, user } = req.body;
                        const infoHistory = await HISTORY_MODEL.insert({ rule, currentPoint, newPoint, user });
                        res.json(infoHistory);
                    }]
                },
            },

            [CF_ROUTINGS_HISTORY.API_GET_LASTED_HISTORY_BY_USER]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { user } = req.query;
						const infoHistory = await HISTORY_MODEL.getLastedHistoryByUser({ userID: user })
                        res.json(infoHistory.data || {});
                    }]
                },
            },

            [CF_ROUTINGS_HISTORY.API_TEST_UPDATE_HISTORY_AGENDA]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        let timeScheduler = `1 minute`;

                        agenda.schedule(timeScheduler, 'event_get_point', {
                            milestoneID: "",
                        });
                        console.log(`agenda: event_get_point added - ${timeScheduler}`);
                    }]
                },
            },

			/**
             * Function: Thông tin điểm của user (API)
             * Date: 07/09/2021
             * Dev: MinhVH
             */
			// [CF_ROUTINGS_HISTORY.GET_INFO_HISTORY_BY_USER]: {
            //     config: {
            //         auth: [ roles.role.admin.bin ],
            //         type: 'view',
			// 		title: 'Thông tin lịch sử user nhận token - V-TOKEN',
			// 		code: CF_ROUTINGS_HISTORY.GET_INFO_HISTORY_BY_USER,
			// 		inc: path.resolve(__dirname, '../views/info_history_user.ejs'),
            //         view: 'index_admin.ejs'
            //     },
            //     methods: {
            //         get: [ async function (req, res) {
            //             const { userID } = req.params;
			// 			const infoPointOfUser = await HISTORY_MODEL.getInfoPointByUser({ userID });

			// 			ChildRouter.renderToView(req, res, {
			// 				infoPointOfUser: infoPointOfUser.data || []
			// 			})
            //         }]
            //     },
            // },

			// /**
            //  * Function: Thông tin điểm của user (API)
            //  * Date: 07/09/2021
            //  * Dev: MinhVH
            //  */
			// [CF_ROUTINGS_HISTORY.GET_INFO_HISTORY_BY_ID]: {
            //     config: {
            //         auth: [ roles.role.admin.bin ],
            //         type: 'view',
			// 		title: 'Thông tin lịch sử user nhận token - V-TOKEN',
			// 		code: CF_ROUTINGS_HISTORY.GET_INFO_HISTORY_BY_ID,
			// 		inc: path.resolve(__dirname, '../views/info_history.ejs'),
            //         view: 'index_admin.ejs'
            //     },
            //     methods: {
            //         get: [ async function (req, res) {
            //             const { historyID } = req.params;
			// 			const infoHistory = await HISTORY_MODEL.getInfoHistory({ historyID });

			// 			ChildRouter.renderToView(req, res, {
			// 				infoHistory: infoHistory.data || {}
			// 			})
            //         }]
            //     },
            // },

        }
    }
};
