const BASE_ROUTE = '/admin';

const CF_ROUTINGS_HISTORY = {
    API_GET_LIST_HISTORY_GROUP_BY_USER:  `/api${BASE_ROUTE}/history`,
    API_GET_LIST_HISTORY_BY_USER:    `/api${BASE_ROUTE}/histories`,
    API_GET_LASTED_HISTORY_BY_USER:  `/api${BASE_ROUTE}/lasted-history`,
    API_ADD_HISTORY_BY_USER:  `/api${BASE_ROUTE}/add-history`,
    API_TEST_UPDATE_HISTORY_AGENDA:  `/api${BASE_ROUTE}/update-history-agenda`,

    GET_LIST_HISTORY_BY_USER_EMBED:  `/embed/histories`,
    GET_LIST_HISTORY_GROUP_BY_USER:  `${BASE_ROUTE}/history`,
    GET_LIST_HISTORY_BY_USER:  `${BASE_ROUTE}/history/info`,
	GET_INFO_HISTORY_BY_USER:  `${BASE_ROUTE}/history/user/:userID`,
	GET_INFO_HISTORY_BY_ID:  `${BASE_ROUTE}/history/id/:historyID`,

    GET_LIST_HISTORY:  `${BASE_ROUTE}/list-history`,

    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_HISTORY = CF_ROUTINGS_HISTORY;
