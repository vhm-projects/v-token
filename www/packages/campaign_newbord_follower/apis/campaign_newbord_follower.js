"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path 											= require('path');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                           		= require('../../../routing/child_routing');
const roles                                 		= require('../../../config/cf_role');
const { CF_ROUTINGS_CAMPAIGN_NEWBORD_FOLLOWER }		= require('../constants/campaign_newbord_follower.uri');

/**
 * MODELS
 */
const CAMPAIGN__NEWBORD_FOLLOWER_MODEL				= require('../models/campaign_newbord_follower').MODEL;
const MILESTONE__CAMPAIGN_NEWBORD_FOLLOWER_MODEL	= require('../models/milestone_campaign_newbord_follower').MODEL;
const IMAGE_MODEL									= require('../../image/models/image').MODEL;


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * ================ ********************************* ===============
             * ================ QUẢN LÝ CAMPAIGN NEWBORD-FOLLOWER ===============
             * ================ ********************************* ===============
             */

			/**
             * Function: Tạo chiến dịch newbord-follower (API)
             * Date: 25/08/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_CAMPAIGN_NEWBORD_FOLLOWER.ADD_CAMPAIGN_NEWBORD_FOLLOWER]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'view',
					title: 'Tạo chiến dịch newborn theo dõi giải - V-TOKEN',
					code: CF_ROUTINGS_CAMPAIGN_NEWBORD_FOLLOWER.ADD_CAMPAIGN_NEWBORD_FOLLOWER,
					inc: path.resolve(__dirname, '../views/campaign/add_campaign_newbord_follower.ejs'),
                    view: 'index_admin.ejs'
                },
                methods: {
					get: [ function(req, res){
						ChildRouter.renderToView(req, res);
					}],
                    post: [ async function (req, res) {
                        let { campaignName, dateStart, dateEnd, content, icon } = req.body;

						if(icon){
							const image = await IMAGE_MODEL.insert(icon);
							icon = image && image.data && image.data._id;
						}

                        const infoAfterInsert = await CAMPAIGN__NEWBORD_FOLLOWER_MODEL.insert({ 
                            campaignName, dateStart, dateEnd, content, icon
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

			/**
			 * Function: Xóa chiến dịch newbord-follower (API)
			 * Date: 25/08/2021
			 * Dev: MinhVH
			 */
			[CF_ROUTINGS_CAMPAIGN_NEWBORD_FOLLOWER.DELETE_CAMPAIGN_NEWBORD_FOLLOWER]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'json',
                },
                methods: {
                    delete: [ async function (req, res) {
						const { campaignID } = req.query;

                        const infoAfterDelete = await CAMPAIGN__NEWBORD_FOLLOWER_MODEL.delete({ campaignID });
                        res.json(infoAfterDelete);
                    }]
                },
            },

             /**
             * Function: Cập nhật chiến dịch newbord-follower (API)
             * Date: 25/08/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_CAMPAIGN_NEWBORD_FOLLOWER.UPDATE_CAMPAIGN_NEWBORD_FOLLOWER]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'view',
					title: 'Cập nhật chiến dịch newborn theo dõi giải - V-TOKEN',
					code: CF_ROUTINGS_CAMPAIGN_NEWBORD_FOLLOWER.UPDATE_CAMPAIGN_NEWBORD_FOLLOWER,
					inc: path.resolve(__dirname, '../views/campaign/update_campaign_newbord_follower.ejs'),
                    view: 'index_admin.ejs'
                },
                methods: {
					get: [ async function (req, res) {
						const { campaignID } = req.query;

                        const infoCampaign = await CAMPAIGN__NEWBORD_FOLLOWER_MODEL.getInfo({ campaignID });
                        ChildRouter.renderToView(req, res, {
							infoCampaign: infoCampaign.data || {}
						})
                    }],
                    put: [ async function (req, res) {
                        let { campaignID, campaignName, dateStart, dateEnd, content, icon, status } = req.body;

						if(icon){
							const image = await IMAGE_MODEL.insert(icon);
							icon = image && image.data && image.data._id;
						}

                        const infoAfterUpdate = await CAMPAIGN__NEWBORD_FOLLOWER_MODEL.update({ 
                            campaignID, campaignName, dateStart, dateEnd, content, icon, status
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

			/**
             * Function: Thông tin chiến dịch newbord-follower (API)
             * Date: 25/08/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_CAMPAIGN_NEWBORD_FOLLOWER.INFO_CAMPAIGN_NEWBORD_FOLLOWER]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
						const { campaignID } = req.query;

                        const infoCampaign = await CAMPAIGN__NEWBORD_FOLLOWER_MODEL.getInfo({ campaignID });
                        res.json(infoCampaign);
                    }]
                },
            },

			/**
             * Function: Danh sách chiến dịch newbord-follower (API)
             * Date: 25/08/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_CAMPAIGN_NEWBORD_FOLLOWER.GET_LIST_CAMPAIGN_NEWBORD_FOLLOWER]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'view',
					title: 'Danh sách chiến dịch newborn theo dõi giải - V-TOKEN',
					code: CF_ROUTINGS_CAMPAIGN_NEWBORD_FOLLOWER.GET_LIST_CAMPAIGN_NEWBORD_FOLLOWER,
					inc: path.resolve(__dirname, '../views/campaign/list_campaign_newbord_follower.ejs'),
                    view: 'index_admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
						const { status } = req.query;
                        const listCampaign = await CAMPAIGN__NEWBORD_FOLLOWER_MODEL.getList({ status });

                        ChildRouter.renderToView(req, res, {
							listCampaign: listCampaign.data || []
						});
                    }]
                },
            },


			/**
             * ============ ******************************************** ===============
             * ============ QUẢN LÝ MILESTONE CAMPAIGN NEWBORLD-FOLLOWER ===============
             * ============ ******************************************** ===============
             */

			/**
             * Function: Tạo cột mốc chiến dịch newbord-follower (API)
             * Date: 25/08/2021
             * Dev: MinhVH
             */
			 [CF_ROUTINGS_CAMPAIGN_NEWBORD_FOLLOWER.ADD_MILESTONE_CAMPAIGN_NEWBORD_FOLLOWER]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'view',
					title: 'Tạo cột mốc newborn theo dõi giải - V-TOKEN',
					code: CF_ROUTINGS_CAMPAIGN_NEWBORD_FOLLOWER.ADD_MILESTONE_CAMPAIGN_NEWBORD_FOLLOWER,
					inc: path.resolve(__dirname, '../views/milestone/add_milestone_newbord_follower.ejs'),
                    view: 'index_admin.ejs'
                },
                methods: {
					get: [ async function(req, res){
						const { campaignID } = req.query;
                        const infoCampaign = await CAMPAIGN__NEWBORD_FOLLOWER_MODEL.getInfo({ campaignID });

						ChildRouter.renderToView(req, res, {
							infoCampaign: infoCampaign.data || {}
						});
					}],
                    post: [ async function (req, res) {
                        const { campaignID, milestoneName, fromFollower, toFollower, tokenAmount } = req.body;

                        const infoAfterInsert = await MILESTONE__CAMPAIGN_NEWBORD_FOLLOWER_MODEL.insert({ 
                            campaignID, milestoneName, fromFollower, toFollower, tokenAmount
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

			/**
             * Function: Thông tin cột mốc chiến dịch newbord-follower (API)
             * Date: 25/08/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_CAMPAIGN_NEWBORD_FOLLOWER.UPDATE_MILESTONE_CAMPAIGN_NEWBORD_FOLLOWER]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'view',
					title: 'Cập nhật cột mốc newborn theo dõi giải - V-TOKEN',
					code: CF_ROUTINGS_CAMPAIGN_NEWBORD_FOLLOWER.UPDATE_MILESTONE_CAMPAIGN_NEWBORD_FOLLOWER,
					inc: path.resolve(__dirname, '../views/milestone/update_milestone_newbord_follower.ejs'),
                    view: 'index_admin.ejs'
                },
                methods: {
					get: [ async function(req, res){
						const { milestoneID, campaignID } = req.query;

                        const infoMilestone = await MILESTONE__CAMPAIGN_NEWBORD_FOLLOWER_MODEL.getInfo({ milestoneID });
                        const infoCampaign = await CAMPAIGN__NEWBORD_FOLLOWER_MODEL.getInfo({ campaignID });

						ChildRouter.renderToView(req, res, {
							infoMilestone: infoMilestone.data || {},
							infoCampaign: infoCampaign.data || {}
						});
					}],
                    put: [ async function (req, res) {
                        const { milestoneID, milestoneName, fromFollower, toFollower, tokenAmount } = req.body;

                        const infoAfterUpdate = await MILESTONE__CAMPAIGN_NEWBORD_FOLLOWER_MODEL.update({ 
                            milestoneID, milestoneName, fromFollower, toFollower, tokenAmount
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

			/**
			 * Function: Xóa cột mốc chiến dịch newbord-follower (API)
			 * Date: 25/08/2021
			 * Dev: MinhVH
			 */
			[CF_ROUTINGS_CAMPAIGN_NEWBORD_FOLLOWER.DELETE_MILESTONE_CAMPAIGN_NEWBORD_FOLLOWER]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'json',
                },
                methods: {
                    delete: [ async function (req, res) {
						const { milestoneID } = req.query;

                        const infoAfterDelete = await MILESTONE__CAMPAIGN_NEWBORD_FOLLOWER_MODEL.delete({ milestoneID });
                        res.json(infoAfterDelete);
                    }]
                },
            },


			/**
			 * Function: Thông tin cột mốc chiến dịch newbord-follower (API)
			 * Date: 25/08/2021
			 * Dev: MinhVH
			 */
			[CF_ROUTINGS_CAMPAIGN_NEWBORD_FOLLOWER.INFO_MILESTONE_CAMPAIGN_NEWBORD_FOLLOWER]: {
			config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
						const { milestoneID } = req.query;

                        const infoMilestone = await MILESTONE__CAMPAIGN_NEWBORD_FOLLOWER_MODEL.getInfo({ milestoneID });
                        res.json(infoMilestone);
                    }]
                },
            },

			/**
			 * Function: Danh sách cột mốc chiến dịch newbord-follower (API)
			 * Date: 25/08/2021
			 * Dev: MinhVH
			 */
			[CF_ROUTINGS_CAMPAIGN_NEWBORD_FOLLOWER.GET_LIST_MILESTONE_CAMPAIGN_NEWBORD_FOLLOWER]: {
				config: {
					auth: [ roles.role.editor.bin ],
					type: 'view',
					title: 'Danh sách cột mốc newborn theo dõi giải - V-TOKEN',
					code: CF_ROUTINGS_CAMPAIGN_NEWBORD_FOLLOWER.GET_LIST_MILESTONE_CAMPAIGN_NEWBORD_FOLLOWER,
					inc: path.resolve(__dirname, '../views/milestone/list_milestone_newbord_follower.ejs'),
                    view: 'index_admin.ejs'
				},
				methods: {
					get: [ async function(req, res){
						const { campaignID } = req.query;
                        const infoCampaign = await CAMPAIGN__NEWBORD_FOLLOWER_MODEL.getInfo({ campaignID });
						const listMilestone = await MILESTONE__CAMPAIGN_NEWBORD_FOLLOWER_MODEL.getList({ campaignID });

						ChildRouter.renderToView(req, res, {
							infoCampaign: infoCampaign.data || {},
							listMilestone: listMilestone.data || []
						});
					}],
				},
			},

        }
    }
};
