"use strict";

const Schema 	= require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

/**
 * COLLECTION CAMPAIGN NEWBORD-FOLLOWER CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('campaign_newbord_follower', {
	campaignID: {
		type: String,
		unique: true,
		required: true
	},
	campaignName: {
		type: String,
		required: true
	},
	dateStart: {
		type: Date,
		required: true
	},
	dateEnd: {
		type: Date,
		required: true
	},
	content: {
		type: String,
	},
	icon: {
		type: Schema.Types.ObjectId,
		ref: 'image'
	},
	/**
	 * Trạng thái hoạt động.
	 * 0. Không chạy
	 * 1. Đang chạy
	 * 2. Đã kết thúc
	 */
	status: {
		type: Number,
		default: 0
	},
});
