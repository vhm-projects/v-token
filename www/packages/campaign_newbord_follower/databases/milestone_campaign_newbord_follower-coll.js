"use strict";

const Schema = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

/**
 * COLLECTION MILESTONE CAMPAIGN NEWBORD-FOLLOWER CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('milestone_campaign_newbord_follower', {
	milestoneID: {
		type: String,
		unique: true,
		required: true
	},
	campaignID: {
		type: Schema.Types.ObjectId,
		ref: 'campaign_newbord_follower'
	},
	milestoneName: {
		type: String,
		required: true
	},
	fromFollower: {
		type: Number,
		required: true
	},
	toFollower: {
		type: Number
	},
	tokenAmount: {
		type: Number,
		required: true
	},
});
