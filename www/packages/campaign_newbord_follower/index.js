const CAMPAIGN__NEWBORD_FOLLOWER_MODEL 		   	= require('./models/campaign_newbord_follower').MODEL;
const CAMPAIGN__NEWBORD_FOLLOWER_COLL  		   	= require('./databases/campaign_newbord_follower-coll');
const CAMPAIGN__NEWBORD_FOLLOWER_ROUTES         = require('./apis/campaign_newbord_follower');
const { CF_ROUTINGS_CAMPAIGN_NEWBORD_FOLLOWER } = require('./constants/campaign_newbord_follower.uri');

const MILESTONE__CAMPAIGN_NEWBORD_FOLLOWER_MODEL = require('./models/milestone_campaign_newbord_follower').MODEL;
const MILESTONE__CAMPAIGN_NEWBORD_FOLLOWER_COLL  = require('./databases/milestone_campaign_newbord_follower-coll');

module.exports = {
	CAMPAIGN__NEWBORD_FOLLOWER_MODEL,
	CAMPAIGN__NEWBORD_FOLLOWER_COLL,
	CAMPAIGN__NEWBORD_FOLLOWER_ROUTES,
	CF_ROUTINGS_CAMPAIGN_NEWBORD_FOLLOWER,

	MILESTONE__CAMPAIGN_NEWBORD_FOLLOWER_MODEL,
	MILESTONE__CAMPAIGN_NEWBORD_FOLLOWER_COLL
}
