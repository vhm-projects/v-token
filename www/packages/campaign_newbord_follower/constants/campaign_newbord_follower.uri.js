const BASE_ROUTE = '/admin';

const CF_ROUTINGS_CAMPAIGN_NEWBORD_FOLLOWER = {
	// CHAMPAIGN NEWBORD-FOLLOWER
	ADD_CAMPAIGN_NEWBORD_FOLLOWER:    `${BASE_ROUTE}/add-campaign-newbord-follower`,
	UPDATE_CAMPAIGN_NEWBORD_FOLLOWER: `${BASE_ROUTE}/update-campaign-newbord-follower`,
	INFO_CAMPAIGN_NEWBORD_FOLLOWER:   `${BASE_ROUTE}/info-campaign-newbord-follower`,
    DELETE_CAMPAIGN_NEWBORD_FOLLOWER: `${BASE_ROUTE}/delete-campaign-newbord-follower`,
    GET_LIST_CAMPAIGN_NEWBORD_FOLLOWER: `${BASE_ROUTE}/list-campaign-newbord-follower`,

	// MILESTONE
	ADD_MILESTONE_CAMPAIGN_NEWBORD_FOLLOWER:    `${BASE_ROUTE}/add-milestone-campaign-newbord-follower`,
	UPDATE_MILESTONE_CAMPAIGN_NEWBORD_FOLLOWER: `${BASE_ROUTE}/update-milestone-campaign-newbord-follower`,
	INFO_MILESTONE_CAMPAIGN_NEWBORD_FOLLOWER:   `${BASE_ROUTE}/info-milestone-campaign-newbord-follower`,
    DELETE_MILESTONE_CAMPAIGN_NEWBORD_FOLLOWER: `${BASE_ROUTE}/delete-milestone-campaign-newbord-follower`,
    GET_LIST_MILESTONE_CAMPAIGN_NEWBORD_FOLLOWER: `${BASE_ROUTE}/list-milestone-campaign-newbord-follower`,

    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_CAMPAIGN_NEWBORD_FOLLOWER = CF_ROUTINGS_CAMPAIGN_NEWBORD_FOLLOWER;
