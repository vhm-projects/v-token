"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID                      	= require('mongoose').Types.ObjectId;

/**
 * INTERNAL PACKAGE
 */
const { randomStringNumber } 			= require('../../../utils/string_utils');

/**
 * BASES
 */
const BaseModel 						= require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const MILESTONE__CAMPAIGN_NEWBORD_FOLLOWER_COLL	= require('../databases/milestone_campaign_newbord_follower-coll');


class Model extends BaseModel {
    constructor() {
        super(MILESTONE__CAMPAIGN_NEWBORD_FOLLOWER_COLL);
    }

	checkMilestoneIDExists(milestoneID){
		return new Promise(resolve => {
			(async function recursiveCheckMilestoneID(milestoneID){
				let checkExists = await MILESTONE__CAMPAIGN_NEWBORD_FOLLOWER_COLL.findOne({ milestoneID });
				if(checkExists){
					milestoneID = randomStringNumber(5);
					recursiveCheckMilestoneID(milestoneID);
				} else{
					resolve(milestoneID);
				}
			})(milestoneID)
		})
	}

	insert({ campaignID, milestoneName, fromFollower, toFollower = -1, tokenAmount }) {
        return new Promise(async resolve => {
            try {
                if(!milestoneName)
                    return resolve({ error: true, message: 'Tên cột mốc chiến dịch không hợp lệ' });

				if(!ObjectID.isValid(campaignID))
                    return resolve({ error: true, message: "Campaign ID không hợp lệ" });

                let checkExists = await MILESTONE__CAMPAIGN_NEWBORD_FOLLOWER_COLL.findOne({
					milestoneName,
					campaignID
                });
                if(checkExists)
                    return resolve({ error: true, message: "Tên cột mốc chiến dịch đã tồn tại" });

				if(!fromFollower)
                    return resolve({ error: true, message: "Phạm vi follower không hợp lệ" });

				// Convert to number
				fromFollower = +fromFollower;
				toFollower 	 = +toFollower;

				if(fromFollower > toFollower && toFollower !== -1)
                    return resolve({ error: true, message: "Phạm vi follower không hợp lệ" });

				let lastMilestone = await MILESTONE__CAMPAIGN_NEWBORD_FOLLOWER_COLL
					.findOne({ campaignID })
					.sort({ createAt: -1 })
					.lean();
                    
                if (lastMilestone) {
                    let lastToFollower = lastMilestone.toFollower;

                    if(lastToFollower === -1)
                        return resolve({ error: true, message: "Phạm vi follower đã không giới hạn" });
    
                    if(fromFollower <= lastToFollower || fromFollower !== lastToFollower + 1)
                        return resolve({ error: true, message: "Phạm vi follower không hợp lệ" });
                }

				let milestoneID = await this.checkMilestoneIDExists(randomStringNumber(5));
                let dataInsert = {
					campaignID,
					milestoneID,
                    milestoneName, 
                    fromFollower,
                    toFollower,
					tokenAmount
                }

                let infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'Không thể tạo cột mốc chiến dịch' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	update({ milestoneID, milestoneName, fromFollower, toFollower = -1, tokenAmount }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(milestoneID))
                    return resolve({ error: true, message: "Milestone ID không hợp lệ" });

                let checkExists = await MILESTONE__CAMPAIGN_NEWBORD_FOLLOWER_COLL.findById(milestoneID);
                if(!checkExists)
                    return resolve({ error: true, message: "cột mốc chiến dịch không tồn tại" });

				if(checkExists.milestoneName !== milestoneName){
					let checkNameExist = await MILESTONE__CAMPAIGN_NEWBORD_FOLLOWER_COLL.findOne({ 
						campaignID: checkExists.campaignID,
						milestoneName,
					});
					if(checkNameExist) {
						return resolve({ error: true, message: 'Tên cột mốc chiến dịch đã tồn tại' });
					}
				}

				if(+fromFollower > +toFollower && +toFollower !== -1)
                    return resolve({ error: true, message: "Phạm vi follower không hợp lệ" });

                let dataUpdateObj = { toFollower };
                milestoneName 	&& (dataUpdateObj.milestoneName  = milestoneName);
                fromFollower 	&& (dataUpdateObj.fromFollower   = fromFollower);
                tokenAmount		&& (dataUpdateObj.tokenAmount  	 = tokenAmount);

                const infoAfterUpdate = await this.updateWhereClause({ _id: milestoneID }, dataUpdateObj);
				if(!infoAfterUpdate)
					return resolve({ error: true, message: 'Không thể cập nhật cột mốc chiến dịch' });

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getInfo({ milestoneID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(milestoneID))
                    return resolve({ error: true, message: "Milestone ID không hợp lệ" });

                let infoCampaign = await MILESTONE__CAMPAIGN_NEWBORD_FOLLOWER_COLL.findById(milestoneID);
                if(!infoCampaign)
                    return resolve({ error: true, message: "Cột mốc chiến dịch không tồn tại" });

                return resolve({ error: false, data: infoCampaign });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    delete({ milestoneID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(milestoneID))
                    return resolve({ error: true, message: "Milestone ID không hợp lệ" });

				let infoAfterDelete = await MILESTONE__CAMPAIGN_NEWBORD_FOLLOWER_COLL.findByIdAndRemove(milestoneID);

                if(!infoAfterDelete) 
                    return resolve({ error: true, message: "Không thể xoá cột mốc chiến dịch" });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getList({ campaignID }) {
        return new Promise(async resolve => {
            try {
				if(!ObjectID.isValid(campaignID))
					return resolve({ error: true, message: "Campaign ID không hợp lệ" });

                let listMilestone = await MILESTONE__CAMPAIGN_NEWBORD_FOLLOWER_COLL
					.find({ campaignID })
					.sort({ fromFollower: -1, modifyAt: -1 })
					.lean();

                if(!listMilestone)
                    return resolve({ error: true, message: "Không thể lấy danh sách cột mốc" });

                return resolve({ error: false, data: listMilestone });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
