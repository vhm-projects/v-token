"use strict";

/**
 * EXTERNAL PACKAGES
 */
const ObjectID                      = require('mongoose').Types.ObjectId;
const jwt                           = require('jsonwebtoken');
const { hash, hashSync, compare }   = require('bcryptjs');

/**
 * INTERNAL PACKAGES
 */
const cfJWS                         = require('../../../config/cf_jws');
const { validEmail }				= require('../../../utils/string_utils');

/**
 * BASES
 */
const BaseModel 					= require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const USER_COLL  					= require('../databases/user-coll');


class Model extends BaseModel {
    constructor() {
        super(USER_COLL);
    }

	insert({ fullname, email, password, role, status = 1 }) {
        return new Promise(async resolve => {
            try {
                if(!fullname || !email)
                    return resolve({ error: true, message: 'Bạn cần nhập đầy đủ fullname và email' });

                let emailValid = email.toLowerCase().trim();

				if(!validEmail(emailValid))
					return resolve({ error: true, message: 'Email không hợp lệ' });

                let checkExists = await USER_COLL.findOne({ email });
                if(checkExists)
                    return resolve({ error: true, message: "Email đã tồn tại" });

				if(![0,1].includes(+role))
					return resolve({ error: true, message: "Quyền không hợp lệ" });

				if(![0,1].includes(+status))
					return resolve({ error: true, message: "Trạng thái không hợp lệ" });

				if(!password)
					return resolve({ error: true, message: 'Bạn cần nhập mật khẩu cho user' });

                let hashPassword = await hash(password, 8);
				if (!hashPassword)
					return resolve({ error: true, message: 'Không thể hash password' });

                let dataInsert = {
					email: emailValid,
					password: hashPassword,
                    fullname,
					status,
					role
                }

                let infoAfterInsert = await this.insertData(dataInsert);

                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'Tạo người dùng thất bại' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    getInfo({ userID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(userID))
                    return resolve({ error: true, message: "ID user không hợp lệ" });

                let infoUser = await USER_COLL.findById(userID);
                if(!infoUser)
                    return resolve({ error: true, message: "Người dùng không tồn tại" });

                return resolve({ error: false, data: infoUser });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	update({ userID, fullname, password, status, role }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(userID))
                    return resolve({ error: true, message: "ID user không hợp lệ" });

                let checkExists = await USER_COLL.findById(userID);
                if(!checkExists)
                    return resolve({ error: true, message: "Người dùng không tồn tại" });

				// if(oldPassword){
				// 	let isMatchPass = await compare(oldPassword, checkExists.password);
				// 	if (!isMatchPass) 
				// 		return resolve({ error: true, message: 'Mật khẩu cũ không trùng khớp' });
				// }

                let dataUpdateUser = {};
                fullname && (dataUpdateUser.fullname = fullname);
                password && (dataUpdateUser.password = hashSync(password, 8));

                if([0,1].includes(+role)){
                    dataUpdateUser.role = role;
                }

				if([0,1].includes(+status)){
					dataUpdateUser.status = status;
				}

                await this.updateWhereClause({ _id: userID }, dataUpdateUser);
                password && delete dataUpdateUser.password;

                return resolve({ error: false, data: dataUpdateUser });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    delete({ userID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(userID))
                    return resolve({ error: true, message: "ID user không hợp lệ" });

				let infoAfterDelete = await USER_COLL.findByIdAndDelete(userID);
                if(!infoAfterDelete) 
                    return resolve({ error: true, message: "Không thể xoá người dùng" });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getList({ status }){
        return new Promise(async resolve => {
            try {
                let listUsers = await USER_COLL
					.find({ ...!!status && { status } })
					.sort({ createAt: -1 })
					.lean();

                if(!listUsers)
                    return resolve({ error: true, message: "Không thể lấy danh sách người dùng" });

                return resolve({ error: false, data: listUsers });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    signIn({ email, password }) {
        return new Promise(async resolve => {
            try {
                let checkExists = await USER_COLL.findOne({ email: email.toLowerCase().trim() });
                if (!checkExists) 
                    return resolve({ error: true, message: 'Email không tồn tại' });

				if (checkExists.status == 0) 
                    return resolve({ error: true, message: 'Người dùng đã bị khoá' });

                let isMatchPass = await compare(password, checkExists.password);
                if (!isMatchPass) 
                    return resolve({ error: true, message: 'Mật khẩu không trùng khớp' });

                let infoUser = {
                    _id: checkExists._id,
                    fullname: checkExists.fullname,
                    email: checkExists.email,
                    status: checkExists.status,
                    role: checkExists.role,
                }
                let token = jwt.sign(infoUser, cfJWS.secret);

                return resolve({
                    error: false,
                    data: { user: infoUser, token }
                });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
