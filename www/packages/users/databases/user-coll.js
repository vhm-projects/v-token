"use strict";

const BASE_COLL = require('../../../database/intalize/base-coll');

/**
 * COLLECTION USER CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('user', {
	fullname: {
		type: String,
	},
	email: {
		type: String,
		trim: true,
		unique : true
	},
	password: {
		type: String
	},
	/**
	 * Trạng thái hoạt động.
	 * 1. Hoạt động
	 * 0. Khóa
	 */
	status: {
		type: Number,
		default: 1
	},
	devices: [{
		deviceName      : { type : String }, 
		deviceID        : { type : String },
		registrationID  : { type : String }
	}],
	/**
	 * Phân quyền truy cập.
	 * 0. ADMIN
	 * 1. EDITOR
	 */
	role: {
		type: Number,
		default: 1
	},
});
