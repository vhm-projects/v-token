const BASE_ROUTE = '/admin';

const CF_ROUTINGS_USER = {
    LOGIN:  '/login',
    LOGOUT: '/logout',

	LIST_USER:   `${BASE_ROUTE}/users`,
	ADD_USER:    `${BASE_ROUTE}/add-user`,
	INFO_USER:   `${BASE_ROUTE}/info-user`,
    UPDATE_USER: `${BASE_ROUTE}/update-user`,
    DELETE_USER: `${BASE_ROUTE}/delete-user`,

    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_USER = CF_ROUTINGS_USER;
