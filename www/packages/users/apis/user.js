"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path 									= require('path');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                           = require('../../../routing/child_routing');
const roles                                 = require('../../../config/cf_role');
const { CF_ROUTINGS_USER } 					= require('../constants/user.uri');
const USER_SESSION 	                        = require('../../../session/user-session');

/**
 * MODELS
 */
const USER_MODEL 							= require('../models/user').MODEL;


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * ========================== ************************ ================================
             * ========================== QUẢN LÝ USER PERMISSION  ================================
             * ========================== ************************ ================================
             */

			/**
             * Function: Tạo user (permission: admin) (API)
             * Date: 14/06/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_USER.ADD_USER]: {
                config: {
                    auth: [ roles.role.all.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { fullname, email, password, role, status } = req.body;

                        const infoAfterInsertAccount = await USER_MODEL.insert({ 
                            fullname, email, password, role, status
                        });
                        res.json(infoAfterInsertAccount);
                    }]
                },
            },

			/**
             * Function: Danh sách user (permission: admin) (API, VIEW)
             * Date: 14/06/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_USER.LIST_USER]: {
                config: {
                    auth: [ roles.role.admin.bin ],
					type: 'view',
					title: 'Quản lý User - V-TOKEN',
					code: 'USER',
					inc: path.resolve(__dirname, '../views/list_user.ejs'),
                    view: 'index_admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
						const { status } = req.query;
						const listUser = await USER_MODEL.getList({ status });

                        ChildRouter.renderToView(req, res, {
							listUser: listUser.data || []
						});
                    }]
                },
            },

			/**
			 * Function: Xóa user (permission: admin) (API)
			 * Date: 14/06/2021
			 * Dev: MinhVH
			 */
			[CF_ROUTINGS_USER.DELETE_USER]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    delete: [ async function (req, res) {
						const { userID } = req.query;

                        const infoAfterDelete = await USER_MODEL.delete({ userID });
                        res.json(infoAfterDelete);
                    }]
                },
            },

			/**
             * Function: Cập nhật user (permission: admin) (API)
             * Date: 14/06/2021
             * Dev: MinhVH
             */
			 [CF_ROUTINGS_USER.UPDATE_USER]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const { userID, fullname, password, status, role } = req.body;

                        const infoAfterUpdate = await USER_MODEL.update({ 
                            userID, fullname, password, status, role
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

            /**
             * Function: Thông tin user (permission: admin) (API)
             * Date: 14/06/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_USER.INFO_USER]: {
                config: {
                    auth: [ roles.role.admin.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
						const { userID } = req.query;

                        const infoUser = await USER_MODEL.getInfo({ userID });
                        res.json(infoUser);
                    }]
                },
            },

			/**
             * Function: Đăng nhập account (API, VIEW)
             * Date: 30/09/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_USER.LOGIN]: {
                config: {
					auth: [ roles.role.all.bin ],
					type: 'view',
                    inc : 'pages/login-admin.ejs',
                    view: 'pages/login-admin.ejs'
				},
				methods: {
					get: [ (req, res) => {
						 /**
                         * CHECK AND REDIRECT WHEN LOGIN
                         */
						const infoLogin = USER_SESSION.getUser(req.session);
						if (infoLogin && infoLogin.user && infoLogin.token)
							return res.redirect('/');

						ChildRouter.renderToView(req, res);
					}],
                    post: [ async (req, res) => {
                        const { email, password } = req.body;

                        const infoSignIn = await USER_MODEL.signIn({ email, password });
						if (!infoSignIn.error) {
							const { user, token } = infoSignIn.data;

                            USER_SESSION.saveUser(req.session, {
                                user, 
                                token,
                            });
                        } 
                        res.json(infoSignIn);
                    }],
				},
            },

			/**
             * Function: Đăng xuất account (API)
             * Date: 30/09/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_USER.LOGOUT]: {
                config: {
                    auth: [ roles.role.all.bin ],
					type: 'json',
                },
                methods: {
                    get: [ (req, res) => {
                        USER_SESSION.destroySession(req.session);
						res.redirect('/login');
                    }]
                },
            },

        }
    }
};
