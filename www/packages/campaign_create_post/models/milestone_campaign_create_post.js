"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID                      			= require('mongoose').Types.ObjectId;

/**
 * INTERNAL PACKAGE
 */
const { randomStringNumber } 					= require('../../../utils/string_utils');
const agenda                                    = require('../../../config/cf_agenda');

/**
 * BASES
 */
const BaseModel 								= require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const MILESTONE__CAMPAIGN_CREATE_POST_COLL	    = require('../databases/milestone_campaign_create_post-coll');
const MILESTONE__POINT_CREATE_POST_COLL	    	= require('../databases/milestone_point_create_post-coll');


class Model extends BaseModel {
    constructor() {
        super(MILESTONE__CAMPAIGN_CREATE_POST_COLL);
    }

	checkMilestoneIDExists(milestoneID){
		return new Promise(resolve => {
			(async function recursiveCheckMilestoneID(milestoneID){
				let checkExists = await MILESTONE__CAMPAIGN_CREATE_POST_COLL.findOne({ milestoneID });
				if(checkExists){
					milestoneID = randomStringNumber(5);
					recursiveCheckMilestoneID(milestoneID);
				} else{
					resolve(milestoneID);
				}
			})(milestoneID)
		})
	}

	insert({ campaignID, milestoneName, numPost, period, continuous, limit, delayTime, tokenAmount, listPointMilestone }) {
        return new Promise(async resolve => {
            try {
                if(!milestoneName)
                    return resolve({ error: true, message: 'Tên cột mốc chiến dịch không hợp lệ' });

				if(!ObjectID.isValid(campaignID))
                    return resolve({ error: true, message: "ID chiến dịch không hợp lệ" });

                let checkExists = await MILESTONE__CAMPAIGN_CREATE_POST_COLL.findOne({
					milestoneName,
					campaignID
                });
                if(checkExists)
                    return resolve({ error: true, message: "Tên cột mốc chiến dịch đã tồn tại" });

				if(!numPost)
                    return resolve({ error: true, message: "Số lượng tạo bài viết không hợp lệ" });

				if(!period)
                    return resolve({ error: true, message: "Chu kì tạo bài viết không hợp lệ" });

				// Convert to number
				numPost = +numPost;

				if(numPost <= 0)
                    return resolve({ error: true, message: "Số lượng tạo bài viết không hợp lệ" });

				let milestoneID = await this.checkMilestoneIDExists(randomStringNumber(5));
                let dataInsert = {
					campaignID,
					milestoneID,
                    milestoneName, 
                    numPost,
					period,
					continuous,
					limit,
					tokenAmount,
                    delayTime
                }

                let infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'Không thể tạo cột mốc chiến dịch' });

				if(listPointMilestone && listPointMilestone.length){
					listPointMilestone = listPointMilestone.map(dateMilestone => ({
						milestoneID: infoAfterInsert._id,
						date: dateMilestone.date,
						point: dateMilestone.point
					}))
					await MILESTONE__POINT_CREATE_POST_COLL.insertMany(listPointMilestone);
				}

                if(delayTime){
                    let timeScheduler = `${delayTime} minute`;

                    let campaignID = infoAfterInsert.campaignID.toString();
                    let milestoneID = infoAfterInsert._id.toString();

                    agenda.schedule(timeScheduler, 'event_get_point', {
                        campaignID: campaignID,
                        milestoneID: milestoneID,
                    });
                    console.log(`agenda: event_get_point added - ${timeScheduler}`);
                }

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	update({ milestoneID, milestoneName, numPost, period, continuous, limit, delayTime, tokenAmount, listPointMilestone }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(milestoneID))
                    return resolve({ error: true, message: "ID cột mốc không hợp lệ" });

                let checkExists = await MILESTONE__CAMPAIGN_CREATE_POST_COLL.findById(milestoneID);
                if(!checkExists)
                    return resolve({ error: true, message: "cột mốc chiến dịch không tồn tại" });

				if(checkExists.milestoneName !== milestoneName){
					let checkNameExist = await MILESTONE__CAMPAIGN_CREATE_POST_COLL.findOne({ 
						campaignID: checkExists.campaignID,
						milestoneName,
					});
					if(checkNameExist) {
						return resolve({ error: true, message: 'Tên cột mốc chiến dịch đã tồn tại' });
					}
				}

				if(!numPost)
                    return resolve({ error: true, message: "Số lượng tạo bài viết không hợp lệ" });

				if(!period)
                    return resolve({ error: true, message: "Chu kì tạo bài viết không hợp lệ" });

				// Convert to number
				numPost = +numPost;

				if(numPost <= 0)
                    return resolve({ error: true, message: "Số lượng tạo bài viết không hợp lệ" });

                let dataUpdateObj = { numPost, period };
                milestoneName 	&& (dataUpdateObj.milestoneName  	= milestoneName);
                continuous 		&& (dataUpdateObj.continuous  		= continuous);
                limit 			&& (dataUpdateObj.limit  			= limit);
                tokenAmount		&& (dataUpdateObj.tokenAmount  	 	= tokenAmount);
                delayTime		&& (dataUpdateObj.delayTime  	 	= delayTime);

                const infoAfterUpdate = await this.updateWhereClause({ _id: milestoneID }, dataUpdateObj);
				if(!infoAfterUpdate)
					return resolve({ error: true, message: 'Không thể cập nhật cột mốc chiến dịch' });

				if(listPointMilestone && listPointMilestone.length){
					listPointMilestone = listPointMilestone.map(dateMilestone => ({
						milestoneID,
						date: dateMilestone.date,
						point: dateMilestone.point
					}))
					await MILESTONE__POINT_CREATE_POST_COLL.deleteMany({ milestoneID });
					await MILESTONE__POINT_CREATE_POST_COLL.insertMany(listPointMilestone);
				} else{
					await MILESTONE__POINT_CREATE_POST_COLL.deleteMany({ milestoneID });
				}

                if(delayTime){
                    let timeScheduler = `${delayTime} minute`;
                    let campaignID = infoAfterUpdate.campaignID.toString();

                    await agenda.cancel({
                        "data.campaignID": campaignID,
                        "data.milestoneID": milestoneID.toString(),
                    })

                    agenda.schedule(timeScheduler, 'event_get_point', {
                        campaignID: campaignID,
                        milestoneID: milestoneID.toString(),
                    });
                    console.log(`agenda: event_get_point updated - ${timeScheduler}`);
                }

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getInfo({ milestoneID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(milestoneID))
                    return resolve({ error: true, message: "ID cột mốc không hợp lệ" });

                let infoCampaign = await MILESTONE__CAMPAIGN_CREATE_POST_COLL.findById(milestoneID);
                if(!infoCampaign)
                    return resolve({ error: true, message: "Cột mốc chiến dịch không tồn tại" });

                return resolve({ error: false, data: infoCampaign });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    delete({ milestoneID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(milestoneID))
                    return resolve({ error: true, message: "ID cột mốc không hợp lệ" });

				let infoAfterDelete = await MILESTONE__CAMPAIGN_CREATE_POST_COLL.findByIdAndRemove(milestoneID);

                if(!infoAfterDelete) 
                    return resolve({ error: true, message: "Không thể xoá cột mốc chiến dịch" });

				await MILESTONE__POINT_CREATE_POST_COLL.deleteMany({ milestoneID });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getList({ campaignID }) {
        return new Promise(async resolve => {
            try {
				if(!ObjectID.isValid(campaignID))
					return resolve({ error: true, message: "ID chiến dịch không hợp lệ" });

                let listMilestone = await MILESTONE__CAMPAIGN_CREATE_POST_COLL
					.find({ campaignID })
					.sort({ modifyAt: -1 })
					.lean();

                if(!listMilestone)
                    return resolve({ error: true, message: "Không thể lấy danh sách cột mốc" });

                return resolve({ error: false, data: listMilestone });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
