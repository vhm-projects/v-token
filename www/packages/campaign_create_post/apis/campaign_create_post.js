"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                       	= require('../../../routing/child_routing');
const roles                             	= require('../../../config/cf_role');
const { CF_ROUTINGS_CAMPAIGN_CREATE_POST }	= require('../constants/campaign_create_post.uri');

/**
 * MODELS
 */
const CAMPAIGN__CREATE_POST_MODEL			= require('../models/campaign_create_post').MODEL;
const MILESTONE__CAMPAIGN_CREATE_POST_MODEL	= require('../models/milestone_campaign_create_post').MODEL;
const MILESTONE__POINT_CREATE_POST_COLL		= require('../databases/milestone_point_create_post-coll');
const IMAGE_MODEL							= require('../../image/models/image').MODEL;


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * ================ **************************** ===============
             * ================ QUẢN LÝ CAMPAIGN CREATE POST ===============
             * ================ **************************** ===============
             */

			/**
             * Function: Tạo chiến dịch create post (API)
             * Date: 25/08/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_CAMPAIGN_CREATE_POST.ADD_CAMPAIGN_CREATE_POST]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'view',
					title: 'Tạo chiến dịch tạo nội dung bài viết - V-TOKEN',
					code: CF_ROUTINGS_CAMPAIGN_CREATE_POST.ADD_CAMPAIGN_CREATE_POST,
					inc: path.resolve(__dirname, '../views/campaign/add_campaign_create_post.ejs'),
                    view: 'index_admin.ejs'
                },
                methods: {
					get: [ function(req, res){
						ChildRouter.renderToView(req, res);
					}],
                    post: [ async function (req, res) {
                        let { campaignName, dateStart, dateEnd, content, icon } = req.body;

						if(icon){
							const image = await IMAGE_MODEL.insert(icon);
							icon = image && image.data && image.data._id;
						}

                        const infoAfterInsert = await CAMPAIGN__CREATE_POST_MODEL.insert({ 
                            campaignName, dateStart, dateEnd, content, icon
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

			/**
			 * Function: Xóa chiến dịch create post (API)
			 * Date: 25/08/2021
			 * Dev: MinhVH
			 */
			[CF_ROUTINGS_CAMPAIGN_CREATE_POST.DELETE_CAMPAIGN_CREATE_POST]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'json',
                },
                methods: {
                    delete: [ async function (req, res) {
						const { campaignID } = req.query;

                        const infoAfterDelete = await CAMPAIGN__CREATE_POST_MODEL.delete({ campaignID });
                        res.json(infoAfterDelete);
                    }]
                },
            },

             /**
             * Function: Cập nhật chiến dịch create post (API)
             * Date: 25/08/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_CAMPAIGN_CREATE_POST.UPDATE_CAMPAIGN_CREATE_POST]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'view',
					title: 'Cập nhật chiến dịch tạo nội dung bài viết - V-TOKEN',
					code: CF_ROUTINGS_CAMPAIGN_CREATE_POST.UPDATE_CAMPAIGN_CREATE_POST,
					inc: path.resolve(__dirname, '../views/campaign/update_campaign_create_post.ejs'),
                    view: 'index_admin.ejs'
                },
                methods: {
					get: [ async function (req, res) {
						const { campaignID } = req.query;

                        const infoCampaign = await CAMPAIGN__CREATE_POST_MODEL.getInfo({ campaignID });
                        ChildRouter.renderToView(req, res, {
							infoCampaign: infoCampaign.data || {}
						})
                    }],
                    put: [ async function (req, res) {
                        let { campaignID, campaignName, dateStart, dateEnd, content, icon, status } = req.body;

						if(icon){
							const image = await IMAGE_MODEL.insert(icon);
							icon = image && image.data && image.data._id;
						}

                        const infoAfterUpdate = await CAMPAIGN__CREATE_POST_MODEL.update({ 
                            campaignID, campaignName, dateStart, dateEnd, content, icon, status
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

			/**
             * Function: Thông tin chiến dịch create post (API)
             * Date: 25/08/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_CAMPAIGN_CREATE_POST.INFO_CAMPAIGN_CREATE_POST]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
						const { campaignID } = req.query;

                        const infoCampaign = await CAMPAIGN__CREATE_POST_MODEL.getInfo({ campaignID });
                        res.json(infoCampaign);
                    }]
                },
            },

			/**
             * Function: Danh sách chiến dịch create post (API)
             * Date: 25/08/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_CAMPAIGN_CREATE_POST.GET_LIST_CAMPAIGN_CREATE_POST]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'view',
					title: 'Danh sách chiến dịch tạo nội dung bài viết - V-TOKEN',
					code: CF_ROUTINGS_CAMPAIGN_CREATE_POST.GET_LIST_CAMPAIGN_CREATE_POST,
					inc: path.resolve(__dirname, '../views/campaign/list_campaign_create_post.ejs'),
                    view: 'index_admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
						const { status, type } = req.query;
                        const listCampaign = await CAMPAIGN__CREATE_POST_MODEL.getList({ status });

						if(type === 'API'){
							return res.json({ listCampaign: listCampaign.data || [] });
						}

                        ChildRouter.renderToView(req, res, {
							listCampaign: listCampaign.data || []
						});
                    }]
                },
            },


			/**
             * ============ ************************************** ===============
             * ============ QUẢN LÝ MILESTONE CAMPAIGN CREATE POST ===============
             * ============ ************************************** ===============
             */

			/**
             * Function: Tạo cột mốc chiến dịch create post (API)
             * Date: 25/08/2021
             * Dev: MinhVH
             */
			 [CF_ROUTINGS_CAMPAIGN_CREATE_POST.ADD_MILESTONE_CAMPAIGN_CREATE_POST]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'view',
					title: 'Tạo cột mốc tạo nội dung bài viết - V-TOKEN',
					code: CF_ROUTINGS_CAMPAIGN_CREATE_POST.ADD_MILESTONE_CAMPAIGN_CREATE_POST,
					inc: path.resolve(__dirname, '../views/milestone/add_milestone_create_post.ejs'),
                    view: 'index_admin.ejs'
                },
                methods: {
					get: [ async function(req, res){
						const { campaignID } = req.query;
                        const infoCampaign = await CAMPAIGN__CREATE_POST_MODEL.getInfo({ campaignID });

						ChildRouter.renderToView(req, res, {
							infoCampaign: infoCampaign.data || {}
						});
					}],
                    post: [ async function (req, res) {
                        const { 
							campaignID, milestoneName, numPost, period, 
							continuous, limit, delayTime, tokenAmount, listPointMilestone
						} = req.body;

                        const infoAfterInsert = await MILESTONE__CAMPAIGN_CREATE_POST_MODEL.insert({ 
                            campaignID, milestoneName, numPost, period, 
							continuous, limit, delayTime, tokenAmount, listPointMilestone
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

			/**
             * Function: Thông tin cột mốc chiến dịch create post (API)
             * Date: 25/08/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_CAMPAIGN_CREATE_POST.UPDATE_MILESTONE_CAMPAIGN_CREATE_POST]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'view',
					title: 'Cập nhật cột mốc tạo nội dung bài viết - V-TOKEN',
					code: CF_ROUTINGS_CAMPAIGN_CREATE_POST.UPDATE_MILESTONE_CAMPAIGN_CREATE_POST,
					inc: path.resolve(__dirname, '../views/milestone/update_milestone_create_post.ejs'),
                    view: 'index_admin.ejs'
                },
                methods: {
					get: [ async function(req, res){
						const { milestoneID, campaignID } = req.query;

                        const infoMilestone 	= await MILESTONE__CAMPAIGN_CREATE_POST_MODEL.getInfo({ milestoneID });
                        const infoCampaign 		= await CAMPAIGN__CREATE_POST_MODEL.getInfo({ campaignID });
						const listDateMilestone = await MILESTONE__POINT_CREATE_POST_COLL.find({ milestoneID }).lean();

						ChildRouter.renderToView(req, res, {
							infoMilestone: infoMilestone.data || {},
							infoCampaign: infoCampaign.data || {},
							listDateMilestone
						});
					}],
                    put: [ async function (req, res) {
                        const { 
							milestoneID, milestoneName, numPost, period, 
							continuous, limit, delayTime, tokenAmount, listPointMilestone
						} = req.body;

                        const infoAfterUpdate = await MILESTONE__CAMPAIGN_CREATE_POST_MODEL.update({ 
                            milestoneID, milestoneName, numPost, period, 
							continuous, limit, delayTime, tokenAmount, listPointMilestone
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

			/**
			 * Function: Xóa cột mốc chiến dịch create post (API)
			 * Date: 25/08/2021
			 * Dev: MinhVH
			 */
			[CF_ROUTINGS_CAMPAIGN_CREATE_POST.DELETE_MILESTONE_CAMPAIGN_CREATE_POST]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'json',
                },
                methods: {
                    delete: [ async function (req, res) {
						const { milestoneID } = req.query;

                        const infoAfterDelete = await MILESTONE__CAMPAIGN_CREATE_POST_MODEL.delete({ milestoneID });
                        res.json(infoAfterDelete);
                    }]
                },
            },


			/**
			 * Function: Thông tin cột mốc chiến dịch create post (API)
			 * Date: 25/08/2021
			 * Dev: MinhVH
			 */
			[CF_ROUTINGS_CAMPAIGN_CREATE_POST.INFO_MILESTONE_CAMPAIGN_CREATE_POST]: {
			config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
						const { milestoneID } = req.query;

                        const infoMilestone = await MILESTONE__CAMPAIGN_CREATE_POST_MODEL.getInfo({ milestoneID });
                        res.json(infoMilestone);
                    }]
                },
            },

			/**
			 * Function: Danh sách cột mốc chiến dịch create post (API)
			 * Date: 25/08/2021
			 * Dev: MinhVH
			 */
			[CF_ROUTINGS_CAMPAIGN_CREATE_POST.GET_LIST_MILESTONE_CAMPAIGN_CREATE_POST]: {
				config: {
					auth: [ roles.role.editor.bin ],
					type: 'view',
					title: 'Danh sách cột mốc tạo nội dung bài viết - V-TOKEN',
					code: CF_ROUTINGS_CAMPAIGN_CREATE_POST.GET_LIST_MILESTONE_CAMPAIGN_CREATE_POST,
					inc: path.resolve(__dirname, '../views/milestone/list_milestone_create_post.ejs'),
                    view: 'index_admin.ejs'
				},
				methods: {
					get: [ async function(req, res){
						const { campaignID, type } = req.query;
                        const infoCampaign = await CAMPAIGN__CREATE_POST_MODEL.getInfo({ campaignID });
						const listMilestone = await MILESTONE__CAMPAIGN_CREATE_POST_MODEL.getList({ campaignID });

						if(type === 'API'){
							return res.json({
								infoCampaign: infoCampaign.data || {},
								listMilestone: listMilestone.data || []
							});
						}

						ChildRouter.renderToView(req, res, {
							infoCampaign: infoCampaign.data || {},
							listMilestone: listMilestone.data || []
						});
					}],
				},
			},

        }
    }
};
