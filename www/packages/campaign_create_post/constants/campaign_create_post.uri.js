const BASE_ROUTE = '/admin';

const CF_ROUTINGS_CAMPAIGN_CREATE_POST = {
	// CHAMPAIGN CREATE POST
	ADD_CAMPAIGN_CREATE_POST:    `${BASE_ROUTE}/add-campaign-create-post`,
	UPDATE_CAMPAIGN_CREATE_POST: `${BASE_ROUTE}/update-campaign-create-post`,
	INFO_CAMPAIGN_CREATE_POST:   `${BASE_ROUTE}/info-campaign-create-post`,
    DELETE_CAMPAIGN_CREATE_POST: `${BASE_ROUTE}/delete-campaign-create-post`,
    GET_LIST_CAMPAIGN_CREATE_POST: `${BASE_ROUTE}/list-campaign-create-post`,

	// MILESTONE
	ADD_MILESTONE_CAMPAIGN_CREATE_POST:    `${BASE_ROUTE}/add-milestone-campaign-create-post`,
	UPDATE_MILESTONE_CAMPAIGN_CREATE_POST: `${BASE_ROUTE}/update-milestone-campaign-create-post`,
	INFO_MILESTONE_CAMPAIGN_CREATE_POST:   `${BASE_ROUTE}/info-milestone-campaign-create-post`,
    DELETE_MILESTONE_CAMPAIGN_CREATE_POST: `${BASE_ROUTE}/delete-milestone-campaign-create-post`,
    GET_LIST_MILESTONE_CAMPAIGN_CREATE_POST: `${BASE_ROUTE}/list-milestone-campaign-create-post`,

    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_CAMPAIGN_CREATE_POST = CF_ROUTINGS_CAMPAIGN_CREATE_POST;
