"use strict";

const Schema 	= require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

/**
 * COLLECTION MILESTONE POINT CREATE POST CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('milestone_point_create_post', {
	milestoneID: {
		type: Schema.Types.ObjectId,
		ref: 'milestone_campaign_create_post'
	},
	date: {
		type: Number,
		required: true
	},
	point: {
		type: Number,
		required: true
	}
});
