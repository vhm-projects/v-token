"use strict";

const Schema 	= require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

/**
 * COLLECTION MILESTONE CAMPAIGN CREATE POST CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('milestone_campaign_create_post', {
	milestoneID: {
		type: String,
		unique: true,
		required: true
	},
	campaignID: {
		type: Schema.Types.ObjectId,
		ref: 'campaign_create_post'
	},
	milestoneName: {
		type: String,
		required: true
	},
	numPost: {
		type: Number,
		required: true
	},
	period: {
		type: Number,
		required: true
	},
	continuous: {
		type: Boolean,
		default: false
	},
	delayTime: {
		type: Number,
		default: 0
	},
	limit: {
		type: Number,
		default: -1
	},
	tokenAmount: {
		type: Number,
		required: true
	},
});
