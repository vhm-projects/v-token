const CAMPAIGN__CREATE_POST_MODEL 		  		= require('./models/campaign_create_post').MODEL;
const CAMPAIGN__CREATE_POST_COLL  		  		= require('./databases/campaign_create_post-coll');
const CAMPAIGN__CREATE_POST_ROUTES         		= require('./apis/campaign_create_post');
const { CF_ROUTINGS_CAMPAIGN_CREATE_POST } 		= require('./constants/campaign_create_post.uri');

const MILESTONE__CAMPAIGN_CREATE_POST_MODEL 	= require('./models/milestone_campaign_create_post').MODEL;
const MILESTONE__CAMPAIGN_CREATE_POST_COLL  	= require('./databases/milestone_campaign_create_post-coll');
const MILESTONE__POINT_CREATE_POST_COLL  		= require('./databases/milestone_point_create_post-coll');

module.exports = {
	CAMPAIGN__CREATE_POST_MODEL,
	CAMPAIGN__CREATE_POST_COLL,
	CAMPAIGN__CREATE_POST_ROUTES,

	MILESTONE__CAMPAIGN_CREATE_POST_MODEL,
	MILESTONE__CAMPAIGN_CREATE_POST_COLL,
	MILESTONE__POINT_CREATE_POST_COLL,

	CF_ROUTINGS_CAMPAIGN_CREATE_POST,
}
