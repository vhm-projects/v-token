const BASE_ROUTE = '/admin';

const CF_ROUTINGS_CAMPAIGN_MINIGAME = {
	// CHAMPAIGN MINIGAME
	ADD_CAMPAIGN_MINIGAME:    `${BASE_ROUTE}/add-campaign-minigame`,
	UPDATE_CAMPAIGN_MINIGAME: `${BASE_ROUTE}/update-campaign-minigame`,
	INFO_CAMPAIGN_MINIGAME:   `${BASE_ROUTE}/info-campaign-minigame`,
    DELETE_CAMPAIGN_MINIGAME: `${BASE_ROUTE}/delete-campaign-minigame`,
    GET_LIST_CAMPAIGN_MINIGAME: `${BASE_ROUTE}/list-campaign-minigame`,

	// MILESTONE
	ADD_MILESTONE_CAMPAIGN_MINIGAME:    `${BASE_ROUTE}/add-milestone-campaign-minigame`,
	UPDATE_MILESTONE_CAMPAIGN_MINIGAME: `${BASE_ROUTE}/update-milestone-campaign-minigame`,
	INFO_MILESTONE_CAMPAIGN_MINIGAME:   `${BASE_ROUTE}/info-milestone-campaign-minigame`,
    DELETE_MILESTONE_CAMPAIGN_MINIGAME: `${BASE_ROUTE}/delete-milestone-campaign-minigame`,
    GET_LIST_MILESTONE_CAMPAIGN_MINIGAME: `${BASE_ROUTE}/list-milestone-campaign-minigame`,

    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_CAMPAIGN_MINIGAME = CF_ROUTINGS_CAMPAIGN_MINIGAME;
