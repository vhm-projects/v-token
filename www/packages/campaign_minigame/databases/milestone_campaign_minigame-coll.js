"use strict";

const Schema 	= require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

/**
 * COLLECTION MILESTONE CAMPAIGN MINI-GAME CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('milestone_campaign_minigame', {
	milestoneID: {
		type: String,
		unique: true,
		required: true
	},
	campaignID: {
		type: Schema.Types.ObjectId,
		ref: 'campaign_minigame'
	},
	milestoneName: {
		type: String,
		required: true
	},
	numPost: {
		type: Number,
		required: true
	},
	numVote: {
		type: Number,
		required: true
	},
	limit: {
		type: Number,
		default: 1
	},
	tokenAmount: {
		type: Number,
		required: true
	},
});
