"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID                      	= require('mongoose').Types.ObjectId;

/**
 * INTERNAL PACKAGE
 */
const { randomStringNumber } 			= require('../../../utils/string_utils');

/**
 * BASES
 */
const BaseModel 						= require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const CAMPAIGN__MINIGAME_COLL           = require('../databases/campaign_minigame-coll');


class Model extends BaseModel {
    constructor() {
        super(CAMPAIGN__MINIGAME_COLL);
    }

	checkCampaignIDExists(campaignID){
		return new Promise(resolve => {
			(async function recursiveCheckCampaignID(campaignID){
				let checkExists = await CAMPAIGN__MINIGAME_COLL.findOne({ campaignID });
				if(checkExists){
					campaignID = randomStringNumber(5);
					recursiveCheckCampaignID(campaignID);
				} else{
					resolve(campaignID);
				}
			})(campaignID)
		})
	}

	insert({ campaignName, dateStart, dateEnd, content, icon }) {
        return new Promise(async resolve => {
            try {
                if(!campaignName || !dateStart || !dateEnd)
                    return resolve({ error: true, message: 'Params không hợp lệ' });

                let checkExists = await CAMPAIGN__MINIGAME_COLL.findOne({
					campaignName
                });
                if(checkExists)
                    return resolve({ error: true, message: "Tên chiến dịch đã tồn tại" });

				let campaignID = await this.checkCampaignIDExists(randomStringNumber(5));
                let dataInsert = {
					campaignID,
					campaignName, 
                    dateStart: new Date(dateStart),
                    dateEnd: new Date(dateEnd),
					content, 
					icon: icon || null,
					status: 0,
                }

                let infoAfterInsert = await this.insertData(dataInsert);

                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'Không thể tạo chiến dịch' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	update({ campaignID, campaignName, dateStart, dateEnd, content, icon, status }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(campaignID))
                    return resolve({ error: true, message: "ID chiến dịch không hợp lệ" });

                let checkExists = await CAMPAIGN__MINIGAME_COLL.findById(campaignID);
                if(!checkExists)
                    return resolve({ error: true, message: "Chiến dịch không tồn tại" });

				if(checkExists.campaignName !== campaignName){
					let checkNameExist = await CAMPAIGN__MINIGAME_COLL.findOne({ campaignName });
					if(checkNameExist) {
						return resolve({ error: true, message: 'Tên chiến dịch đã tồn tại' });
					}
				}

                let dataUpdateObj = {};
                campaignName 	&& (dataUpdateObj.campaignName  = campaignName);
                dateStart 		&& (dataUpdateObj.dateStart    	= new Date(dateStart));
                dateEnd 		&& (dataUpdateObj.dateEnd    	= new Date(dateEnd));
				content 		&& (dataUpdateObj.content    	= content);
                icon 			&& (dataUpdateObj.icon    		= icon);

				if([0,1,2].includes(+status)){
					if(+status === 1){
						let checkCampaignRunning = await CAMPAIGN__MINIGAME_COLL.findOne({ status });
						if(checkCampaignRunning){
							return resolve({ error: true, message: 'Hiện có 1 chiến dịch đang chạy' });
						}
					}
					dataUpdateObj.status = status;
				}

                const infoAfterUpdate = await this.updateWhereClause({ _id: campaignID }, dataUpdateObj);
				if(!infoAfterUpdate)
					return resolve({ error: true, message: 'Không thể cập nhật chiến dịch' });

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	delete({ campaignID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(campaignID))
                    return resolve({ error: true, message: "ID chiến dịch không hợp lệ" });

				let infoAfterDelete = await CAMPAIGN__MINIGAME_COLL.findByIdAndRemove(campaignID);

                if(!infoAfterDelete) 
                    return resolve({ error: true, message: "Không thể xoá chiến dịch" });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getInfo({ campaignID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(campaignID))
                    return resolve({ error: true, message: "ID chiến dịch không hợp lệ" });

                let infoCampaign = await CAMPAIGN__MINIGAME_COLL
					.findById(campaignID)
					.populate('icon')
					.lean();

                if(!infoCampaign)
                    return resolve({ error: true, message: "Chiến dịch không tồn tại" });

                return resolve({ error: false, data: infoCampaign });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getList({ status }) {
        return new Promise(async resolve => {
            try {
				if(status && ![0,1,2].includes(+status))
					return resolve({ error: true, message: "Trạng thái không hợp lệ" });

				status = !status ? { $in: [0,1,2] } : status;

                let listCampaign = await CAMPAIGN__MINIGAME_COLL
					.find({ status })
					.sort({ modifyAt: -1 })
					.lean();

                if(!listCampaign)
                    return resolve({ error: true, message: "Không thể lấy danh sách chiến dịch" });

                return resolve({ error: false, data: listCampaign });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
