const CAMPAIGN__MINIGAME_MODEL 		  		= require('./models/campaign_minigame').MODEL;
const CAMPAIGN__MINIGAME_COLL  		  		= require('./databases/campaign_minigame-coll');
const CAMPAIGN__MINIGAME_ROUTES         	= require('./apis/campaign_minigame');
const { CF_ROUTINGS_CAMPAIGN_MINIGAME } 	= require('./constants/campaign_minigame.uri');

const MILESTONE__CAMPAIGN_MINIGAME_MODEL 	= require('./models/milestone_campaign_minigame').MODEL;
const MILESTONE__CAMPAIGN_MINIGAME_COLL  	= require('./databases/milestone_campaign_minigame-coll');

module.exports = {
	CAMPAIGN__MINIGAME_MODEL,
	CAMPAIGN__MINIGAME_COLL,
	CAMPAIGN__MINIGAME_ROUTES,

	MILESTONE__CAMPAIGN_MINIGAME_MODEL,
	MILESTONE__CAMPAIGN_MINIGAME_COLL,

	CF_ROUTINGS_CAMPAIGN_MINIGAME,
}
