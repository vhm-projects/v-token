const CAMPAIGN__AWARDS_MODEL 		  	= require('./models/campaign_awards').MODEL;
const CAMPAIGN__AWARDS_COLL  		  	= require('./databases/campaign_awards-coll');
const CAMPAIGN__AWARDS_ROUTES         	= require('./apis/campaign_awards');
const { CF_ROUTINGS_CAMPAIGN_AWARDS } 	= require('./constants/campaign_awards.uri');

const MILESTONE__CAMPAIGN_AWARDS_MODEL 	= require('./models/milestone_campaign_awards').MODEL;
const MILESTONE__CAMPAIGN_AWARDS_COLL  	= require('./databases/milestone_campaign_awards-coll');

module.exports = {
	CAMPAIGN__AWARDS_MODEL,
	CAMPAIGN__AWARDS_COLL,
	CAMPAIGN__AWARDS_ROUTES,

	MILESTONE__CAMPAIGN_AWARDS_MODEL,
	MILESTONE__CAMPAIGN_AWARDS_COLL,

	CF_ROUTINGS_CAMPAIGN_AWARDS,
}
