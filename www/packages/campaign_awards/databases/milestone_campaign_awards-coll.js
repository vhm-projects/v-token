"use strict";

const Schema 	= require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

/**
 * COLLECTION MILESTONE CAMPAIGN AWARDS CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('milestone_campaign_awards', {
	milestoneID: {
		type: String,
		unique: true,
		required: true
	},
	campaignID: {
		type: Schema.Types.ObjectId,
		ref: 'campaign_awards'
	},
	milestoneName: {
		type: String,
		required: true
	},
	fromNumAwards: {
		type: Number,
		required: true
	},
	toNumAwards: {
		type: Number
	},
	tokenAmount: {
		type: Number,
		required: true
	},
});
