const BASE_ROUTE = '/admin';

const CF_ROUTINGS_CAMPAIGN_AWARDS = {
	// CHAMPAIGN AWARDS
	ADD_CAMPAIGN_AWARDS:    `${BASE_ROUTE}/add-campaign-awards`,
	UPDATE_CAMPAIGN_AWARDS: `${BASE_ROUTE}/update-campaign-awards`,
	INFO_CAMPAIGN_AWARDS:   `${BASE_ROUTE}/info-campaign-awards`,
    DELETE_CAMPAIGN_AWARDS: `${BASE_ROUTE}/delete-campaign-awards`,
    GET_LIST_CAMPAIGN_AWARDS: `${BASE_ROUTE}/list-campaign-awards`,

	// MILESTONE
	ADD_MILESTONE_CAMPAIGN_AWARDS:    `${BASE_ROUTE}/add-milestone-campaign-awards`,
	UPDATE_MILESTONE_CAMPAIGN_AWARDS: `${BASE_ROUTE}/update-milestone-campaign-awards`,
	INFO_MILESTONE_CAMPAIGN_AWARDS:   `${BASE_ROUTE}/info-milestone-campaign-awards`,
    DELETE_MILESTONE_CAMPAIGN_AWARDS: `${BASE_ROUTE}/delete-milestone-campaign-awards`,
    GET_LIST_MILESTONE_CAMPAIGN_AWARDS: `${BASE_ROUTE}/list-milestone-campaign-awards`,

    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_CAMPAIGN_AWARDS = CF_ROUTINGS_CAMPAIGN_AWARDS;
