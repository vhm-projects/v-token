const CAMPAIGN__APPROVE_TEAM_MODEL 		   	= require('./models/campaign_approve_team').MODEL;
const CAMPAIGN__APPROVE_TEAM_COLL  		   	= require('./databases/campaign_approve_team-coll');
const CAMPAIGN__APPROVE_TEAM_ROUTES         = require('./apis/campaign_approve_team');
const { CF_ROUTINGS_CAMPAIGN_APPROVE_TEAM } = require('./constants/campaign_approve_team.uri');

const MILESTONE__CAMPAIGN_APPROVE_TEAM_MODEL = require('./models/milestone_campaign_approve_team').MODEL;
const MILESTONE__CAMPAIGN_APPROVE_TEAM_COLL  = require('./databases/milestone_campaign_approve_team-coll');

module.exports = {
	CAMPAIGN__APPROVE_TEAM_MODEL,
	CAMPAIGN__APPROVE_TEAM_COLL,
	CAMPAIGN__APPROVE_TEAM_ROUTES,
	CF_ROUTINGS_CAMPAIGN_APPROVE_TEAM,

	MILESTONE__CAMPAIGN_APPROVE_TEAM_MODEL,
	MILESTONE__CAMPAIGN_APPROVE_TEAM_COLL
}
