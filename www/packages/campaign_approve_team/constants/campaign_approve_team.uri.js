const BASE_ROUTE = '/admin';

const CF_ROUTINGS_CAMPAIGN_APPROVE_TEAM = {
	// CHAMPAIGN APPROVE-TEAM
	ADD_CAMPAIGN_APPROVE_TEAM:    `${BASE_ROUTE}/add-campaign-approve-team`,
	UPDATE_CAMPAIGN_APPROVE_TEAM: `${BASE_ROUTE}/update-campaign-approve-team`,
	INFO_CAMPAIGN_APPROVE_TEAM:   `${BASE_ROUTE}/info-campaign-approve-team`,
    DELETE_CAMPAIGN_APPROVE_TEAM: `${BASE_ROUTE}/delete-campaign-approve-team`,
    GET_LIST_CAMPAIGN_APPROVE_TEAM: `${BASE_ROUTE}/list-campaign-approve-team`,

	// MILESTONE
	ADD_MILESTONE_CAMPAIGN_APPROVE_TEAM:    `${BASE_ROUTE}/add-milestone-campaign-approve-team`,
	UPDATE_MILESTONE_CAMPAIGN_APPROVE_TEAM: `${BASE_ROUTE}/update-milestone-campaign-approve-team`,
	INFO_MILESTONE_CAMPAIGN_APPROVE_TEAM:   `${BASE_ROUTE}/info-milestone-campaign-approve-team`,
    DELETE_MILESTONE_CAMPAIGN_APPROVE_TEAM: `${BASE_ROUTE}/delete-milestone-campaign-approve-team`,
    GET_LIST_MILESTONE_CAMPAIGN_APPROVE_TEAM: `${BASE_ROUTE}/list-milestone-campaign-approve-team`,

    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_CAMPAIGN_APPROVE_TEAM = CF_ROUTINGS_CAMPAIGN_APPROVE_TEAM;
