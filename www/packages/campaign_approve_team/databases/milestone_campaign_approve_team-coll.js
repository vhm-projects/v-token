"use strict";

const Schema 	= require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

/**
 * COLLECTION MILESTONE CAMPAIGN APPROVE-TEAM CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('milestone_campaign_approve_team', {
	milestoneID: {
		type: String,
		unique: true,
		required: true
	},
	campaignID: {
		type: Schema.Types.ObjectId,
		ref: 'campaign_approve_team'
	},
	milestoneName: {
		type: String,
		required: true
	},
	fromNumTeam: {
		type: Number,
		required: true
	},
	toNumTeam: {
		type: Number
	},
	tokenAmount: {
		type: Number,
		required: true
	},
});
