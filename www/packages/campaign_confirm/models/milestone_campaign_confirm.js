"use strict";

/**
 * EXTERNAL PACKAGE
 */
const ObjectID                      	= require('mongoose').Types.ObjectId;

/**
 * INTERNAL PACKAGE
 */
const { randomStringNumber } 			= require('../../../utils/string_utils');

/**
 * BASES
 */
const BaseModel 						= require('../../../models/intalize/base_model');

/**
 * COLLECTIONS
 */
const MILESTONE__CAMPAIGN_CONFIRM_COLL	= require('../databases/milestone_campaign_confirm-coll');


class Model extends BaseModel {
    constructor() {
        super(MILESTONE__CAMPAIGN_CONFIRM_COLL);
    }

	checkMilestoneIDExists(milestoneID){
		return new Promise(resolve => {
			(async function recursiveCheckMilestoneID(milestoneID){
				let checkExists = await MILESTONE__CAMPAIGN_CONFIRM_COLL.findOne({ milestoneID });
				if(checkExists){
					milestoneID = randomStringNumber(5);
					recursiveCheckMilestoneID(milestoneID);
				} else{
					resolve(milestoneID);
				}
			})(milestoneID)
		})
	}

	insert({ campaignID, milestoneName, fromNumConfirm, toNumConfirm = -1, tokenAmount }) {
        return new Promise(async resolve => {
            try {
                if(!milestoneName)
                    return resolve({ error: true, message: 'Tên cột mốc chiến dịch không hợp lệ' });

				if(!ObjectID.isValid(campaignID))
                    return resolve({ error: true, message: "ID chiến dịch không hợp lệ" });

                let checkExists = await MILESTONE__CAMPAIGN_CONFIRM_COLL.findOne({
					milestoneName,
					campaignID
                });
                if(checkExists)
                    return resolve({ error: true, message: "Tên cột mốc chiến dịch đã tồn tại" });

				if(!fromNumConfirm)
                    return resolve({ error: true, message: "Bạn cần nhập số lần xác minh" });
				// Convert to number
				fromNumConfirm 	= +fromNumConfirm;
				toNumConfirm 	= +toNumConfirm;

				if(fromNumConfirm > toNumConfirm && toNumConfirm !== -1)
                    return resolve({ error: true, message: "Phạm vi số lần xác minh không hợp lệ" });

				let lastMilestone = await MILESTONE__CAMPAIGN_CONFIRM_COLL
					.findOne({ campaignID })
					.sort({ createAt: -1 })
					.lean();
                if (lastMilestone) {
                    let lastToNumConfirm = lastMilestone.toNumConfirm;

                    if(lastToNumConfirm === -1)
                        return resolve({ error: true, message: "Phạm vi số lần xác minh đã không giới hạn" });
    
                    if(fromNumConfirm <= lastToNumConfirm || fromNumConfirm !== lastToNumConfirm + 1)
                        return resolve({ error: true, message: "Phạm vi số lần xác minh không hợp lệ" });
                        
                }
                
				let milestoneID = await this.checkMilestoneIDExists(randomStringNumber(5));
                let dataInsert = {
					campaignID,
					milestoneID,
                    milestoneName, 
                    fromNumConfirm,
					toNumConfirm,
					tokenAmount
                }

                let infoAfterInsert = await this.insertData(dataInsert);
                if(!infoAfterInsert)
                    return resolve({ error: true, message: 'Không thể tạo cột mốc chiến dịch' });

                return resolve({ error: false, data: infoAfterInsert });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	update({ milestoneID, milestoneName, fromNumConfirm, toNumConfirm = -1, tokenAmount }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(milestoneID))
                    return resolve({ error: true, message: "ID mốc không hợp lệ" });

                let checkExists = await MILESTONE__CAMPAIGN_CONFIRM_COLL.findById(milestoneID);
                if(!checkExists)
                    return resolve({ error: true, message: "cột mốc chiến dịch không tồn tại" });

				if(checkExists.milestoneName !== milestoneName){
					let checkNameExist = await MILESTONE__CAMPAIGN_CONFIRM_COLL.findOne({ 
						campaignID: checkExists.campaignID,
						milestoneName,
					});
					if(checkNameExist) {
						return resolve({ error: true, message: 'Tên cột mốc chiến dịch đã tồn tại' });
					}
				}

				if(!fromNumConfirm)
                    return resolve({ error: true, message: "Bạn cần nhập số lần xác minh" });

				// Convert to number
				fromNumConfirm 	= +fromNumConfirm;
				toNumConfirm 	= +toNumConfirm;

				if(fromNumConfirm > toNumConfirm && toNumConfirm !== -1)
                    return resolve({ error: true, message: "Phạm vi số lần xác minh không hợp lệ" });

                let dataUpdateObj = { toNumConfirm };
                milestoneName 	&& (dataUpdateObj.milestoneName  = milestoneName);
                fromNumConfirm 	&& (dataUpdateObj.fromNumConfirm = fromNumConfirm);
                tokenAmount		&& (dataUpdateObj.tokenAmount  	 = tokenAmount);

                const infoAfterUpdate = await this.updateWhereClause({ _id: milestoneID }, dataUpdateObj);
				if(!infoAfterUpdate)
					return resolve({ error: true, message: 'Không thể cập nhật cột mốc chiến dịch' });

                return resolve({ error: false, data: infoAfterUpdate });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getInfo({ milestoneID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(milestoneID))
                    return resolve({ error: true, message: "ID mốc không hợp lệ" });

                let infoCampaign = await MILESTONE__CAMPAIGN_CONFIRM_COLL.findById(milestoneID);
                if(!infoCampaign)
                    return resolve({ error: true, message: "Cột mốc chiến dịch không tồn tại" });

                return resolve({ error: false, data: infoCampaign });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

    delete({ milestoneID }) {
        return new Promise(async resolve => {
            try {
                if(!ObjectID.isValid(milestoneID))
                    return resolve({ error: true, message: "ID mốc không hợp lệ" });

				let infoAfterDelete = await MILESTONE__CAMPAIGN_CONFIRM_COLL.findByIdAndRemove(milestoneID);

                if(!infoAfterDelete) 
                    return resolve({ error: true, message: "Không thể xoá cột mốc chiến dịch" });

                return resolve({ error: false, data: infoAfterDelete });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

	getList({ campaignID }) {
        return new Promise(async resolve => {
            try {
				if(!ObjectID.isValid(campaignID))
					return resolve({ error: true, message: "ID chiến dịch không hợp lệ" });

                let listMilestone = await MILESTONE__CAMPAIGN_CONFIRM_COLL.find({ campaignID })
					.sort({ fromNumConfirm: -1, modifyAt: -1 })
					.lean();

                if(!listMilestone)
                    return resolve({ error: true, message: "Không thể lấy danh sách cột mốc" });

                return resolve({ error: false, data: listMilestone });
            } catch (error) {
                return resolve({ error: true, message: error.message });
            }
        })
    }

}

exports.MODEL = new Model;
