"use strict";

/**
 * EXTERNAL PACKAGE
 */
const path = require('path');

/**
 * INTERNAL PACKAGE
 */
const ChildRouter                           	= require('../../../routing/child_routing');
const roles                                 	= require('../../../config/cf_role');
const { CF_ROUTINGS_CAMPAIGN_CONFIRM }			= require('../constants/campaign_confirm.uri');

/**
 * MODELS
 */
const CAMPAIGN__CONFIRM_MODEL					= require('../models/campaign_confirm').MODEL;
const MILESTONE__CAMPAIGN_CONFIRM_MODEL			= require('../models/milestone_campaign_confirm').MODEL;
const IMAGE_MODEL								= require('../../image/models/image').MODEL;


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * ================ ************************ ===============
             * ================ QUẢN LÝ CAMPAIGN CONFIRM ===============
             * ================ ************************ ===============
             */

			/**
             * Function: Tạo chiến dịch confirm (API)
             * Date: 25/08/2021
             * Dev: MinhVH
             */
            [CF_ROUTINGS_CAMPAIGN_CONFIRM.ADD_CAMPAIGN_CONFIRM]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'view',
					title: 'Tạo chiến dịch xác minh giải đấu - V-TOKEN',
					code: CF_ROUTINGS_CAMPAIGN_CONFIRM.ADD_CAMPAIGN_CONFIRM,
					inc: path.resolve(__dirname, '../views/campaign/add_campaign_confirm.ejs'),
                    view: 'index_admin.ejs'
                },
                methods: {
					get: [ function(req, res){
						ChildRouter.renderToView(req, res);
					}],
                    post: [ async function (req, res) {
                        let { campaignName, dateStart, dateEnd, content, icon } = req.body;

						if(icon){
							const image = await IMAGE_MODEL.insert(icon);
							icon = image && image.data && image.data._id;
						}

                        const infoAfterInsert = await CAMPAIGN__CONFIRM_MODEL.insert({ 
                            campaignName, dateStart, dateEnd, content, icon
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

			/**
			 * Function: Xóa chiến dịch confirm (API)
			 * Date: 25/08/2021
			 * Dev: MinhVH
			 */
			[CF_ROUTINGS_CAMPAIGN_CONFIRM.DELETE_CAMPAIGN_CONFIRM]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'json',
                },
                methods: {
                    delete: [ async function (req, res) {
						const { campaignID } = req.query;

                        const infoAfterDelete = await CAMPAIGN__CONFIRM_MODEL.delete({ campaignID });
                        res.json(infoAfterDelete);
                    }]
                },
            },

             /**
             * Function: Cập nhật chiến dịch confirm (API)
             * Date: 25/08/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_CAMPAIGN_CONFIRM.UPDATE_CAMPAIGN_CONFIRM]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'view',
					title: 'Cập nhật chiến dịch xác minh giải đấu - V-TOKEN',
					code: CF_ROUTINGS_CAMPAIGN_CONFIRM.UPDATE_CAMPAIGN_CONFIRM,
					inc: path.resolve(__dirname, '../views/campaign/update_campaign_confirm.ejs'),
                    view: 'index_admin.ejs'
                },
                methods: {
					get: [ async function (req, res) {
						const { campaignID } = req.query;

                        const infoCampaign = await CAMPAIGN__CONFIRM_MODEL.getInfo({ campaignID });
                        ChildRouter.renderToView(req, res, {
							infoCampaign: infoCampaign.data || {}
						})
                    }],
                    put: [ async function (req, res) {
                        let { campaignID, campaignName, dateStart, dateEnd, content, icon, status } = req.body;

						if(icon){
							const image = await IMAGE_MODEL.insert(icon);
							icon = image && image.data && image.data._id;
						}

                        const infoAfterUpdate = await CAMPAIGN__CONFIRM_MODEL.update({ 
                            campaignID, campaignName, dateStart, dateEnd, content, icon, status
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

			/**
             * Function: Thông tin chiến dịch confirm (API)
             * Date: 25/08/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_CAMPAIGN_CONFIRM.INFO_CAMPAIGN_CONFIRM]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
						const { campaignID } = req.query;

                        const infoCampaign = await CAMPAIGN__CONFIRM_MODEL.getInfo({ campaignID });
                        res.json(infoCampaign);
                    }]
                },
            },

			/**
             * Function: Danh sách chiến dịch confirm (API)
             * Date: 25/08/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_CAMPAIGN_CONFIRM.GET_LIST_CAMPAIGN_CONFIRM]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'view',
					title: 'Danh sách chiến dịch xác minh giải đấu - V-TOKEN',
					code: CF_ROUTINGS_CAMPAIGN_CONFIRM.GET_LIST_CAMPAIGN_CONFIRM,
					inc: path.resolve(__dirname, '../views/campaign/list_campaign_confirm.ejs'),
                    view: 'index_admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
						const { status } = req.query;
                        const listCampaign = await CAMPAIGN__CONFIRM_MODEL.getList({ status });

                        ChildRouter.renderToView(req, res, {
							listCampaign: listCampaign.data || []
						});
                    }]
                },
            },


			/**
             * ============ ******************************************** ===============
             * ============ QUẢN LÝ MILESTONE CAMPAIGN NEWBORLD-FOLLOWER ===============
             * ============ ******************************************** ===============
             */

			/**
             * Function: Tạo cột mốc chiến dịch confirm (API)
             * Date: 25/08/2021
             * Dev: MinhVH
             */
			 [CF_ROUTINGS_CAMPAIGN_CONFIRM.ADD_MILESTONE_CAMPAIGN_CONFIRM]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'view',
					title: 'Tạo cột mốc xác minh giải đấu - V-TOKEN',
					code: CF_ROUTINGS_CAMPAIGN_CONFIRM.ADD_MILESTONE_CAMPAIGN_CONFIRM,
					inc: path.resolve(__dirname, '../views/milestone/add_milestone_confirm.ejs'),
                    view: 'index_admin.ejs'
                },
                methods: {
					get: [ async function(req, res){
						const { campaignID } = req.query;
                        const infoCampaign = await CAMPAIGN__CONFIRM_MODEL.getInfo({ campaignID });

						ChildRouter.renderToView(req, res, {
							infoCampaign: infoCampaign.data || {}
						});
					}],
                    post: [ async function (req, res) {
                        const { campaignID, milestoneName, fromNumConfirm, toNumConfirm, tokenAmount } = req.body;

                        const infoAfterInsert = await MILESTONE__CAMPAIGN_CONFIRM_MODEL.insert({ 
                            campaignID, milestoneName, fromNumConfirm, toNumConfirm, tokenAmount
                        });
                        res.json(infoAfterInsert);
                    }]
                },
            },

			/**
             * Function: Thông tin cột mốc chiến dịch confirm (API)
             * Date: 25/08/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_CAMPAIGN_CONFIRM.UPDATE_MILESTONE_CAMPAIGN_CONFIRM]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'view',
					title: 'Cập nhật cột mốc xác minh giải đấu - V-TOKEN',
					code: CF_ROUTINGS_CAMPAIGN_CONFIRM.UPDATE_MILESTONE_CAMPAIGN_CONFIRM,
					inc: path.resolve(__dirname, '../views/milestone/update_milestone_confirm.ejs'),
                    view: 'index_admin.ejs'
                },
                methods: {
					get: [ async function(req, res){
						const { milestoneID, campaignID } = req.query;

                        const infoMilestone = await MILESTONE__CAMPAIGN_CONFIRM_MODEL.getInfo({ milestoneID });
                        const infoCampaign = await CAMPAIGN__CONFIRM_MODEL.getInfo({ campaignID });

						ChildRouter.renderToView(req, res, {
							infoMilestone: infoMilestone.data || {},
							infoCampaign: infoCampaign.data || {}
						});
					}],
                    put: [ async function (req, res) {
                        const { milestoneID, milestoneName, fromNumConfirm, toNumConfirm, tokenAmount } = req.body;

                        const infoAfterUpdate = await MILESTONE__CAMPAIGN_CONFIRM_MODEL.update({ 
                            milestoneID, milestoneName, fromNumConfirm, toNumConfirm, tokenAmount
                        });
                        res.json(infoAfterUpdate);
                    }]
                },
            },

			/**
			 * Function: Xóa cột mốc chiến dịch confirm (API)
			 * Date: 25/08/2021
			 * Dev: MinhVH
			 */
			[CF_ROUTINGS_CAMPAIGN_CONFIRM.DELETE_MILESTONE_CAMPAIGN_CONFIRM]: {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'json',
                },
                methods: {
                    delete: [ async function (req, res) {
						const { milestoneID } = req.query;

                        const infoAfterDelete = await MILESTONE__CAMPAIGN_CONFIRM_MODEL.delete({ milestoneID });
                        res.json(infoAfterDelete);
                    }]
                },
            },


			/**
			 * Function: Thông tin cột mốc chiến dịch confirm (API)
			 * Date: 25/08/2021
			 * Dev: MinhVH
			 */
			[CF_ROUTINGS_CAMPAIGN_CONFIRM.INFO_MILESTONE_CAMPAIGN_CONFIRM]: {
			config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
						const { milestoneID } = req.query;

                        const infoMilestone = await MILESTONE__CAMPAIGN_CONFIRM_MODEL.getInfo({ milestoneID });
                        res.json(infoMilestone);
                    }]
                },
            },

			/**
			 * Function: Danh sách cột mốc chiến dịch confirm (API)
			 * Date: 25/08/2021
			 * Dev: MinhVH
			 */
			[CF_ROUTINGS_CAMPAIGN_CONFIRM.GET_LIST_MILESTONE_CAMPAIGN_CONFIRM]: {
				config: {
					auth: [ roles.role.editor.bin ],
					type: 'view',
					title: 'Danh sách cột mốc xác minh giải đấu - V-TOKEN',
					code: CF_ROUTINGS_CAMPAIGN_CONFIRM.GET_LIST_MILESTONE_CAMPAIGN_CONFIRM,
					inc: path.resolve(__dirname, '../views/milestone/list_milestone_confirm.ejs'),
                    view: 'index_admin.ejs'
				},
				methods: {
					get: [ async function(req, res){
						const { campaignID } = req.query;
                        const infoCampaign = await CAMPAIGN__CONFIRM_MODEL.getInfo({ campaignID });
						const listMilestone = await MILESTONE__CAMPAIGN_CONFIRM_MODEL.getList({ campaignID });

						ChildRouter.renderToView(req, res, {
							infoCampaign: infoCampaign.data || {},
							listMilestone: listMilestone.data || []
						});
					}],
				},
			},

        }
    }
};
