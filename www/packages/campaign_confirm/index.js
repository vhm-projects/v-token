const CAMPAIGN__CONFIRM_MODEL 		   	= require('./models/campaign_confirm').MODEL;
const CAMPAIGN__CONFIRM_COLL  		   	= require('./databases/campaign_confirm-coll');
const CAMPAIGN__CONFIRM_ROUTES         	= require('./apis/campaign_confirm');
const { CF_ROUTINGS_CAMPAIGN_CONFIRM } 	= require('./constants/campaign_confirm.uri');

const MILESTONE__CAMPAIGN_CONFIRM_MODEL = require('./models/milestone_campaign_confirm').MODEL;
const MILESTONE__CAMPAIGN_CONFIRM_COLL  = require('./databases/milestone_campaign_confirm-coll');

module.exports = {
	CAMPAIGN__CONFIRM_MODEL,
	CAMPAIGN__CONFIRM_COLL,
	CAMPAIGN__CONFIRM_ROUTES,
	CF_ROUTINGS_CAMPAIGN_CONFIRM,

	MILESTONE__CAMPAIGN_CONFIRM_MODEL,
	MILESTONE__CAMPAIGN_CONFIRM_COLL
}
