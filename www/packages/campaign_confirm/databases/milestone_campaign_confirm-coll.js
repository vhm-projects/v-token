"use strict";

const Schema = require('mongoose').Schema;
const BASE_COLL = require('../../../database/intalize/base-coll');

/**
 * COLLECTION MILESTONE CAMPAIGN-CONFIRM CỦA HỆ THỐNG
 */
module.exports = BASE_COLL('milestone_campaign_confirm', {
	milestoneID: {
		type: String,
		unique: true,
		required: true
	},
	campaignID: {
		type: Schema.Types.ObjectId,
		ref: 'campaign_confirm'
	},
	milestoneName: {
		type: String,
		required: true
	},
	// Số lần xác minh (từ)
	fromNumConfirm: {
		type: Number,
		required: true
	},
	// Số lần xác minh (đến)
	toNumConfirm: {
		type: Number,
	},
	tokenAmount: {
		type: Number,
		required: true
	},
});
