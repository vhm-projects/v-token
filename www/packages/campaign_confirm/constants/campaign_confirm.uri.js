const BASE_ROUTE = '/admin';

const CF_ROUTINGS_CAMPAIGN_CONFIRM = {
	// CHAMPAIGN NEWBORD-CONFIRM
	ADD_CAMPAIGN_CONFIRM:    `${BASE_ROUTE}/add-campaign-confirm`,
	UPDATE_CAMPAIGN_CONFIRM: `${BASE_ROUTE}/update-campaign-confirm`,
	INFO_CAMPAIGN_CONFIRM:   `${BASE_ROUTE}/info-campaign-confirm`,
    DELETE_CAMPAIGN_CONFIRM: `${BASE_ROUTE}/delete-campaign-confirm`,
    GET_LIST_CAMPAIGN_CONFIRM: `${BASE_ROUTE}/list-campaign-confirm`,

	// MILESTONE
	ADD_MILESTONE_CAMPAIGN_CONFIRM:    `${BASE_ROUTE}/add-milestone-campaign-confirm`,
	UPDATE_MILESTONE_CAMPAIGN_CONFIRM: `${BASE_ROUTE}/update-milestone-campaign-confirm`,
	INFO_MILESTONE_CAMPAIGN_CONFIRM:   `${BASE_ROUTE}/info-milestone-campaign-confirm`,
    DELETE_MILESTONE_CAMPAIGN_CONFIRM: `${BASE_ROUTE}/delete-milestone-campaign-confirm`,
    GET_LIST_MILESTONE_CAMPAIGN_CONFIRM: `${BASE_ROUTE}/list-milestone-campaign-confirm`,

    ORIGIN_APP: BASE_ROUTE
}

exports.CF_ROUTINGS_CAMPAIGN_CONFIRM = CF_ROUTINGS_CAMPAIGN_CONFIRM;
