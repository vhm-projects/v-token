"use strict";

const path        				= require('path');
const roles 					= require('../../config/cf_role');
const ChildRouter 				= require('../child_routing');

const { TYPE_RULE_WITH_KEY }	= require('../../config/cf_constants');
const HISTORY_COLL  			= require('../../database/history-coll');
const HISTORY_MODEL				= require('../../models/history').MODEL;

module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {
            /**
             * ==================== QUẢN LÝ TOKEN =======================
             */

            /**
             * Function: Thống kê token  (VIEW)
             * Date: 14/09/2021
             * Dev: MinhVH
             */
			'/admin/token': {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'view',
					title: 'Thống kê token - V-TOKEN',
					code: 'TOKEN',
					inc: path.resolve(__dirname, '../../views/inc/admin/token.ejs'),
                    view: 'index_admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
                        let totalNewUser = await HISTORY_COLL.countDocuments({ isNewUser: true });
                        let totalBornToken = await HISTORY_COLL.aggregate([
                            {
                                $group: {
                                    _id: null,
                                    sum: { $sum: "$afterPoint" }
                                }
                            }
                        ]);

						ChildRouter.renderToView(req, res, {
                            totalNewUser,
						    totalBornToken: totalBornToken && totalBornToken[0] && totalBornToken[0].sum || 0,
                        });
                    }]
                },
            },

			/**
             * Function: Thống kê hệ thống (VIEW)
             * Date: 22/09/2021
             * Dev: MinhVH
             */
			 '/admin/statistic': {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'view',
					title: 'Thống kê hệ thống - V-TOKEN',
					code: 'STATISTIC',
					inc: path.resolve(__dirname, '../../views/inc/admin/list_history_group_by_season.ejs'),
                    view: 'index_admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
						ChildRouter.renderToView(req, res);
                    }]
                },
            },

			/**
             * Function: Thống kê hệ thống (API)
             * Date: 25/10/2021
             * Dev: MinhVH
             */
			'/api/admin/statistic': {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
						const { start, length } = req.query;
                        const page = Number(start)/Number(length) + 1;

						let listHistoryGroupBySeason = await HISTORY_MODEL.getListHistoryGroupBySeason({
							page, limit: length
						})
                        res.json(listHistoryGroupBySeason);
                    }]
                },
            },

			/**
             * Function: Thống kê hệ thống (VIEW)
             * Date: 22/09/2021
             * Dev: MinhVH
             */
			'/admin/rules': {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'view',
					title: 'Thống kê hệ thống - V-TOKEN',
					code: 'STATISTIC',
					inc: path.resolve(__dirname, '../../views/inc/admin/list_rule_by_season.ejs'),
                    view: 'index_admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
						let { seasonID } = req.query;

						let listRuleBySeason = await HISTORY_COLL.aggregate([
							{
								$match: {
									"metadata.seasonID": seasonID
								}
							},
							{
								$group: {
									_id: { rule: "$rule" },
									historiesByRule: { $push: "$$ROOT" },
								}
							},
							{ $sort: { _id: -1 } },
						])

						listRuleBySeason = listRuleBySeason.map(async ({ historiesByRule }) => {
							let totalPoint = 0;
							let history = {};

							if(historiesByRule && historiesByRule.length){
								totalPoint = historiesByRule.reduce((acc, history) => acc += history.newPoint, 0);
								history = historiesByRule[0];
							}

							return { history, totalPoint };
						})
						listRuleBySeason = await Promise.all(listRuleBySeason);

						ChildRouter.renderToView(req, res, {
							listRuleBySeason,
							TYPE_RULE_WITH_KEY
						});
                    }]
                },
            },

			/**
             * Function: Thống kê hệ thống (VIEW)
             * Date: 22/09/2021
             * Dev: MinhVH
             */
			 '/admin/rule/histories': {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'view',
					title: 'Thống kê hệ thống - V-TOKEN',
					code: 'STATISTIC',
					inc: path.resolve(__dirname, '../../views/inc/admin/list_history_by_rule.ejs'),
                    view: 'index_admin.ejs'
                },
                methods: {
                    get: [ async function (req, res) {
						ChildRouter.renderToView(req, res);
                    }]
                },
            },

			/**
             * Function: Thống kê hệ thống (API)
             * Date: 25/10/2021
             * Dev: MinhVH
             */
			'/api/admin/rule/histories': {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
						const { rule, seasonID, fromDate, toDate, keyword, start, length } = req.query;
                        const page = Number(start)/Number(length) + 1;

						let listHistoryGroupBySeason = await HISTORY_MODEL.getListHistoryByRuleSeason({
							rule, seasonID, fromDate, toDate, keyword, page, limit: length
						})
                        res.json(listHistoryGroupBySeason);
                    }]
                },
            },

        }
    }
};
