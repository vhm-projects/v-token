"use strict";

const roles 					= require('../../config/cf_role');
const ChildRouter 				= require('../child_routing');
const USER_SESSION				= require('../../session/user-session');

const { TYPE_RULE } 			= require('../../config/cf_constants');
const { CF_ROUTINGS_RULE } 		= require('../../config/constants/rule.uri');
const { CF_ROUTINGS_HISTORY } 	= require('../../config/constants/history.uri');

const CHECK_RULE 				= require('../../models/rule').MODEL;
const HISTORY_MODEL				= require('../../models/history').MODEL;
const HISTORY_COLL				= require('../../database/history-coll');


module.exports = class Auth extends ChildRouter {
    constructor() {
        super('/');
    }

    registerRouting() {
        return {

			'/': {
                config: {
                    auth: [ roles.role.editor.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ (req, res) => {
						 /**
                         * CHECK LOGGED AND REDIRECT
                         */
						  const infoLogin = USER_SESSION.getUser(req.session);
						  if (!infoLogin)
							  return res.redirect('/login');

						res.redirect('/admin/history');
					}]
                },
            },

			/** =================== ********** =======================
             * 	=================== CHECK RULE =======================
			 * 	=================== ********** =======================
             */

			/**
             * Function: Check Rule (API)
             * Date: 25/08/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_RULE.CHECK_RULE]: {
                config: {
                    auth: [ roles.role.user.bin ],
                    type: 'json',
                },
                methods: {
                    post: [ async function (req, res) {
                        const RULE_TYPE = req.body.typeRule;

						switch (+RULE_TYPE) {

							// CHECK RULE CONFIRM-SEASON
							case TYPE_RULE.CONFIRM_SEASON: {
								const { userID, seasonID } = req.body;
								const responseCheckRule = await CHECK_RULE.checkRuleCampaignConfirm({ 
									userConfirm: userID, seasonID
								});
								return res.json(responseCheckRule);
							}

							// CHECK RULE NEWBORN-FOLLOWER
							case TYPE_RULE.NEWBORD_FOLLOWER: {
								const { userID, authorID, leagueID } = req.body;
								const responseCheckRule = await CHECK_RULE.checkRuleCampaignNewbordFollower({ 
									userID, authorID, seasonID: leagueID 
								});
								return res.json(responseCheckRule);
							}

							// CHECK RULE APPROVE-TEAM
							case TYPE_RULE.APPROVE_TEAM: {
								const { authorID, leagueID, seasonID, teamID } = req.body;
								const responseCheckRule = await CHECK_RULE.checkRuleCampaignApproveTeam({ 
									authorID, leagueID, seasonID, teamID
								});
								return res.json(responseCheckRule);
							}

							// CHECK RULE CREATE-SCHEDULE
							case TYPE_RULE.CREATE_SCHEDULE: {
								const { authorID, leagueID, seasonID } = req.body;
								const responseCheckRule = await CHECK_RULE.checkRuleCampaignCreateSchedule({ 
									authorID, leagueID, seasonID
								});
								return res.json(responseCheckRule);
							}

                            // CHECK RULE LIKE POST
							case TYPE_RULE.LIKE_POST: {
								const { authorID, postID, likerID } = req.body;
								const responseCheckRule = await CHECK_RULE.checkRuleCampaignLikePost({ 
									authorID, postID, likerID
								});
								return res.json(responseCheckRule);
							}

                            // CHECK RULE AWARDS
							case TYPE_RULE.AWARDS: {
								const { authorID, leagueID, seasonID, typeReward } = req.body;
								const responseCheckRule = await CHECK_RULE.checkRuleCampaignAwards({ 
									authorID, leagueID, seasonID, typeReward
								});
								return res.json(responseCheckRule);
							}

							// CHECK RULE CREATE POST
							case TYPE_RULE.CREATE_POST: {
								const { authorID, postID } = req.body;
								const responseCheckRule = await CHECK_RULE.checkRuleCampaignCreatePost({ 
									authorID, postID
								});
								return res.json(responseCheckRule);
							}

							// CHECK RULE MINIGAME
							case TYPE_RULE.MINIGAME: {
								const { authorID, minigameID, postID, voterID } = req.body;
								const responseCheckRule = await CHECK_RULE.checkRuleCampaignMinigame({ 
									authorID, minigameID, postID, voterID
								});
								return res.json(responseCheckRule);
							}

							// CHECK RULE UPDATE RESULT
							case TYPE_RULE.UPDATE_RESULT: {
								const { authorID, leagueID, seasonID, matchID } = req.body;
								const responseCheckRule = await CHECK_RULE.checkRuleCampaignUpdateResult({ 
									authorID, leagueID, seasonID, matchID
								});
								return res.json(responseCheckRule);
							}

							default:
								return res.json({ code: 404, message: "Not found rule" });
						}

                    }]
                },
            },


			/**
             * Function: Danh sách điểm của user (API)
             * Date: 07/09/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_HISTORY.GET_LIST_POINT_HISTORY_BY_USER]: {
                config: {
                    auth: [ roles.role.user.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { userID } = req.params;

						const listHistoryPoint = await HISTORY_COLL
							.find({ user: userID })
							.sort({ _id: -1 })
							.lean();

						res.json(listHistoryPoint);
                    }]
                },
            },

			/**
             * Function: Thông tin điểm của user (API)
             * Date: 07/09/2021
             * Dev: MinhVH
             */
			[CF_ROUTINGS_HISTORY.GET_INFO_POINT_BY_USER]: {
                config: {
                    auth: [ roles.role.user.bin ],
                    type: 'json',
                },
                methods: {
                    get: [ async function (req, res) {
                        const { userID } = req.params;

						const infoPointOfUser = await HISTORY_MODEL.getInfoPointByUser({ userID });
						res.json(infoPointOfUser);
                    }]
                },
            },

        }
    }
};
