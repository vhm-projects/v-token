"use strict";

const express 	= require('express');
const APP 		= require('../../app');
const url 		= require('url');
const moment 	= require('moment');

const { CF_ROUTINGS_CAMPAIGN_NEWBORD_FOLLOWER } = require('../packages/campaign_newbord_follower/constants/campaign_newbord_follower.uri');
const { CF_ROUTINGS_CAMPAIGN_LIMIT_NEWBORN } 	= require('../packages/campaign_limit_newborn/constants/campaign_limit_newborn.uri');
const { CF_ROUTINGS_CAMPAIGN_CONFIRM } 			= require('../packages/campaign_confirm/constants/campaign_confirm.uri');
const { CF_ROUTINGS_CAMPAIGN_APPROVE_TEAM } 	= require('../packages/campaign_approve_team/constants/campaign_approve_team.uri');
const { CF_ROUTINGS_CAMPAIGN_CREATE_SCHEDULE } 	= require('../packages/campaign_create_schedule/constants/campaign_create_schedule.uri');
const { CF_ROUTINGS_CAMPAIGN_LIKE } 			= require('../packages/campaign_like/constants/campaign_like.uri');
const { CF_ROUTINGS_CAMPAIGN_AWARDS } 			= require('../packages/campaign_awards/constants/campaign_awards.uri');
const { CF_ROUTINGS_CAMPAIGN_CREATE_POST }		= require('../packages/campaign_create_post/constants/campaign_create_post.uri');
const { CF_ROUTINGS_CAMPAIGN_MINIGAME }			= require('../packages/campaign_minigame/constants/campaign_minigame.uri');
const { CF_ROUTINGS_CAMPAIGN_UPDATE_RESULT }	= require('../packages/campaign_update_result/constants/campaign_update_result.uri');
const { CF_ROUTINGS_HISTORY } 					= require('../packages/history/constants/history.uri');
const { CF_ROUTINGS_USER } 						= require('../packages/users/constants/user.uri');

const { TYPE_RULE, TYPE_RULE_WITH_KEY }			= require('../config/cf_constants');
const { loadPathImage } 						= require('../utils/utils');


class ChildRouter{
    constructor(basePath) {
        this.basePath = basePath;
        this.registerRouting;
    }

    registerRouting() {
    }
    exportModule() {
        let router = express.Router();
        
        for (let basePath in this.registerRouting()) {
            const item = this.registerRouting()[basePath];
    
            if (typeof item.methods.post !== 'undefined' && item.methods.post !== null) {
                if (item.methods.post.length === 1) {
                    router.post(basePath, item.methods.post[0]);
                } else if (item.methods.post.length === 2) {
                    router.post(basePath, item.methods.post[0], item.methods.post[1]);
                }
            }

            if (typeof item.methods.get !== 'undefined' && item.methods.get !== null) {
                if (item.methods.get.length === 1) {
                    router.get(basePath, item.methods.get[0]);
                } else if (item.methods.get.length === 2) {
                    router.get(basePath, item.methods.get[0], item.methods.get[1]);
                }
            }

            if (typeof item.methods.put !== 'undefined' && item.methods.put !== null) {
                if (item.methods.put.length === 1) {
                    router.put(basePath, item.methods.put[0]);
                } else if (item.methods.put.length === 2) {
                    router.put(basePath, item.methods.put[0], item.methods.put[1]);
                }
            }

            if (typeof item.methods.delete !== 'undefined' && item.methods.delete !== null) {
                if (item.methods.delete.length === 1) {
                    router.delete(basePath, item.methods.delete[0]);
                } else if (item.methods.delete.length === 2) {
                    router.delete(basePath, item.methods.delete[0], item.methods.delete[1]);
                }
            }

        }
        return router;
    }

    /**
     * 
     * @param {*} msg 
     * @param {*} res 
     * @param {*} data 
     * @param {*} code
     * @param {*} status  tham số nhận biết lỗi
     */
    static responseError(msg, res, data, code, status) {
        if (code) {
            res.status(code);
        }
        return res.json({error: true, message: msg, data: data, status: status});
    }

    static response(response, data) {
        return response.json(data);
    }


    static responseSuccess(msg, res, data, code) {
        if (code) {
            res.status(code);
        }

        return res.json({error: false, message: msg, data: data});
    }

    static pageNotFoundJson(res) {
        res.status(404);
        return res.json({"Error": "Page not found!"});
    }

     /**
     * 
     * @param {*} req 
     * @param {*} res 
     * @param {*} data 
     * @param {*} title 
     */
    static renderToView(req, res, data, title) {
        data = typeof data === 'undefined' || data === null ? {} : data;

        if (title) {
            res.bindingRole.config.title = title;
        }

        data.render = res.bindingRole.config;
        data.url = url.format({
            protocol: req.protocol,
            host: req.get('host'),
            pathname: req.originalUrl
        });

		data.hrefCurrent = req.originalUrl;

		if(req.user){
			data.infoUser = req.user;
		}

		// console.log({
		// 	REQUEST_LIMIT: req.rateLimit
		// })

        /**
         * Thư viện moment
         */
        data.moment = moment;
        
		data.CF_ROUTINGS_CAMPAIGN_NEWBORD_FOLLOWER 	= CF_ROUTINGS_CAMPAIGN_NEWBORD_FOLLOWER;
		data.CF_ROUTINGS_CAMPAIGN_CONFIRM 			= CF_ROUTINGS_CAMPAIGN_CONFIRM;
		data.CF_ROUTINGS_CAMPAIGN_APPROVE_TEAM 		= CF_ROUTINGS_CAMPAIGN_APPROVE_TEAM;
		data.CF_ROUTINGS_CAMPAIGN_CREATE_SCHEDULE 	= CF_ROUTINGS_CAMPAIGN_CREATE_SCHEDULE;
		data.CF_ROUTINGS_CAMPAIGN_LIMIT_NEWBORN 	= CF_ROUTINGS_CAMPAIGN_LIMIT_NEWBORN;
		data.CF_ROUTINGS_CAMPAIGN_LIKE 	            = CF_ROUTINGS_CAMPAIGN_LIKE;
		data.CF_ROUTINGS_CAMPAIGN_AWARDS 	        = CF_ROUTINGS_CAMPAIGN_AWARDS;
		data.CF_ROUTINGS_CAMPAIGN_CREATE_POST		= CF_ROUTINGS_CAMPAIGN_CREATE_POST;
		data.CF_ROUTINGS_CAMPAIGN_MINIGAME			= CF_ROUTINGS_CAMPAIGN_MINIGAME;
		data.CF_ROUTINGS_CAMPAIGN_UPDATE_RESULT		= CF_ROUTINGS_CAMPAIGN_UPDATE_RESULT;
		data.CF_ROUTINGS_HISTORY 					= CF_ROUTINGS_HISTORY;
		data.CF_ROUTINGS_USER						= CF_ROUTINGS_USER;

        data.TYPE_RULE_WITH_KEY = TYPE_RULE_WITH_KEY;
		data.TYPE_RULE          = TYPE_RULE;

		// Utils
		data.loadPathImage = loadPathImage;

        return res.render(res.bindingRole.config.view, data);
    }

    static renderToPath(req, res, path, data) {
        data = data == null ? {} : data;
        data.render = res.bindingRole.config;
        return res.render(path, data);
    }

    static renderToViewWithOption(req, res, pathRender, data) {
        return res.render(pathRender, {data});
    }

    static redirect(res, path) {
        return res.redirect(path);
    }
}

module.exports = ChildRouter;