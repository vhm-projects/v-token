
module.exports = async function changeStream(collection) {
	// Create a new mongoose model
	// const personSchema = new mongoose.Schema({
	//   name: String
	// });
	// const Person = mongoose.model('Person', personSchema);
  
	// Create a change stream. The 'change' event gets emitted when there's a
	// change in the database
	collection.watch().on('change', data => console.log(new Date(), data));
  
	// Insert a doc, will trigger the change stream handler above
	console.log(new Date(), 'Inserting doc');
	await collection.create({ rule: 1, currentPoint: 0, newPoint: 100, afterPoint: 100 });
}

