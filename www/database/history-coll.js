"use strict";

const Schema	= require('mongoose').Schema;
const BASE_COLL = require('./intalize/base-coll');
const changeStream = require('./intalize/change-stream');

/**
 * COLLECTION HISTORY CỦA HỆ THỐNG
 */

let HISTORY_COLL = BASE_COLL('history', {
	rule: {
		type: Number,
		required: true
	},
	// Cột mốc
	milestone: {
		kind: String,
		item: { type: Schema.Types.ObjectId, refPath: 'milestone.kind' }
	},
	// Điểm hiện tại
	currentPoint: {
		type: Number,
		default: 0
	},
	// Điểm được tích
	newPoint: {
		type: Number
	},
	// Điểm sau khi tích
	afterPoint: {
		type: Number
	},
	// Số lượng tích luỹ theo rule
	cumulativeAmount: {
		type: Number
	},
	// Ngừơi dùng mới
	isNewUser: {
		type: Boolean,
		default: true
	},
	user: {
		type: String,
		required: true
	},
	metadata: {
		type: Schema.Types.Mixed
	}
});

module.exports = HISTORY_COLL;