#!/usr/bin/env bash

echo "
----------------------
  MONGODB INSTALL (BEGIN)
----------------------
"

wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | sudo apt-key add -

echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.2.list

sudo apt-get update

sudo apt-get install -y mongodb-org

# start mongodb
sudo systemctl start mongod

# set mongodb to start automatically on system startup
sudo systemctl enable mongod

echo "
--------------------------------------------
  MONGODB INSTALL (DONE)
--------------------------------------------
"


echo "
--------------------------------------------
  NODEJS RUNTIME INSTALL (BEGIN)
--------------------------------------------
"
cd ~

curl -sL https://deb.nodesource.com/setup_10.x -o nodesource_setup.sh

sudo apt install nodejs

nodejs -v

sudo apt install build-essential

echo "
--------------------------------------------
  NODEJS RUNTIME INSTALL (DONE)
--------------------------------------------
"

sudo npm cache clean -f

sudo npm install -g n

sudo n stable

echo "
--------------------------------------------
  NODEJS_RUNTIME UPDATE VERSION (DONE)
--------------------------------------------
"

echo "
--------------------------------------------
  REDIS INSTALL (BEGIN)
--------------------------------------------
"

sudo apt update

sudo apt install redis-server

# Bổ sung thêm 'sudo nano /etc/redis/redis.conf' thay đổi 'supervised systemd'

echo "
--------------------------------------------
  REDIS INSTALL (DONE)
--------------------------------------------
"

echo "
--------------------------------------------
  YARN, PM2
--------------------------------------------
"
npm install yarn -g
npm install pm2 -g


echo "
--------------------------------------------
  NGINX (SETUP)
--------------------------------------------
"
sudo apt update

sudo apt install nginx

sudo ufw app list

sudo ufw allow 'Nginx HTTP'